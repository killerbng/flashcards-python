# This Python file uses the following encoding: utf-8
from Russian_Flash_Cards import *
sys.path.insert(0, './tools')
from tools import *

JustRunMain = True

DBGGR = Debugger()
if not JustRunMain:
    ARS = AudioRowSearch()
    IIDB = InsertIntoDB()
    SW = StripWhitespace()
    AP = ADD_PRO();

while True:
    if JustRunMain:
        DBGGR.run()
    else:
        options = "[1] DEBUGGER [2] Audio Row Search [3] Insert Into DB [4] Strip Whitespace [5] Add Translites"
        answer = input(str(options) + " ")
        if answer == '1':
            DBGGR.run()
        elif answer == '2':
            ARS.run()
        elif answer == '3':
            IIDB.run()
        elif answer == '4':
            SW.run()
        elif answer == '5':
            AP.get_table()
