# This Python file uses the following encoding: utf-8
from Russian_Flash_Cards import *
sys.path.insert(0, './libs')
from libs import *

# Init Classes
FC = Flashcards()
LT = ListTroubles()
LP = ListeningPractice()
SS = SentenceStructures()
WP = WritingPractice()
CHLG = Challenge()
NSW = NoSingleWords()
WO = Words_Only()
TT = Ten_Ten()
SPN = SpecialNumbers()
SC = SpecificCategory()
LNGTXT = LongText()
NASS = NotAddedScreenshots()
TC = TodaysConvos()


# LOOP IT
while True:
    # First we check "Pen And Paper" Task if True
    if TASKER:
        do_task_checks()
        if len(need_to_practice) > 0:
            practice_these()

    # Now Run Selections
    options = "[1] Flashcards [2] Listening Practice [3] Writing Practice [4] Constructions Practice\n"
    options += "[5] List ALL Trouble Words [6] 3x Challenge [7] Words ONLY [8] Sentences - Questions ONLY\n"
    options += "[9] 10-10 RU-DE [10] Special Phone Numbers [11] Specific Topic [12] Long Text Practice\n"
    options += "[13] Practice SS not added yet [14] Todays Course Convos"

    # Let user choose option
    answer = input(str(options) + " ")
    if answer == '1':
        FC.run()
    elif answer == '2':
        LP.run()
    elif answer == '3':
        WP.run()
        print("\n")
    elif answer == '4':
        print("\n")
        SS.run()
    elif answer == '5':
        LT.run()
    elif answer == '6':
        CHLG.run()
    elif answer == '7':
        WO.run()
    elif answer == '8':
        NSW.run()
    elif answer == '9':
        TT.run()
    elif answer == '10':
        SPN.run()
    elif answer == '11':
        SC.run()
    elif answer == '12':
        LNGTXT.run()
    elif answer == '13':
        NASS.run()
    elif answer == '14':
        TC.run()
