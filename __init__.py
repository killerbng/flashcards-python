# This Python file uses the following encoding: utf-8
from Russian_Flash_Cards.db import db_user, db_pass, db_host, db_db
import datetime
import mysql.connector
#from mysql.connector import errorcode
from termcolor import colored
from random import randint, choice
from time import sleep
import re
import platform
import os
from os.path import abspath
import fnmatch
import sys
import tempfile
from subprocess import call

#
# EDIT MISC SETTINGS HERE
#
DEFAULT_OUTPUT_ENCODING = "utf-8"
IGNORE_ZERO = False
SHOW_EXCEPTION_ERRORS = False
TASKER = False
IMAGE_PROGRAM_TITLE = "IrfanView"  # CaSe SenSiTiVe
ADD_CUSTOM_RU_ITEMS = True
custom_force_add_RU = [] # Any entry you want forced to be added to Flashcards - even if it's not "up for review"
custom_force_add_RU_old = [4228, 1153, 6249, 10199, 3958, 10217, 10157, 10224, 10148, 10439, 657, 10535, 1350, 10602]
play_sounds = True
ru_forced = "ru_forced"
TEMP_FILE_LOC = '/tmp' if platform.system() == 'Darwin' else tempfile.gettempdir()
Only_Old_Entries = True # Toggle this if you want only old old entries.. IE: it's 2018, so only query 2017 or older.
SS_TODO_PATH = "C:/Users/Nathan Chambers/Desktop/Русский/SS/" # This is the path to any screen shots you have that are not yet in the database


'''
    FFMPEG SUPPORT:
        WINDOWS:
            Set direct path IF on windows
            Set to False to use ffmpeg.exe in the libs/ folder. (May only work on x64 systems)
        LINUX:
            Setting location to False should be fine
            If you have a non-default path, set a path
        MAC:
            See Linux info.
'''
FFMPEG_LOC = 'G:/PROGRAMMING-SCRIPTING/python/Russian_Flash_Cards/libs/ffmpeg.exe'


'''
      EDIT ONLINE SETTINGS HERE
    
      HAVE_VLC is for the python binding for VLC. Note also, python and vlc must match as x86 or x64.
      COPY libvlc.dll to whereever vlc.py is in your python Lib folder
      HAVE_GST can not test currently. Not sure why.
'''
INTERNET_SOUNDS = True # False
HAVE_VLC = True # True  - Requires python-vlc package
HAVE_GST = False #
# False
BASE_AUDIO_URL = "http://killerbng.info/global/audio/"
INTERNET_EXT = ".mp3"


#
# CONVO PATHS
# EDIT AS NEEDED - FULL PATH IS BEST - EVEN IF IN SAME FOLDER AS THIS PROJECT
# IF YOU DO NOT HAVE TOGGLE TO FALSE
#
PIMS_CONVO_PATH = "G:/PROGRAMMING-SCRIPTING/python/Russian_Flash_Cards/audio/ru_pimsleur_convos/" # Path or False
RUACCCEL_CONVO_PATH = "J:/Language Tools/Русский/Course Material/Russian Accelerator/" # Path or False


'''
    LINUX SETTINGS:
    
'''
#TO DO


#
#
# DO NOT EDIT BELOW THIS LINE
#
#

INIT_DIR = os.getcwd()
LIB_DIR = INIT_DIR + "\\libs\\"
TOOLS_DIR = INIT_DIR + "\\tools\\"

ON_LINUX = False
ON_MAC = False
ON_WINDOWS = False

if platform.system() == 'Windows':
    ON_WINDOWS = True
    if SHOW_EXCEPTION_ERRORS:
        print("FOUND WINDOWS OS")
elif platform.system() == 'Darwin': # Mac support not really implememnted. BUT here incase I want to better support it
    ON_MAC =  True
    if SHOW_EXCEPTION_ERRORS:
        print("FOUND MAC OS")
else:
    ON_LINUX = True
    from subprocess import check_call
    if SHOW_EXCEPTION_ERRORS:
        print("FOUND LINUX OS")

if ON_LINUX:
    SET_LINUX_PATH = str(os.path.realpath(__file__))
    LINUX_PATH = SET_LINUX_PATH.replace("__init__.py", "")
    LINUX_PATH = LINUX_PATH.replace("__init__.pyc", "")
    LINUX_PATH = abspath(LINUX_PATH)
    if SHOW_EXCEPTION_ERRORS:
        print(SET_LINUX_PATH)
        print(LINUX_PATH)
else:
    import pyglet
    pyglet.options['audio'] = ('openal', 'directsound', 'silent')
    SET_WINDOWS_PATH = str(os.path.realpath(__file__))
    WINDOWS_PATH = SET_WINDOWS_PATH.replace("__init__.py", "")
    WINDOWS_PATH = WINDOWS_PATH.replace("__init__.pyc", "")
    WINDOWS_PATH = abspath(WINDOWS_PATH)
    if SHOW_EXCEPTION_ERRORS:
        print(SET_WINDOWS_PATH)
        print(WINDOWS_PATH)

# These date settings need to be phased out becuase it is too static.
today = datetime.datetime.now()
year = today.year
month = today.month
day = today.day

OnBR = False
OnDE = False
OnCN = False
OnESP = False
OnFI = False
OnLTV = False
OnON = False
OnRU = False
OnSE = False
OnSPA = False
OnUKR = False

Reverse = False

long = 35 # 40
long2 = 45

'''
################################################
                Dictionaries
################################################
'''

config = {'user': db_user, 'password': db_pass, 'host': db_host, 'database': db_db, 'raise_on_warnings': True}

lang_tables = {
    "ara": "ara_words", "br": 'br_words', "cn": 'cn_words', 'de': 'de_words', 'esp': 'esp_words', 'fi': 'fi_words',
    'ltv': 'ltv_words', 'on': "on_words", 'ru': 'ru_words', 'se': 'se_words', 'spa': 'spa_words', 'ukr': 'ukr_words',
    'latin': 'lat_words'
}

styles = {
    "first": "blue", "second": "magenta", "third": "red",
    "type": "green", 'left': 'white',
    ">>>": "yellow", "()": "blue"
}

table_rows = {1: "ru", 2: "pro", 3: "en", 4: "audio", 5: "word_type", 6: "word_gender"}

cyrillic_translit_RU = {
    u'\u0410': 'A', u'\u0430': 'a', u'\u0411': 'B', u'\u0431': 'b', u'\u0412': 'V', u'\u0432': 'v',
    u'\u0413': 'G', u'\u0433': 'g', u'\u0414': 'D', u'\u0434': 'd', u'\u0415': 'E', u'\u0435': 'e',
    u'\u0416': 'Zh', u'\u0436': 'zh', u'\u0417': 'Z', u'\u0437': 'z', u'\u0418': 'I', u'\u0438': 'i',
    u'\u0419': 'I', u'\u0439': 'i', u'\u041a': 'K', u'\u043a': 'k', u'\u041b': 'L', u'\u043b': 'l',
    u'\u041c': 'M', u'\u043c': 'm', u'\u041d': 'N', u'\u043d': 'n', u'\u041e': 'O', u'\u043e': 'o',
    u'\u041f': 'P', u'\u043f': 'p', u'\u0420': 'R', u'\u0440': 'r', u'\u0421': 'S', u'\u0441': 's',
    u'\u0422': 'T', u'\u0442': 't', u'\u0423': 'U', u'\u0443': 'u', u'\u0424': 'F', u'\u0444': 'f',
    u'\u0425': 'Kh', u'\u0445': 'kh', u'\u0426': 'Ts', u'\u0446': 'ts',u'\u0427': 'Ch', u'\u0447': 'ch',
    u'\u0428': 'Sh', u'\u0448': 'sh', u'\u0429': 'Shch', u'\u0449': 'shch', u'\u042a': '"', u'\u044a': '"',
    u'\u042b': 'Y', u'\u044b': 'y', u'\u042c': "'", u'\u044c': "'", u'\u042d': 'E', u'\u044d': 'e', 'ё': 'yo',
    u'\u042e': 'Iu', u'\u044e': 'iu', u'\u042f': 'Ya', u'\u044f': 'ya', u'\u1D27': 'el', u'\xa0': ' ', 'Ё': 'YO'
}

cyrillic_translit_UKR = {  # Very Incomplete - Not in order :/ Enable in Main program once done
    u'\u0410': 'A', u'\u0430': 'a', u'\u0411': 'B', u'\u0431': 'b', "В": "V", "в": "v", "Д": "D", "д": "d", "Х": "H",
    "х": "h", "Й": "Y", "й": "y", "Ї": "YE", "ї": "ye", "О": "O", "о": "o", "Е": "E", "е": "e", "М": "M", "м": "m",
    u'\u041d': 'N', u'\u043d': 'n', "И": "I", "и": "i", "Я": "YA", "я": "ya", u'\u0422': 'T', u'\u0442': 't', "У": "U",
    "у": "u", "Л": "L", "л": "l", u'\u0427': 'Ch', u'\u0447': 'ch', u'\u0426': 'Ts', u'\u0446': 'ts', 'ь': "'"
}

german_translite = {  # Very Incomplete  -  'Ö':'', 'ö':'', 'Ü':'', 'ü':'', '§':'',   Enable in Main program once done
    'er': 'a', 'S': 'Z', 's': 'z', 'W': 'V', 'w': 'v', 'ß': 'ss', 'Ä': 'O', 'ä': 'o', 'V': 'F', 'v': 'f', 'J': 'y',
    'j': 'y'
}

latvian_translit = {}

chinese_pinyin = {}

'''
################################################
               LIST
################################################
'''

sentence_cats = [35, 46, 47, 48, 61]  # Sentences / Questions / Q&A really
exit_quit = ['q', 'QUIT', 'e', 'exit', 'E', 'EXIT', 'b', 'back']
do_it = ['1', 1, 'y', 'Y', 'yes', 'YES']

'''
################################################
               DB QUERY RELATED
################################################
'''


def fill_forced_words():
    global custom_force_add_RU
    conn = mysql.connector.connect(**config)
    cur = conn.cursor()
    query = "SELECT * FROM "+ru_forced+" WHERE enabled='1' ORDER BY id ASC"
    cur.execute(query)
    for c in cur:
        custom_force_add_RU.append(c[1])
    cur.close()
    conn.close()


def update_item(word, table, row, id):
    try:
        word = word.replace("'", "''")
        word = word.replace("`", "``")
    except Exception as e:
        if SHOW_EXCEPTION_ERRORS:
            print("Error in update_item while running word.replace()\n" + e)
        else:
            pass
    try:
        # I want to use comments out below, but this seems safer for now.
        conn = mysql.connector.connect(**config)
        cur = conn.cursor()
        query = "UPDATE " + str(table) + " SET " + str(row) + "='" + str(word) + "' WHERE id='" + str(id) + "'"
        cur.execute(query)
        conn.commit()
        cur.close()
        conn.close()
        return True
        '''
        #This way errors still when a ' is in the mix
        if row is 'trouble_word':
            conn = mysql.connector.connect(**config)
            cur = conn.cursor()
            query = "UPDATE " + str(table) + " SET " + str(row) + "='" + str(word) + "' WHERE id='" + str(id) + "'"
            cur.execute(query)
            conn.commit()
            cur.close()
            conn.close()
            return True
        else:
            # This new way doesn't update trouble_word toggle, but works with ' in string
            conn = mysql.connector.connect(**config)
            cur = conn.cursor()
            query = "UPDATE %s SET %s='%s' WHERE id='%s'"
            data = (
                str(table),
                str(row),
                str(word),
                str(id)
            )
            cur.execute(query, data)
            conn.commit()
            cur.close()
            conn.close()
            return True
        '''
    except Exception as e:
        if SHOW_EXCEPTION_ERRORS:
            print("Error in update_item while adding to db" + e)
        return False


def add_to_correct_incorrect_count(id, table, cic, dt):
    dt = dt.replace('"', '')
    conn = mysql.connector.connect(**config)
    cur = conn.cursor()
    query = "SELECT * FROM " + table + " WHERE id='" + id + "'"
    cur.execute(str(query))
    corr = 0
    incorr = 0
    if cic == 0:
        for i in cur:
            corr = i[11] + 1
            incorr = i[12]
            query = "UPDATE " + table + " SET date='" + dt + "', times_correct='" + str(corr) + "' WHERE id='" \
                    + str(id) + "'"
    elif cic == 1:
        for i in cur:
            corr = i[11]
            incorr = i[12] + 1
            query = "UPDATE " + table + " SET date='" + dt + "', times_wrong='" + str(incorr) + "' WHERE id='" \
                    + str(id) + "'"
    cur.execute(str(query))
    conn.commit()
    cur.close()
    conn.close()
    print("You have got this word correct", str(corr), "time(s) and incorrect", str(incorr), "time(s).")


def update_daily_count(toggle):  # toggle 0 = +corr  1 = +incorr
    dateexist = False
    current_correct = 0
    current_incorrect = 0
    temp_date = get_date()
    d = "" + str(temp_date[0]) + " " + str(temp_date[1]) + " " + str(temp_date[2]) + ""

    #  Check if date exist yet.
    conn = mysql.connector.connect(**config)
    cur = conn.cursor()
    query = "SELECT COUNT(*) AS rc FROM `ru_study_count` WHERE date='" + d + "'"
    cur.execute(query)
    count_rows = cur.fetchone()
    rows = count_rows[0]
    if rows > 0:
        # print("rows was above 0")
        dateexist = True
    cur.close()
    conn.close()

    #  IF exist - get current numbers
    if dateexist:
        conn = mysql.connector.connect(**config)
        cur = conn.cursor()
        cur.execute("SELECT * FROM ru_study_count WHERE date='"+d+"'")
        for w in cur:
            # print(w)
            current_correct = w[2]
            current_incorrect = w[3]
            break
        cur.close()
        conn.close()

    #  UPDATE current numbers
    if toggle == 0:
        current_correct += 1
    else:
        current_incorrect += 1

    #  Actually update the DB
    try:
        conn = mysql.connector.connect(**config)
        cur = conn.cursor()
        if dateexist:
            query = "UPDATE ru_study_count SET correct='"+str(current_correct)+"', incorrect='"+str(current_incorrect)+"'"
            query += " WHERE date='"+d+"'"
            # print(query)
            cur.execute(str(query))
            conn.commit()
        else:  # ELSE STATEMENT UNTESTED ATM
            query = "INSERT INTO ru_study_count (date,correct,incorrect)"
            query += " VALUES ('"+d+"',"+str(current_correct)+","+str(current_incorrect)+")"
            # print(query)
            cur.execute(str(query))
            conn.commit()
        cur.close()
        conn.close()
    except Exception as e:
        print(e)


def query_active_words(use_db, arry):
    global Only_Old_Entries
    global year
    conn = mysql.connector.connect(**config)
    cur = conn.cursor()
    if Only_Old_Entries:
        cur.execute("SELECT * FROM " + use_db + " WHERE active='1' AND date NOT LIKE '%"+str(year)+"%' ORDER BY id ASC")
    else:
        cur.execute("SELECT * FROM " + use_db + " WHERE active='1' ORDER BY id ASC")
    for w in cur:
        arry.append(w)
    cur.close()
    conn.close()


def get_cat_count():
    c = 0
    conn = mysql.connector.connect(**config)
    cur = conn.cursor()
    cur.execute("SELECT * FROM categories ORDER BY id ASC")
    for _ in cur:
        c += 1
    cur.close()
    conn.close()
    return c


def all_words(use_db, arry):
    conn = mysql.connector.connect(**config)
    cur = conn.cursor()
    cur.execute("SELECT * FROM " + use_db + " ORDER BY id ASC")
    for w in cur:
        arry.append(w)
    cur.close()
    conn.close()


def toggle_trouble(id, t, table):
    conn = mysql.connector.connect(**config)
    cur = conn.cursor()
    query = ""
    if t == 0 or t == '0':
        query = "UPDATE " + table + " SET trouble_word='0' WHERE id='" + str(id) + "'"
    elif t == 1 or t == '1':
        query = "UPDATE " + table + " SET trouble_word='1' WHERE id='" + str(id) + "'"
    print("Toggled trouble_word to", t)
    cur.execute(str(query))
    conn.commit()
    cur.close()
    conn.close()

'''
################################################
              DECK BUILDING
################################################
'''


def build_review_deck(inArr, outArr, isRU=False):
    for i in range(0, len(inArr)):
        if inArr[i][1] != "":
            if inArr[i][3] != "":
                if inArr[i][4] != "":
                    try:
                        wd = datetime.datetime.strptime(inArr[i][8], '%Y %m %d')
                    except Exception as e:
                        if SHOW_EXCEPTION_ERRORS:
                            print("ID #" + str(inArr[i][0]) + " - Error in time format, reverting to default.\n" + e)
                        wd = datetime.datetime.strptime("2009 11 02", '%Y %m %d')
                    ratio = get_ratio(inArr[i][11], inArr[i][12])
                    d = set_day_limit(ratio)
                    chk_time = datetime.datetime.now()-datetime.timedelta(days=d)
                    try:
                        trbl = int(inArr[i][10])
                    except Exception as e:
                        if SHOW_EXCEPTION_ERRORS:
                            print("Trouble toggle is not a number for id #" + str(inArr[i][0]) + ".\n" + e)
                        else:
                            print("Trouble toggle is not a number for id #" + str(inArr[i][0]) + ".")
                    if wd < chk_time or trbl == 1:
                        outArr.append(inArr[i])
                else:
                    if SHOW_EXCEPTION_ERRORS:
                        print("ID: " + str(inArr[i][0]) + " has blank audio field.")
            else:
                if SHOW_EXCEPTION_ERRORS:
                        print("ID: " + str(inArr[i][0]) + " has blank en field.")
        else:
            if SHOW_EXCEPTION_ERRORS:
                    print("ID: " + str(inArr[i][0]) + " has blank RU field.")
    if ADD_CUSTOM_RU_ITEMS and len(outArr) > 0 and isRU:
        for i in custom_force_add_RU:
            add_custom_entry(i, lang_tables["ru"], outArr)


def build_custom_deck(sql, deck, ru=False):
    # print("Building custom deck with SQL of\n" + sql)
    global OnRU
    conn = mysql.connector.connect(**config)
    cur = conn.cursor()
    cur.execute(str(sql))
    for i in cur:
        # print(i)
        if i[1] != "":
            if i[3] != "":
                if str(i[4]) != "":
                    deck.append(i)
                else:
                    if SHOW_EXCEPTION_ERRORS:
                        print("ID: " + str(i[0]) + " has blank audio field.")
            else:
                if SHOW_EXCEPTION_ERRORS:
                    print("ID: " + str(i[0]) + " has blank en field.")
        else:
            if SHOW_EXCEPTION_ERRORS:
                print("ID: " + str(i[0]) + " has blank RU field.")

    cur.close()
    conn.close()
    if ADD_CUSTOM_RU_ITEMS and len(deck) > 0 and ru is True:
        for i in custom_force_add_RU:
            add_custom_entry(i, lang_tables["ru"], deck)


def build_trouble_deck(table, deck):
    global OnRU
    conn = mysql.connector.connect(**config)
    cur = conn.cursor()
    query = "SELECT * FROM " + table + " WHERE trouble_word='1' AND active='1'"
    cur.execute(str(query))
    for i in cur:
        if i[1] != "":
            if i[3] != "":
                if str(i[4]) != "":
                    deck.append(i)
                else:
                    if SHOW_EXCEPTION_ERRORS:
                        print("ID: " + str(i[0]) + " has blank audio field.")
            else:
                if SHOW_EXCEPTION_ERRORS:
                        print("ID: " + str(i[0]) + " has blank en field.")
        else:
            if SHOW_EXCEPTION_ERRORS:
                    print("ID: " + str(i[0]) + " has blank RU field.")

    cur.close()
    conn.close()
    # if ADD_CUSTOM_RU_ITEMS and len(deck) > 0 and table == lang_tables["ru"]:
    if ADD_CUSTOM_RU_ITEMS and len(deck) > 0 and table == lang_tables["ru"]:
        for i in custom_force_add_RU:
            add_custom_entry(i, lang_tables["ru"], deck)


def add_custom_entry(id, table, deck):
    conn = mysql.connector.connect(**config)
    cur = conn.cursor()
    query = "SELECT * FROM " + table + " WHERE id='" + str(id) + "'"
    cur.execute(str(query))
    for i in cur:
        deck.append(i)
    cur.close()
    conn.close()


'''
################################################
                 FORMATTING
################################################
'''


def transliterate(word, translit_table):
    converted_word = ''
    for char in word:
        transchar = ''
        if char in translit_table:
            transchar = translit_table[char]
        else:
            transchar = char
        converted_word += transchar
    return converted_word


def check_for_whitespace(row, table):
    rows_to_fix = [1, 2, 3, 4, 5, 6]
    for r in rows_to_fix:  # using a list over a range() in case I remove a middle number
        word = row[r]
        if word is not None:
            if len(word) > 0:
                if word != word.strip():
                    question = input("'" + word + "'   ---- seems to have whitespace, fix it?")
                    if question in do_it:
                        word = word.strip()
                        tr = tables(r)
                        result = update_item(word, table, tr, row[0])
                    else:
                        return False
                    if result:
                        return True
                    else:
                        print("Unknown error updating table")
                        return False
    return False


def rebuild_list_item(item, id, outArr):
    newItem = [
        item[0], item[1], item[2], item[3], item[4], item[5], item[6], item[7], item[8], item[9], 1, item[11], item[12]
    ]
    del outArr[id]
    outArr.append(newItem)


def split_long_sentences(txt):
    if ' ... ' not in txt:
        if len(txt) > long:
            txt = txt.replace(". ", ".\n")
            txt = txt.replace("! ", "!\n")
            txt = txt.replace("? ", "?\n")
        arr = txt.splitlines()
        if len(arr) > 1:
            txt = ""
            for i in range(0,len(arr)):
                if arr[i].startswith('|'):
                    arr[i] = arr[i].replace("| ", '')
                if arr[i].startswith('/'):
                    arr[i] = arr[i].replace("/ ", '')
                if len(arr[i]) > long:
                    arr[i] = arr[i].replace(", ", ",\n")
                if i < (len(arr)-1):
                    txt = txt + arr[i] + "\n"
                else:
                    txt = txt + arr[i]
    return txt


def sentence_case(txt):  # Not Implemented - Most likely will not use - But keeping in case
    rtn = re.split('([.!?] *)', txt)
    final = ''.join([i.capitalize() for i in rtn])
    return final


'''
################################################
                 MISC STUFF
################################################
'''


def tables(num):
    switcher = {1: "ru", 2: "pro", 3: "en", 4: "audio", 5: "word_type", 6: "word_gender"}
    return switcher.get(num, "error")


def str_to_bool(s):
    t = [True, 'True', 'true', 't', 'T', 1, '1']
    f = [False, None, 'False', 'f', 'F', 'false', '0', 0, 'null', '']
    if s in t:
        return True
    elif s in f:
        return False
    else:
        return True


def get_ratio(x, y):
    if x > 0 and y == 0:
        return float(x)
    elif (x == 0 and y > 0) or (x == 0 and y == 0):
        return float(0)
    else:
        return float(x) / float(y)


def set_day_limit(ratio):
    if ratio > 12.0:
        d = 30
    elif ratio > 8.0:
        d = 21
    elif ratio > 5.0:
        d = 16
    elif ratio > 3.0:
        d = 12
    elif ratio > 1.5:
        d = 7
    else:
        d = 2
    return d


def check_trouble_ratio(arr, ratio, tbl, id, outArr):
    trouble = int(arr[10])
    if ratio >= 3.0 and trouble == 1:
        toggle_trouble(int(arr[0]), 0, tbl)
    elif ratio < 2.0 and trouble == 0:
        toggle_trouble(int(arr[0]), 1, tbl)
        rebuild_list_item(arr, id, outArr)


def my_randint(a):
    lst = []
    for i in range(1, 20):
        c = randint(0, a-1)
        lst.append(c)
    return choice(lst)


def locate(pattern, root):
    for path, dirs, files in os.walk(os.path.abspath(root)):
        for filename in fnmatch.filter(files, pattern):
            print(os.path.join(path, filename))
            return str(os.path.join(path, filename))

def strip_to_nums(a):
    '''
    If words are allowed, just use an if, and if returned is '', than don't accept new. use input string
    Example:
        corr_incorr = input(str("Did you get this correct?"))
        cinc_temp = strip_to_nums(corr_incorr)
        if cinc_temp is not '':
            corr_incorr = cinc_temp

    This will use cinc_temp if a number or corr_incorr if a letter/word
    '''
    nums = re.compile('\d+(?:\.\d+)?')
    result = nums.findall(a)
    if len(result) > 0:
        return result[0]
    else:
        return ''

def get_date():
    today = datetime.datetime.now()
    year = today.year
    month = today.month
    day = today.day
    return [year, month, day]


fill_forced_words()  # Maybe I shouldn't make this forced and make it manual? Not sure atm
