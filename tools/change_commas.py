choice = 2


def do_formatting(txt, change):
    txt = txt.replace(",", " " + change)
    return txt

while True:
    q = input("Change to 1: /  2: |  ")
    try:
        a = int(q)
    except ValueError:
        print("Not a number")
        continue

    if a is 1 or a is 2:
        choice = a
    else:
        print("Invalid option")
        continue

    s = input("Input string with commas: ")
    c = ""
    if choice is 1:
        c = "/"
    else:
        c = "|"
    print(do_formatting(s, c) + "\n")


