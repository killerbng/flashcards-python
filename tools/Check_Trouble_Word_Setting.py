from Russian_Flash_Cards import *

RU = True
default_table = lang_tables['ru']


class CheckTroubleWordSetting:
    tbl = lang_tables['ru']  # defaults to RU table, but can be set on new instance
    rate_diff = 3  # X correct for every 1 incorrect to leave trouble_word state.

    def run(self):
        conn = mysql.connector.connect(**config)
        cur = conn.cursor()
        query = "SELECT * FROM " + self.tbl + " ORDER BY id ASC"
        cur.execute(str(query))
        for i in cur:
            if i[10] is '1' or i[10] is 1 or i[10] is "":
                correct = i[11]
                incorrect = i[12]
                if correct > 0:
                    check = incorrect * self.rate_diff
                    if check <= correct - 1 and correct > 3:
                        p = "ID: " + str(i[0]) + " MIGHT not be a trouble word."
                        p += "Correct: " + str(correct) + " Incorrect: " + str(incorrect) + " Check Value: " + str(check)
                        print(p)
        cur.close()
        conn.close()

if __name__ == "__main__":
    a = CheckTroubleWordSetting()
    if RU:
        a.tbl = default_table
    else:
        a.tbl = input("Enter table name.. ie 'ru_words':  ")
    if a.tbl is "" or a.tbl is None:
        a.tbl = default_table
    a.run()
