"""

    Change the casing of a string
    Not specific to this program
    but a nice helper file.

"""

while True:
    c = input("What casing? [0] Exit [1] Title [2] lowercase [3] uppercase [4] Standard sentence")
    if c == '0':
        exit()

    q = input("Enter string: ")

    if c == '1':
        print("\n" + q.title() + "\n")
    elif c == '2':
        print("\n" + q.lower() + "\n")
    elif c == '3':
        print("\n" + q.upper() + "\n")
    elif c == '4':  # Things like names and counties will be set to lowercase and you will have to change it manually
        q = q.lower()
        q = list(q)
        q[0] = str(q[0]).upper()
        q = "".join(q)
        print("\n" + q + "\n")
