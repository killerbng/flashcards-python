# This Python file uses the following encoding: utf-8
from Russian_Flash_Cards import *
import mysql.connector

"""
    NOT IMPLEMENTED ANYWHERE
"""

class ru_ave:
    @staticmethod
    def run():
        conn = mysql.connector.connect(**config)
        cur = conn.cursor()

        last_row = 0
        prev_day = 0

        total = 0
        count = 0

        sql = "SELECT * FROM ru_study_count"
        cur.execute(sql)
        for e in cur:
            total += e[2] + e[3]
            count = count + 1

        cur.close()
        conn.close()

        #  printtotal)
        #  print(count)
        ave = total / count
        print("An average of " + str(int(ave)) + " entries reviewed per day.")

if __name__ == "__main__":
    a = ru_ave()
    a.run()