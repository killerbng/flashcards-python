# This Python file uses the following encoding: utf-8
from Russian_Flash_Cards import *
from Russian_Flash_Cards.tools import *
import traceback
import sys
# from Not_Add_SS import *


class Debugger():

    queARA = []
    queBR = []
    queCN = []
    queDE = []
    queESP = []
    queFI = []
    queLTV = []
    queRU = []
    queSE = []
    queSPA = []
    queUKR = []

    ARA = []
    BR = []
    CN = []
    DE = []
    ESP = []
    FI = []
    LTV = []
    RU = []
    SE = []
    SPA = []
    UKR = []

    ARAT = []
    BRT = []
    CNT = []
    CNT = []
    DET = []
    ESPT = []
    FIT = []
    LTVT = []
    RUT = []
    SET = []
    SPAT = []
    UKRT = []

    # MAKE SURE ONLY ONE OF THESE BOOLEANS IS TRUE (IF ANY)!
    ONLY_RU = True
    ONLY_DE = False
    RU_DE = False
    DoResourceList = False  # No longer used?

    def do_checks(self, k, v):
        ru_default_table = lang_tables["ru"]
        de_default_table = lang_tables["de"]
        cn_default_table = lang_tables["cn"]
        if self.ONLY_RU:
            question = "y"
        elif self.ONLY_DE:
            question = "y"
        elif self.RU_DE:
            question = "y"
        else:
            question = input(str("\ndebug " + str(v)+"?"))
        if question in do_it:
            #
            print("\nChecking " + k + " for general category errors.")
            try:
                check_for_bad_cats(lang_tables[k])
            except Exception as e:
                print("An unknown error happened, while checking cats in " + v + "\n")
                print(e)
            #
            print("\nChecking " + k + " for date errors.")
            try:
                check_for_invalid_dates(lang_tables[k])
            except Exception as e:
                print("An unknown error happened, while checking dates in " + v + "\n")
                print(e)
            #
            if lang_tables[k] is ru_default_table:
                print("\nChecking for stress marks...")
                try:
                    find_stress_marks(lang_tables[k])
                except Exception as e:
                    print("An unknown error happened, while checking for stress marks in " + v + "\n")
                    print(e)
            #
            """
            print("\nChecking for bad whitespace...")
            try:
                find_whitespace_items(lang_tables[k], ONLY_RU)
            except Exception as e:
                print("An unknown error happened, while checking for whitespace in " + v + "\n")
                print(e)
            """
            # Long if commented out because tts.lang = "XX-XX" is not set correctly for some reason
            #if lang_tables[k] is ru_default_table or lang_tables[k] is de_default_table or lang_tables[k] is cn_default_table:
            if lang_tables[k] is ru_default_table:
                print("\nFinding all entries with no sound files.")
                try:
                    check_for_no_audio(lang_tables[k])
                except Exception as e:
                    print("An unknown error happened, while checking audio in " + v + "\n")
                    print(e)
            #
            print("\nLooking for bad inactive status")
            try:
                a_test = []
                all_words(lang_tables[k],  a_test)
                check_trouble_words(a_test)
            except Exception as e:
                print("An unknown error happened, well inactive trouble words in " + v + "\n")
                print(e)
            #
            print("\nLooking for missing ID numbers...")
            try:
                find_missing_ids(lang_tables[k])
            except Exception as e:
                print("An unknown error happened, well finding missing ID #'s in " + v + "\n")
                print(e)
            #
            print("\nCheck for questions in wrong categories...")
            try:
                find_questions_with_wrong_cat(lang_tables[k])
            except Exception as e:
                print("An unknown error happened, well looking at question categories in " + v + "\n")
                print(e)
            #
            print("\nChecking for unused audio files...")
            try:
                find_unused_audio(lang_tables[k])
            except Exception as e:
                print("An unknown error happened, while checking for unused audio files in " + v + "\n")
                print(str(e))
            #
            print("\nChecking verb word_type's for bad category...")
            try:
                bad_verb_cat(lang_tables[k])
            except Exception as e:
                print("An unknown error happened, while checking verb categories in " + v + "\n")
                print(e)
            #
            print("\nChecking for QaA not in correct category...")
            try:
                find_bad_qa(lang_tables[k])
            except Exception as e:
                print("An unknown error happened, while checking QaA cats in " + v + "\n")
                print(e)
            #
            if FIND_UNKNOWN_TEXT:
                print("\nChecking for unknown entries...")
                try:
                    find_unknown_text(lang_tables[k])
                except Exception as e:
                    print("An unknown error happened, while checking for unknown translations in " + v + "\n")
                    print(e)
            #
            print("\nChecking for blank entries...")
            try:
                find_blank_entries(lang_tables[k])
            except Exception as e:
                print("An unknown error happened, while checking for blank entries in " + v + "\n")
                print(e)
            #
            print("\nChecking for bad/invalid chars...")
            try:
                find_bad_chars(lang_tables[k])
            except Exception as e:
                print("An unknown error happened, while checking for invalid characters in " + v + "\n")
                print(e)
            #
            print("\nChecking for adverbs set as nouns...")
            try:
                find_adverbs_as_noun(lang_tables[k])
            except Exception as e:
                print("An unknown error happened, while checking for adverbs set as nouns in " + v + "\n")
                print(e)
            #
            if lang_tables[k] is ru_default_table:
                print("\nChecking for empty ranked row...")
                try:
                    find_empty_ranked_rows()
                except Exception as e:
                    print("An unknown error happened, while checking for empty ranked row in " + v + "\n")
                    print(e)
            #
            try:
                print("\nChecing for TTS audio files to replace...")
                find_tts_audio(lang_tables[k])
            except:
                print("An unknown error happened, while checking for TTS audio to replace.")
            #
            if lang_tables[k] is ru_default_table:
                print("\nChecking for empty pro row...")
                try:
                        check_for_empty_pro(lang_tables[k])
                except Exception as e:
                    print("An unknown error happened, while checking for empty pro row in " + v + "\n")
                    print(e)
            #
            print("\nChecking for unmarked formal informal tags")
            try:
                find_unmarked_formal_informal(lang_tables[k])
            except Exception as e:
                print("An unknown error happened, while checking for unmarked formal informal tags in " + v + "\n")
                print(e)
            #
            print("\nChecking for incomplete word type/genders")
            try:
                find_false_type_gender(lang_tables[k], k)
            except Exception as e:
                print("An unknown error happened, while checking for incomplete word type/genders in " + v + "\n")
                print(e)
            #
            print("\nChecking for phrases set as nouns.")
            try:
                find_phrase_as_noun(lang_tables[k])
            except Exception as e:
                print("An unknown error happened, while checking for phrases set as nouns in " + v + "\n")
                print(e)
            #
            if lang_tables[k] is ru_default_table and self.DoResourceList is True:
                print("\nListing RU Reference links to do ...")
                try:
                    lurlrl = ListURLResourceList()
                    lurlrl.run()
                except Exception as e:
                    print("An unknown error happened, well attempting to read and list referenced to get in " + v + "\n")
                    print(e)
            #
            '''
            if lang_tables[k] is ru_default_table:
                print("\nGrabbing 1 SS to add to DB")
                try:
                    nass = NotAddedScreenshots()
                    nass.query_images()
                    r = my_randint(len(nass.images))
                    file = nass.images[r]
                    if ON_LINUX:
                        call(["xdg-open", file])
                    else:
                        os.startfile(file)
                except Exception as e:
                    print("An unknown error happened, well grabbing 1 SS to add to DB in " + v + "\n")
                    print(e)
            '''

    def run(self):
        global ONLY_RU
        global ONLY_DE
        global RU_DE
        #
        if self.ONLY_RU:
            self.do_checks("ru", lang_tables["ru"])
        elif self.ONLY_DE:
            self.do_checks("de", lang_tables["de"])
        elif self.RU_DE:
            self.do_checks("ru", lang_tables["ru"])
            self.do_checks("de", lang_tables["de"])
        else:
            for k, v in lang_tables.items():
                self.do_checks(k, v)
        #
        print("\n\nDEBUGGING DONE!")
        quit()

if __name__ == "__main__":
    #a = Debugger()
    #a.run()
    find_false_type_gender("ru_words", "ru")