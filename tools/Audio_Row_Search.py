# This Python file uses the following encoding: utf-8
from Russian_Flash_Cards import *

class AudioRowSearch():

    exit_quit = ['q', 'QUIT', 'e', 'exit', 'E', 'EXIT', 'b', 'back']

    queARA = []
    queBR = []
    queDE = []
    queESP = []
    queFI = []
    queLTV = []
    queRU = []
    queSPA = []
    queUKR = []

    ARA = []
    BR = []
    DE = []
    ESP = []
    FI = []
    LTV = []
    RU = []
    SPA = []
    UKR = []

    ARAT = []
    BRT = []
    DET = []
    ESPT = []
    FIT = []
    LTVT = []
    RUT = []
    SPAT = []
    UKRT = []

    def __init__(self):
        try:
            #query_active_words(lang_tables['br'], self.queBR)
            #query_active_words(lang_tables['de'], self.queDE)
            #query_active_words(lang_tables['esp'], self.queESP)
            #query_active_words(lang_tables['fi'], self.queFI)
            #query_active_words(lang_tables['ltv'], self.queLTV)
            query_active_words(lang_tables['ru'], self.queRU)
            #query_active_words(lang_tables['spa'], self.queSPA)
            #query_active_words(lang_tables['ukr'], self.queUKR)
        except Exception as e:
            if SHOW_EXCEPTION_ERRORS:
                print("Error while running query_active_words()\n" + str(e))
            else:
                print("Database is most likely down.\nTo see the full error, change SHOW_EXCEPTION_ERRORS to True\n")
            quit()
        # Build regular decks
        try:
            #build_review_deck(self.queBR, self.BR)
            #build_review_deck(self.queDE, self.DE)
            #build_review_deck(self.queESP, self.ESP)
            #build_review_deck(self.queFI, self.FI)
            #build_review_deck(self.queLTV, self.LTV)
            build_review_deck(self.queRU, self.RU)
            #build_review_deck(self.queSPA, self.SPA)
            #build_review_deck(self.queUKR, self.UKR)
        except Exception as e:
            if SHOW_EXCEPTION_ERRORS:
                print("Error while running build_review_deck()\n" + e.args)
            else:
                print("There was an error building review decks.\nChange SHOW_EXCEPTION_ERRORS to True for more info.\n")
            quit()
        # Build trouble decks
        try:
            #build_trouble_deck(lang_tables['br'], self.BRT)
            #build_trouble_deck(lang_tables['de'], self.DET)
            #build_trouble_deck(lang_tables['esp'], self.ESPT)
            #build_trouble_deck(lang_tables['fi'], self.FIT)
            #build_trouble_deck(lang_tables['ltv'], self.LTVT)
            build_trouble_deck(lang_tables['ru'], self.RUT)
            #build_trouble_deck(lang_tables['spa'], self.SPAT)
            #build_trouble_deck(lang_tables['ukr'], self.UKRT)
        except Exception as e:
            if SHOW_EXCEPTION_ERRORS:
                print(e)
            else:
                print("There was an error building trouble review decks.\n" +
                      "To see the full error, change SHOW_EXCEPTION_ERRORS to True\n")
            # quit()  # NOT needed when in class?

    def run(self):
        global RU
        while True:
            find_me = input("\nFind what? ")
            if find_me in self.exit_quit:
                quit()
            print()
            for w in self.RU:
                if w[4].find(find_me) > -1:
                    print("#" + str(w[0]) + "  ~~~  " + str(w[4]))


if __name__ == "__main__":
    a = AudioRowSearch()
    a.run()
