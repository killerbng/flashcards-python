from tts import *

#
# EDIT HERE
#
tts = TTS()
tts.REAL_TABLE = True # false for _copy
ON_RU = True # False for German / DE


#
# DO NOT EDIT BELOW HERE
#
word = "hello"
gender = "formal"

arry = []

if ON_RU:
    if tts.REAL_TABLE:
        tts.lang = "ru-ru"
        tts.l_table = "ru_words"
    else:
        tts.lang = "ru-ru"
        tts.l_table = "ru_words_copy"
elif not ON_RU:
    if tts.REAL_TABLE:
        tts.lang = "de-de"
        tts.l_table = "de_words"
    else:
        tts.lang = "de-de"
        tts.l_table = "de_words_copy"

# print(ON_RU)
# print(tts.lang)
# print(tts.l_table)


def get_row(id):
    global arry
    try:
        conn = mysql.connector.connect(**config)
        cur = conn.cursor()
        sql = "SELECT * FROM " + tts.l_table + " WHERE id='"+str(id)+"'"
        cur.execute(sql)
        # print(sql)
        for w in cur:
            arry = [w[0], w[1], w[3], w[5], w[6], w[4]]
        cur.close()
        conn.close()
    except:
        print("mysql error ffs")


while True:
    q = input(str("\nID? "))
    if q == "e" or q == "exit":
        exit()
    elif q is None or q == "":
        exit()
    else:
        id = int(q)
        get_row(id)
        #
        # print(str(id))
        # print(arry)
        # print(tts.REAL_TABLE)
        #
        text_ru = arry[1]
        text_en = arry[2]
        word_type = arry[3]
        word_gender = arry[4]
        audio_field = arry[5]
        text_ru = text_ru.replace(" ", "%20")
        text_ru = text_ru.encode('utf-8')
        ext = "." + tts.audio_type
        if audio_field is None or audio_field is "" or audio_field == "":
            audio_file = tts.format_audio_filename(text_en)
            if tts.lang is "ru-ru":
                audio_file = tts.audio_name_additions(audio_file, word_gender)
        else:
            audio_file = audio_field
            print("RE-USING AUDIO FILENAME " + audio_file)
        audio_file_full = audio_file + ext
        full_path = tts.save_path + audio_file_full
        if path.isfile(full_path) is False:

            full_url = tts.base_url+"key="+str(tts_api_key)+"&src="+text_ru.decode('utf-8')+"&hl="+tts.lang+"&r="\
                       + str(tts.speech_rate)+"&c="+tts.audio_type+"&f="+tts.audio_format

            print(str(arry[0]) + "  --  " + str(arry[1]) + "  --  " + str(arry[2]) + "  --  " + str(audio_file)
                  + "  --  " + str(word_type) + "  --  " + str(word_gender))

            tts.get_file(full_url, tts.save_path, audio_file_full)

            i = arry[0]
            row = "audio"
            tts.update_db(i, row, audio_file, word_type, word_gender)
        else:
            i = arry[0]
            print("Audio file exist, skipping " + str(i) + ", make manually if different entries.")
            tts.errors += 1
        print("\n" + str(q) + " done!\n")
        if tts.errors > 0:
            print("ERRORS HAPPENED, LOOK ABOVE!")


