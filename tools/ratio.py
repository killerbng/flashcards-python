from Russian_Flash_Cards import *
import mysql.connector

'''
    Get Ratio for your daily correct to incorrect studying
'''

correct = 0
incorrect = 0
count = 0

table = "ru_study_count"

conn = mysql.connector.connect(**config)
cur = conn.cursor()
cur.execute("SELECT * from " + table + " ORDER BY id DESC")
for i in cur:
    correct = correct + int(i[2])
    incorrect = incorrect + int(i[3])
    count = count + 1
cur.close()
conn.close()

new_correct = correct / count
new_incorrect = incorrect / count

if count is 1:
    use = "day"
else:
    use = "days"

print("Daily Correct to Incorrect average over " + str(count) + " " + use + ".")
print (repr(new_correct) + " / " + repr(new_incorrect))