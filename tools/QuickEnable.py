from Russian_Flash_Cards import *
import mysql.connector
from mysql.connector import errorcode

RU = True
table = ""

if not RU:
    pass  # TO DO
else:
    table = "ru_words"


def update(rid, trouble=False):  # Using this instead of the regular in main init because of a lot of differences
    global table
    try:
        conn = mysql.connector.connect(**config)
        cur = conn.cursor()
        if trouble:
            query = "UPDATE " + str(table) + " SET active='1', trouble_word='1' WHERE id='" + str(rid) + "'"
        else:
            query = "UPDATE " + str(table) + " SET active='1' WHERE id='" + str(rid) + "'"
        cur.execute(query)
        conn.commit()
        cur.close()
        conn.close()
        print("Successfully updated " + str(rid) + " to enabled.\n")
    except errorcode as e:
        print("Error enabling " + rid + "\n" + e + "\n")

while True:
    a = input(str("ID: "))
    a = a.strip()
    if " -t" in a:
        a = a.replace(" -t", "")
        update(a, True)
    else:
        update(a)

