from Russian_Flash_Cards import *
import mysql.connector


"""
    NEED TO MAKE IT ONLY ACCEPT LANGS WITH A TRANSLITE TABLE
    CURRENTLY SET IN DEBUGGER.py TO ONLY RUN FOR RU
"""


class ADD_PRO():

    RUSSIAN = False
    TEST_RU = False  # RUSSIAN must also be True to use this
    table = ""
    Internal = False

    autoyes = False
    fix_me = []  # id, ru
    fixed = []  # id, pro

    def get_table(self, tbl=""):
        if tbl is "":
            if self.RUSSIAN:
                if self.TEST_RU:
                    self.table = "ru_words_copy"
                else:
                    self.table = "ru_words"
                self.run()
            else:
                while True:
                    q = input("Which lang (short)?")
                    if q in lang_tables:
                        if q == "ru" or q == "ukr":
                            print("Table Accepted.\n")
                            self.table = q + "_words"
                            self.run()
                            break
                        else:
                            print("That table does not have a transliteration... \n")
                    else:
                        print("INVALID TABLE, TRY AGAIN\n")
        else:
            self.table = tbl
            self.run()

    def run(self):
        conn = mysql.connector.connect(**config)
        cur = conn.cursor()
        sql = "SELECT * FROM " + self.table + " WHERE ( pro='' OR pro IS NULL ) AND ru<>'' ORDER BY id ASC"
        # print(sql)
        # exit()
        cur.execute(sql)
        for w in cur:
            self.fix_me.append([w[0], w[1]])
        cur.close()
        conn.close()

        for itm in self.fix_me:
            # get pro
            pro = transliterate(itm[1], cyrillic_translit_RU)
            self.fixed.append([itm[0], pro])

        # update table
        if len(self.fix_me) > 0:
            if self.Internal:
                print(str(len(self.fix_me)))
                print(str(len(self.fixed)))
                print(self.fix_me)
                print(self.fixed)

            if self.autoyes:
                q2 = 'y'
            else:
                q2 = input("Update database?")
            if q2 in do_it:
                for itm in self.fixed:
                    print("Updating " + str(itm[0]) + " pro to " + itm[1])
                    update_item(itm[1], self.table, "pro", itm[0])

if __name__ == "__main__":
    a = ADD_PRO()
    a.Internal = True
    a.get_table()
