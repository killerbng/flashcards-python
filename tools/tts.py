# This Python file uses the following encoding: utf-8
from Russian_Flash_Cards import *
from Russian_Flash_Cards.db import *
import mysql.connector
from mysql.connector import errorcode
#import urllib.request
import requests
from os import path

class TTS():
    REAL_TABLE = True
    save_path = "C:/Users/Nathan Chambers/Desktop/tts/"
    base_url = "http://api.voicerss.org/?"
    speech_rate = -1
    lang = "ru-ru" # ru-ru | de-de | zh-cn  -- Below auto switches to de-de or zh-cn, when this is run from Debugger.py
    audio_type = "mp3"
    audio_format = "48khz_16bit_stereo"

    if lang is "ru-ru":
        if REAL_TABLE:
            l_table = "ru_words"
        else:
            l_table = "ru_words_copy"
    elif lang is "de-de":
        if REAL_TABLE:
            l_table = "de_words"
        else:
            l_table = "de_words_copy"
    elif lang is "zh-cn":
        if REAL_TABLE:
            l_table = "cn_words"
        else:
            l_table = "cn_words_copy"

    db_entries = []

    ru_ignore = [
        16461 # Audio doesn't work with such math problem layout
    ]
    de_ignore = []
    cn_ignore = []

    ignore = []

    if l_table is "ru_words":
        for i in ru_ignore:
           ignore.append(i)
    elif l_table is "de_words":
        for i in de_ignore:
           ignore.append(i)
    elif l_table is "cn_words":
        for i in cn_ignore:
           ignore.append(i)

    errors = 0

    def update_db(self, id, row, word, wt, wg):
        try:
            word = word.replace(".mp3", "")
            conn = mysql.connector.connect(**config)
            cur = conn.cursor()
            try:
                query = "UPDATE " + self.l_table + " SET " + str(row) + "='" + str(word) + "' WHERE id='" + str(id) + "'"
                cur.execute(query)
                conn.commit()
            except:
                print("Error in MySQL PASS 1")
            if wt is '' or wt is None or wt is False:
                try:
                    query = "UPDATE " + self.l_table + " SET word_type='False' WHERE id='" + str(id) + "'"
                    cur.execute(query)
                    conn.commit()
                except:
                    print("Error in MySQL PASS 2")
            if wg is '' or wg is None or wg is False:
                try:
                    query = "UPDATE " + self.l_table + " SET word_gender='False' WHERE id='" + str(id) + "'"
                    cur.execute(query)
                    conn.commit()
                except:
                    print("Error in MySQL PASS 3")
            cur.close()
            conn.close()
        except Exception as e:
            print("Error in update_db while adding to db... " + str(e))

    def get_file(self, url, folder, file):
        full_path = folder + file
        req = requests.get(url)
        file = open(full_path, 'wb')
        for chunk in req.iter_content(100000):
            file.write(chunk)
        file.close()

    def get_rows(self, arry):
        conn = mysql.connector.connect(**config)
        cur = conn.cursor()
        cur.execute("SELECT * FROM " + self.l_table + " ORDER BY id ASC")
        for w in cur:
            if w[4] is False or w[4] is None or w[4] is "":
                if w[1] is not False or w[1] is not None or w[1] is not "":
                    arry.append([w[0], w[1], w[3], w[5], w[6]])
        cur.close()
        conn.close()

    def format_audio_filename(self, s):
        a = ''.join(x for x in s.title() if not x.isspace())
        a = a.replace("?", "")
        a = a.replace("(", "")
        a = a.replace(")", "")
        a = a.replace("!", "")
        a = a.replace(".", "")
        a = a.replace("/", "")
        a = a.replace("¿", "")
        a = a.replace(",", "")
        a = a.replace("|", "")
        a = a.replace("\\", "")
        a = a.replace("-", "")
        a = a.replace("_", "")
        a = a.replace("—", "")
        a = a.replace("'", "")
        a = a.replace('"', "")
        a = a.replace(":", "")
        a = a.replace(";", "")
        a = a.replace("`", "")
        a = a.replace("’", "")
        a = a.replace(">", "MoreThan")
        a = a.replace("<", "LessThan")
        a = a.replace("*", "Times")
        a = a.replace("+", "Plus")
        a = a.replace("=", "Equals")
        return a

    def audio_name_additions(self, word, type, gender):
        if "informal" in gender or "informal" in type:
            word += "INF"
        if "formal " in gender or " formal" in gender or gender == "formal":
            word += "F"
        if "formal " in type or " formal" in type or type == "formal":
            word += "F"
        if "said by woman" in gender or "said by women" in gender:
            word += "F"
        if "masc" in gender or "towards male" in gender or "towards man" in gender or "boy" in gender or "masc" in gender or "by male" in gender or "by man" in gender:
            word += "M"
        if "fem" in gender or "towards female" in gender or "towards woman" in gender or "girl" in gender or "fem" in gender:
            word += "FM"
        if "neut" in gender or gender is "neut form":
            word += "NEUT"
        if "plural" in gender:
            word += "PL"
        if "i verbed" in gender or "I verbed" in gender or "I form" in gender:
            word += "IV"
        if "in " in gender or " in" in gender or gender is "in form" or gender is "in / on form" or gender is "in / at form" or gender is "in/at form" or gender is "in/on form":
            word += "IN"
        if "to form" in gender:
            word += "TO"
        if "we" in gender:
            word += "WF"
        if "imperfective" in gender:
            word += "IMPV"
        if "perfective " in gender or " perfective" in gender or gender == "perfective":
            word += "PV"
        if gender == "suffix form":
            word += "SFXF"
        if "past tense" in gender:
            word += "PT"
        if "short form" in gender:
            word += "SHORT"
        if "first-person" in gender:
            word += "FP"
        if "second-person" in gender:
            word += "SP"
        if "third-person" in gender:
            word += "TP"
        if "internet slang" in gender:
            word += "INTSL"
        if "genitive" in gender:
            word += "GEN"
        if "dative" in gender:
            word += "DAT"
        if "preposition" in gender or "prepositional" in gender:
            word += "PREP"
        if "1st" in gender:
            word += "1ST"
        if "2nd" in gender:
            word += "2ND"
        if "3rd" in gender:
            word += "3RD"
        if "of form" in gender:
            word += "OF"
        if "abbrev" in gender:
            word += "ABBREV"
        if "adjective" in gender:
            word += "ADJ"
        if "phrase" in type:
            word += "Phr"
        if "animate" in type:
            word += "AN"
        if "inanimate" in type:
            word += "IAN"
        if "noun" in type:
            word += "N"
        if "verb" in type:
            word += "V"
        if "accusative" in type:
            word += "ACCU"
        if "singular" in type:
            word += "SING"

        word += "TTS"

        return word

    def run(self):
        global ignore

        self.get_rows(self.db_entries)

        for e in self.db_entries:
            id = e[0]

            if id in self.ignore:
                print("Skipping ignored id " + str(id))
                continue

            text = e[1]

            if text is "":
                print("Skipping " + str(id) + ": no text in text field.")
                continue

            word_type = e[3]
            word_gender = e[4]
            text = text.replace(" ", "%20")
            text = text.encode('utf-8')
            ext = "." + self.audio_type
            audio_file = self.format_audio_filename(e[2])

            audio_file = self.audio_name_additions(audio_file, word_type, word_gender)

            audio_file_full = audio_file + ext
            full_path = self.save_path + audio_file_full

            if path.isfile(full_path) is False:

                full_url = self.base_url+"key="+str(tts_api_key)+"&src="+text.decode('utf-8')+"&hl="+self.lang+"&r="\
                           + str(self.speech_rate)+"&c="+self.audio_type+"&f="+self.audio_format

                print(str(e[0]) + "  --  " +  str(e[1]) + "  --  " + str(e[2]) + "  --  " + str(audio_file) + "  --  "\
                      + str(word_type) + "  --  " + str(word_gender))

                self.get_file(full_url, self.save_path, audio_file_full)

                i = e[0]
                row = "audio"
                self.update_db(i, row, audio_file, word_type, word_gender)
            else:
                print("Audio file exist in tts folder, skipping " + str(e[0]) + ", make manually if different entries.")
                self.errors += 1

        if self.errors > 0:
            print("ERRORS HAPPENED, LOOK ABOVE!")

class TTS_UNKNOWN(TTS):

    unknown = "UNKNOWN-TEXT"

    def get_rows(self, arry): # This bypasses TTS()
        conn = mysql.connector.connect(**config)
        cur = conn.cursor()
        cur.execute("SELECT * FROM " + self.l_table + " WHERE en='"+self.unknown+"' AND audio='"+self.unknown+"' ORDER BY id ASC")
        for w in cur:
            arry.append([w[0], w[1], w[3], w[4]]) # id, ru, en, audio
        cur.close()
        conn.close()

    def update_db(self, id, row, word): # This bypasses TTS()
        try:
            word = word.replace(".mp3", "")
            conn = mysql.connector.connect(**config)
            cur = conn.cursor()

            query = "UPDATE " + self.l_table + " SET " + str(row) + "='" + str(word) + "' WHERE id='" + str(
                id) + "'"
            cur.execute(query)
            conn.commit()

            cur.close()
            conn.close()
        except Exception as e:
            print("Error in update_db while adding to db... " + str(e))

    def run(self): # This bypasses TTS()
        self.get_rows(self.db_entries)

        _LIMITED_ = True # Only do a small amount at a time!
        limited_count = 25

        count = 0

        for e in self.db_entries:
            id = e[0]

            if _LIMITED_:
                if count > limited_count:
                    exit("Done!")

                count += 1

            if id in self.ignore:
                print("Skipping ignored id " + str(id))
                continue

            text = e[1]
            text = text.encode('utf-8')

            if text is "":
                print("Skipping " + str(id) + ": no text in text field.")
                continue

            audio = e[3]

            ext = "." + self.audio_type
            audio_file = self.unknown + "-" + str(id)

            audio_file_full = audio_file + ext
            full_path = self.save_path + audio_file_full
            #print(full_path)

            if path.isfile(full_path) is False:

                full_url = self.base_url + "key=" + str(tts_api_key) + "&src=" + text.decode(
                    'utf-8') + "&hl=" + self.lang + "&r=" \
                           + str(self.speech_rate) + "&c=" + self.audio_type + "&f=" + self.audio_format

                print(str(e[0]) + "  --  " + str(e[1]) + "  --  " + str(audio_file))

                self.get_file(full_url, self.save_path, audio_file_full) # url, folder, file

                row = "audio"
                self.update_db(id, row, audio_file) # id, row, word
            else:
                print("Audio file exist in tts folder, skipping " + str(e[0]) + ", make manually if different entries.")
                self.errors += 1

        if self.errors > 0:
            print("ERRORS HAPPENED, LOOK ABOVE!")


if __name__ == "__main__":
    #a = TTS()
    #a.run()
    b = TTS_UNKNOWN()
    b.run()