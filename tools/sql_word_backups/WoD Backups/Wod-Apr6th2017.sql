/*
MySQL Data Transfer
Source Host: 192.168.1.136
Source Database: WoD
Target Host: 192.168.1.136
Target Database: WoD
Date: 4/6/2017 2:59:53 PM
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for FunEasyRussian
-- ----------------------------
DROP TABLE IF EXISTS `FunEasyRussian`;
CREATE TABLE `FunEasyRussian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `month` int(11) DEFAULT NULL,
  `day` int(11) DEFAULT NULL,
  `ru` text CHARACTER SET utf8,
  `pro` text CHARACTER SET utf8,
  `en` text CHARACTER SET utf8,
  `type` text CHARACTER SET utf8,
  `gender` text CHARACTER SET utf8,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=209 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `FunEasyRussian` VALUES ('1', '10', '8', 'жарить', 'zharit\'', 'To Fry', 'verb', null);
INSERT INTO `FunEasyRussian` VALUES ('2', '10', '9', 'Лунка', 'lunka', 'Hole', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('3', '10', '10', 'Грабли', 'grabli', 'Rake', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('4', '10', '11', 'Закрыто', 'zakryto', 'Closed', 'noun', 'neuter');
INSERT INTO `FunEasyRussian` VALUES ('5', '10', '12', 'Медь', 'med\'', 'Copper', 'noun', null);
INSERT INTO `FunEasyRussian` VALUES ('6', '10', '13', 'Кресельный Подъёмник', 'kresel\'nyy pod\"yomnik', 'Chairlift', 'noun', null);
INSERT INTO `FunEasyRussian` VALUES ('7', '10', '14', 'Каменный Уголь', 'kamennyy ugol\'', 'Coal', 'noun', null);
INSERT INTO `FunEasyRussian` VALUES ('8', '10', '15', 'Отпечатки пальцев', 'otpechatki pal\'tsev', 'Fingerprint', 'noun', null);
INSERT INTO `FunEasyRussian` VALUES ('9', '10', '16', 'Классная доска', 'klassnaya doska', 'Blackboard', 'noun', null);
INSERT INTO `FunEasyRussian` VALUES ('10', '10', '17', 'Опорный прыжок', 'opornyy pryzhok', 'Vualt', 'noun', null);
INSERT INTO `FunEasyRussian` VALUES ('11', '10', '18', 'Подняться на судно', 'podnyat\'sya na sudno', 'To Board', 'verb', null);
INSERT INTO `FunEasyRussian` VALUES ('12', '10', '19', 'Потолок', 'potolok', 'Ceiling', null, null);
INSERT INTO `FunEasyRussian` VALUES ('13', '10', '20', 'Черника', 'chyer-NEE-ka', 'Blueberry', null, null);
INSERT INTO `FunEasyRussian` VALUES ('14', '10', '21', 'Нить', 'nit\'', 'Thread', null, null);
INSERT INTO `FunEasyRussian` VALUES ('15', '10', '22', 'прокол', 'prokol', 'Puncture', null, null);
INSERT INTO `FunEasyRussian` VALUES ('16', '10', '23', 'Палубо', 'paluba', 'Deck', null, null);
INSERT INTO `FunEasyRussian` VALUES ('17', '10', '24', 'Водитель', 'roditel\'', 'Driver', null, null);
INSERT INTO `FunEasyRussian` VALUES ('18', '10', '25', 'Мыло', 'mylo', 'Soap', 'noun', null);
INSERT INTO `FunEasyRussian` VALUES ('19', '10', '26', 'Команда', 'komanda', 'Team / Crew', null, null);
INSERT INTO `FunEasyRussian` VALUES ('20', '10', '27', 'Подошва', 'podoshva', 'Sole', null, null);
INSERT INTO `FunEasyRussian` VALUES ('21', '10', '28', 'Новости', 'novosti', 'News', null, null);
INSERT INTO `FunEasyRussian` VALUES ('22', '10', '29', 'Вагон', 'vagon', 'Carriage', null, null);
INSERT INTO `FunEasyRussian` VALUES ('23', '10', '30', 'Большой палец', 'bol\'shoy palets', 'Thumb', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('24', '10', '31', 'плавать', 'plavat\'', 'To Swim', 'verb', null);
INSERT INTO `FunEasyRussian` VALUES ('25', '11', '1', 'Одна Тысяча', 'odna tysyacha', 'One Thousand', null, null);
INSERT INTO `FunEasyRussian` VALUES ('26', '11', '2', 'Волна', 'volna', 'Wave', 'noun', null);
INSERT INTO `FunEasyRussian` VALUES ('27', '11', '3', 'Пылесос', 'pylesos', 'Vacuum Cleaner', null, null);
INSERT INTO `FunEasyRussian` VALUES ('28', '11', '4', 'Подпись', 'podpis\'', 'Signature', null, null);
INSERT INTO `FunEasyRussian` VALUES ('29', '11', '5', 'Строить', 'stroit\'', 'To Build', 'verb', null);
INSERT INTO `FunEasyRussian` VALUES ('30', '11', '6', 'Карман', 'karman', 'Pocket', 'noun', null);
INSERT INTO `FunEasyRussian` VALUES ('31', '11', '7', 'Запад', 'zapad', 'West', null, null);
INSERT INTO `FunEasyRussian` VALUES ('32', '11', '8', 'Рыболов', 'rybolov', 'Angler', null, null);
INSERT INTO `FunEasyRussian` VALUES ('33', '11', '9', 'Крага', 'kraga', 'Glove', 'noun', null);
INSERT INTO `FunEasyRussian` VALUES ('34', '11', '10', 'Шлем', 'shlem', 'Helmet', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('35', '11', '11', 'Бинт', 'bint', 'Bandage', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('36', '11', '12', 'вспышка', 'vspyshka', 'Flash', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('37', '11', '13', 'Площадка', 'ploshchadka', 'Field', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('38', '11', '14', 'Удар Кулаком', 'udar kulakom', 'Punch', 'noun', null);
INSERT INTO `FunEasyRussian` VALUES ('39', '11', '15', 'Ухо', 'ukho', 'Ear', 'noun', null);
INSERT INTO `FunEasyRussian` VALUES ('40', '11', '16', 'палец ноги', null, 'Toe', 'noun', null);
INSERT INTO `FunEasyRussian` VALUES ('41', '11', '17', 'Сосед', 'sosed', 'Neighbour', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('42', '11', '18', 'Серьги', 'ser\'gi', 'Earring', null, null);
INSERT INTO `FunEasyRussian` VALUES ('43', '11', '19', 'Дирижёр', 'dirizhor', 'Conductor', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('44', '11', '20', 'Весло', 'veslo', 'Oar', 'noun', null);
INSERT INTO `FunEasyRussian` VALUES ('45', '11', '21', 'Раковина', 'rakovina', 'Sink', null, null);
INSERT INTO `FunEasyRussian` VALUES ('46', '11', '22', 'Постоялец', 'postoyalets', 'Guest', null, null);
INSERT INTO `FunEasyRussian` VALUES ('47', '11', '23', 'Петух', 'petukh', 'Cockerel / Rooster', null, null);
INSERT INTO `FunEasyRussian` VALUES ('48', '11', '24', 'подгузник', 'podguznik', 'Nappy / Diaper', 'noun', null);
INSERT INTO `FunEasyRussian` VALUES ('49', '11', '25', 'Номер на двоих', 'nomer na dvoikh', 'Double Room', null, null);
INSERT INTO `FunEasyRussian` VALUES ('50', '11', '26', 'тренировться', 'trenirovat\'sya', 'To Train', 'verb', 'perfective');
INSERT INTO `FunEasyRussian` VALUES ('51', '11', '27', 'Колено', 'koleno', 'Knee', 'noun', null);
INSERT INTO `FunEasyRussian` VALUES ('52', '11', '28', 'Палатка', 'palatka', 'Tent', 'noun', null);
INSERT INTO `FunEasyRussian` VALUES ('53', '11', '29', 'Туман', 'tuman', 'Fog', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('54', '12', '1', 'Кассир', 'kassir', 'Cashier', null, null);
INSERT INTO `FunEasyRussian` VALUES ('55', '12', '2', 'Печень', 'pechen\'', 'Liver', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('56', '12', '3', 'Газета', 'gazeta', 'Newspaper', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('57', '12', '4', 'вокалист', 'vokalist', 'Lead Singer / Vocalist', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('58', '12', '5', 'официант', 'ofitsiant', 'Waiter', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('59', '12', '6', 'наушники', 'naushniki', 'Headphones', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('60', '12', '7', 'Лента', 'lenta', 'Ribbon', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('61', '12', '8', 'Кегля', 'keglya', 'Pin / Bowling Pin', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('62', '12', '9', 'ножницы', 'nozhnitsy', 'Scissors', 'plural noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('63', '12', '10', 'Ребро', 'rebro', 'Rib', 'noun', 'neuter');
INSERT INTO `FunEasyRussian` VALUES ('64', '12', '11', 'Открывалка', 'otkryvalka', 'Bottle Opener', null, null);
INSERT INTO `FunEasyRussian` VALUES ('65', '12', '12', 'Ячмень', 'yachmen\'', 'Barley', null, null);
INSERT INTO `FunEasyRussian` VALUES ('66', '12', '13', 'Завтра', 'zavtra', 'Tomorrow', null, null);
INSERT INTO `FunEasyRussian` VALUES ('67', '12', '14', 'Чихание', 'chikhaniye', 'Sneeze', null, null);
INSERT INTO `FunEasyRussian` VALUES ('68', '12', '15', 'Укус насекомого', 'ukus nasekomogo', 'Sting', null, null);
INSERT INTO `FunEasyRussian` VALUES ('69', '12', '16', 'Забивать', 'zabivat\'', 'To Hammer', 'verb', null);
INSERT INTO `FunEasyRussian` VALUES ('70', '12', '17', 'Йогурт', 'yogurt', 'Yoghurt', null, null);
INSERT INTO `FunEasyRussian` VALUES ('71', '12', '18', 'Леденцы', 'ledentsy', 'Sweets', null, null);
INSERT INTO `FunEasyRussian` VALUES ('72', '12', '19', 'Пепельница', 'pepel\'nitsa', 'Ashtray', null, null);
INSERT INTO `FunEasyRussian` VALUES ('73', '12', '20', 'Мат', 'mat', 'Mat', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('74', '12', '21', 'Отвёртка', 'otvortka', 'Screwdriver', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('75', '12', '22', 'длина', 'dlina', 'Length', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('76', '12', '23', 'Фрикадельки', 'frikadel\'ki', 'Meatballs', null, null);
INSERT INTO `FunEasyRussian` VALUES ('77', '12', '24', 'Седло', 'sedlo', 'Saddle', 'noun', 'neuter');
INSERT INTO `FunEasyRussian` VALUES ('78', '12', '25', 'очки', 'ochki', 'Glasses', 'noun', 'masculine plural');
INSERT INTO `FunEasyRussian` VALUES ('79', '12', '26', 'Лунка', 'lunka', 'Hole', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('80', '12', '27', 'Обед', 'obed', 'Lunch', null, null);
INSERT INTO `FunEasyRussian` VALUES ('81', '12', '28', 'пепельница', 'pepel\'nitsa', 'Ashtray', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('82', '12', '29', 'Нерв', 'nerv', 'Nerve', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('83', '12', '30', 'Пульс', 'pul\'s', 'Pulse', null, null);
INSERT INTO `FunEasyRussian` VALUES ('84', '12', '31', 'Планета', 'planeta', 'Planet', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('85', '1', '1', 'Вагон', 'vagon', 'Carriage', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('86', '1', '2', 'Бутылочка', 'butylochka', 'Bottle', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('87', '1', '3', 'Скачки', null, 'Horse race', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('88', '1', '4', 'Хоккей с шайбой', null, 'Ice hockey', null, null);
INSERT INTO `FunEasyRussian` VALUES ('89', '1', '5', 'Крага', 'kraga', 'Glove', null, null);
INSERT INTO `FunEasyRussian` VALUES ('90', '1', '6', 'Почтовый Ящик', 'pochtovyy yashchik', 'Postbox', null, null);
INSERT INTO `FunEasyRussian` VALUES ('91', '1', '7', 'С Рождеством', '', 'Merry Christmas', '', '');
INSERT INTO `FunEasyRussian` VALUES ('92', '1', '8', 'Скоро Помощь', 'skoraya pomoshch', 'Ambulance', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('93', '1', '9', 'Пинетки', 'pinetki', 'Booties', null, null);
INSERT INTO `FunEasyRussian` VALUES ('94', '1', '10', 'Ползунки', 'polzunki', 'Babygro', null, null);
INSERT INTO `FunEasyRussian` VALUES ('95', '1', '11', 'тасовать', 'tasovat\'', 'To Shuffle', 'verb', 'imperfective');
INSERT INTO `FunEasyRussian` VALUES ('96', '1', '12', 'эскалатор', 'eskalator', 'Escalator', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('97', '1', '13', 'дуб', 'dub', 'Oak', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('98', '1', '14', 'Туклипс', 'tuklip', 'Toe Clip', null, null);
INSERT INTO `FunEasyRussian` VALUES ('99', '1', '15', 'чёрный', null, 'Black', 'adjective', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('100', '1', '16', 'Резать', null, 'To Cut', 'verb', 'imperfective');
INSERT INTO `FunEasyRussian` VALUES ('101', '1', '17', 'Небоскрёб', 'neboskrob', 'Skyscraper', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('102', '1', '18', 'Квитанция', 'kvitantsiya', 'Receipt', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('103', '1', '19', 'Адвокат', 'advokat', 'Lawyer', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('104', '1', '20', 'Карате', 'karate', 'Karate', 'noun', 'neuter');
INSERT INTO `FunEasyRussian` VALUES ('105', '1', '21', 'Маяк', 'mayak', 'Lighthouse', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('106', '1', '22', 'преступник', 'prestupnik', 'Criminal', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('107', '1', '23', 'Гриль', 'gril\'', 'barbecue', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('108', '1', '24', 'пробка', 'probka', 'Cork', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('109', '1', '25', 'Майонеа', 'mayonez', 'Mayonnaise', null, null);
INSERT INTO `FunEasyRussian` VALUES ('110', '1', '26', 'чеснок', 'chesnok', 'Garlic', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('111', '1', '27', 'Кариес', 'kariyes', 'Decay / Cavity', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('112', '1', '28', 'чемпионат', 'chempionat', 'Champion', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('113', '1', '29', 'Сегодня', 'segodnya', 'Today', 'noun', 'neuter');
INSERT INTO `FunEasyRussian` VALUES ('114', '1', '30', 'масса', 'massa', 'Mass', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('115', '1', '31', 'заказывать', 'zakazyvat\'', 'To Order', 'verb', 'imperfective');
INSERT INTO `FunEasyRussian` VALUES ('116', '2', '1', 'Освещение', 'osveshcheniye', 'Light', 'noun', 'neuter');
INSERT INTO `FunEasyRussian` VALUES ('117', '2', '2', 'коллега', 'killega', 'Colleage', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('118', '2', '3', 'хирург', 'khirurg', 'Surgeon', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('119', '2', '4', 'Ответить', 'otvetit\'', 'to answer', 'verb', 'perfective');
INSERT INTO `FunEasyRussian` VALUES ('120', '2', '5', 'Взлетать', 'vzletat\'', 'to take off', 'verb', 'imperfective');
INSERT INTO `FunEasyRussian` VALUES ('121', '2', '6', 'Якорь', 'yakor\'', 'Anchor', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('122', '2', '7', 'Бросать', 'brosat\'', 'To throw', 'verb', 'imperfective');
INSERT INTO `FunEasyRussian` VALUES ('123', '2', '8', 'Багаж', 'bagazh', 'Luggage', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('124', '2', '9', 'развести огонь', 'razvesti ogon\'', 'To light a fire', 'verb', null);
INSERT INTO `FunEasyRussian` VALUES ('125', '2', '10', 'рис', 'ris', 'Rice', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('126', '2', '11', 'Свидетель', 'svidetel\'', 'Witness', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('127', '2', '12', 'Февраль', null, 'February', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('128', '2', '13', 'Звезда', 'zvezda', 'Star', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('129', '2', '14', 'саксофон', 'saksofon', 'Saxophone', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('130', '2', '15', 'друг', 'drug', '(male) Friend', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('131', '2', '16', 'соска', null, 'pacifier', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('132', '2', '17', 'река', 'reka', 'River', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('133', '2', '18', 'рынок', 'rynok', 'Market', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('134', '2', '19', 'Смывать', 'smyvat\'', 'To Rinse / To Wash off', 'verb', 'imperfective');
INSERT INTO `FunEasyRussian` VALUES ('135', '2', '20', 'Духи', 'dukhi', 'Perfume', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('136', '2', '21', 'Маска', 'maska', 'Mask', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('137', '2', '22', 'подозреваемый', 'podozrevayemyy', 'Suspect', 'Participle', 'present passive imperfective');
INSERT INTO `FunEasyRussian` VALUES ('138', '2', '23', 'храм', 'khram', 'Temple', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('139', '2', '24', 'Сингапур', 'Singapur', 'Singapore', 'proper noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('140', '2', '25', 'хронометр', 'khronometr', 'Timer', null, null);
INSERT INTO `FunEasyRussian` VALUES ('141', '2', '26', 'омлет', 'omlet', 'Scrambled Eggs / Omlet', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('142', '2', '27', 'зарегистрироваться', 'zaregistrirovat\'sya', 'To Check In', 'verb', 'perfective');
INSERT INTO `FunEasyRussian` VALUES ('143', '2', '28', 'пересечение', 'peresecheniye', 'Junction', 'noun', 'neuter');
INSERT INTO `FunEasyRussian` VALUES ('144', '2', '29', 'бить по мячу', 'bit\' po myachu', 'To kick', 'verb', null);
INSERT INTO `FunEasyRussian` VALUES ('145', '2', '30', 'безе', 'beze', 'meringue', 'noun', null);
INSERT INTO `FunEasyRussian` VALUES ('146', '2', '31', 'гардероб', 'garderob', 'wardrobe', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('147', '3', '1', 'леденцы', 'ledentsy', 'Candies', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('148', '3', '2', 'белка', 'belka', 'Squirrel', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('149', '3', '3', 'столица', 'stolitsa', 'Capital', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('150', '3', '4', 'опорный прыжок', 'opornyy pryzhok', 'Vault', null, null);
INSERT INTO `FunEasyRussian` VALUES ('151', '3', '5', 'губная помада', 'gubnaya pomada', 'Lip stick', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('152', '3', '6', 'будильник', 'budil\'nik', 'Alarm clock', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('153', '3', '7', 'сверлить', 'sverlit\'', 'To drill', 'verb', 'imperfective');
INSERT INTO `FunEasyRussian` VALUES ('154', '3', '8', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('155', '3', '9', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('156', '3', '10', 'Кухонный нож', 'kukhonnyy nozh', 'Kitchen Knife', null, null);
INSERT INTO `FunEasyRussian` VALUES ('157', '3', '11', 'Десна', 'dosna', 'Gum', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('158', '3', '12', 'Моццарелла', 'motstsarella', 'Mozzarella', null, null);
INSERT INTO `FunEasyRussian` VALUES ('159', '3', '13', 'дырокол', 'dyrokol', 'Hold punch', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('160', '3', '14', 'тонна', 'tonna', 'Tonne / Metric Ton', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('161', '3', '15', 'Монумент', 'monument', 'Monument', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('162', '3', '16', 'зелёные оливки', 'zelonyye olivki', 'Green Olive', null, null);
INSERT INTO `FunEasyRussian` VALUES ('163', '3', '17', 'нарезать', 'narezat\'', 'To Slice', 'verb', 'imperfective');
INSERT INTO `FunEasyRussian` VALUES ('164', '3', '18', 'младшии ребёнок', null, 'Toddler', null, null);
INSERT INTO `FunEasyRussian` VALUES ('165', '3', '19', 'свежая', 'svezhaya', 'Fresh', null, null);
INSERT INTO `FunEasyRussian` VALUES ('166', '3', '20', 'колбаски', 'kolbaski', 'Sausages', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('167', '3', '21', 'снять дом', 'snyat\' dom', 'To Rent', 'verb', null);
INSERT INTO `FunEasyRussian` VALUES ('168', '3', '22', 'смеяться', 'smeyat\'sya', 'To Laugh', 'verb', 'imperfective');
INSERT INTO `FunEasyRussian` VALUES ('169', '3', '23', 'Загар', 'zagar', 'Tan', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('170', '3', '24', 'Часы', 'chasy', 'Clock', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('171', '3', '25', 'жареный цыплёнок', 'zharenyy tsyplonok', 'Fried Chicken', 'phrase', null);
INSERT INTO `FunEasyRussian` VALUES ('172', '3', '26', 'шприц', 'shprits', 'Syringe', 'noun', 'masculine');
INSERT INTO `FunEasyRussian` VALUES ('173', '3', '27', 'газированная', 'gazirovannaya', 'Sparkling', null, null);
INSERT INTO `FunEasyRussian` VALUES ('174', '3', '28', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('175', '3', '29', 'прыгать', 'prygat\'', 'To Jump', 'verb', 'imperfective');
INSERT INTO `FunEasyRussian` VALUES ('176', '3', '30', 'трусы', 'trucy', 'Underpants / Knickers', 'noun', 'masc');
INSERT INTO `FunEasyRussian` VALUES ('177', '3', '31', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('178', '4', '1', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('179', '4', '2', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('180', '4', '3', 'барная стойка', 'barnaya stoyka', 'Bar Counter', null, null);
INSERT INTO `FunEasyRussian` VALUES ('181', '4', '4', 'картофель', 'kartofel\'', 'Potato', 'noun', 'masc');
INSERT INTO `FunEasyRussian` VALUES ('182', '4', '5', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('183', '4', '6', 'эмаль', 'emal\'', 'Enamel', 'noun', 'feminine');
INSERT INTO `FunEasyRussian` VALUES ('184', '4', '7', 'Барный стул', 'barnyy stul', 'Bar Stool', 'phrase', null);
INSERT INTO `FunEasyRussian` VALUES ('185', '4', '8', 'Бюстгальтер', 'byustal\'ter', 'Bra / brassiere', 'noun', 'masc');
INSERT INTO `FunEasyRussian` VALUES ('186', '4', '9', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('187', '4', '10', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('188', '4', '11', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('189', '4', '12', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('190', '4', '13', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('191', '4', '14', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('192', '4', '15', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('193', '4', '16', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('194', '4', '17', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('195', '4', '18', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('196', '4', '19', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('197', '4', '20', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('198', '4', '21', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('199', '4', '22', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('200', '4', '23', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('201', '4', '24', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('202', '4', '25', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('203', '4', '26', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('204', '4', '27', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('205', '4', '28', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('206', '4', '29', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('207', '4', '30', null, null, null, null, null);
INSERT INTO `FunEasyRussian` VALUES ('208', '4', '31', null, null, null, null, null);
