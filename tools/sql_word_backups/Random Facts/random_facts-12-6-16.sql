/*
MySQL Data Transfer
Source Host: 192.168.1.136
Source Database: ru_stuff
Target Host: 192.168.1.136
Target Database: ru_stuff
Date: 12/6/2016 7:29:13 PM
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for random_facts
-- ----------------------------
DROP TABLE IF EXISTS `random_facts`;
CREATE TABLE `random_facts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fact` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `random_facts` VALUES ('1', 'Хируко - MoP 1st to get all Achievement');
INSERT INTO `random_facts` VALUES ('2', 'Cyrillic charset:  utf8 - utf8_general_ci');
INSERT INTO `random_facts` VALUES ('3', 'Russia is an Orthodox Christian country');
INSERT INTO `random_facts` VALUES ('4', 'Public bathrooms are hard to find and when you do, you usually have to pay and get a pre-determinted amount of toilet paper.');
INSERT INTO `random_facts` VALUES ('5', '“RUnet” –  “RUssian InterNET.”');
INSERT INTO `random_facts` VALUES ('6', 'Many Russians celebrate Christmas Day on January 7 in the Gregorian calendar, which corresponds to December 25 in the Julian calendar.');
INSERT INTO `random_facts` VALUES ('7', 'Дед Мороз - Ded Moroz - Grand Father Frost - Like Father Christmas - Accompanied by Снегурочка - Snegurochk - Snow Maiden');
INSERT INTO `random_facts` VALUES ('8', 'Russia does not have sexual education classes in schools.');
INSERT INTO `random_facts` VALUES ('9', 'Never shake someones hand with a glove on.');
INSERT INTO `random_facts` VALUES ('10', 'Empty bottles must go UNDER a table. It is considered rude and bad luck to do otherwise.');
INSERT INTO `random_facts` VALUES ('11', 'Never sit at the side of the table.');
INSERT INTO `random_facts` VALUES ('12', 'Do not shake hands with a woman unless she initiates.');
INSERT INTO `random_facts` VALUES ('13', 'Maslenitsa is one of the oldest Slavic holidays, and came to be long before Christianity. The Maslenitsa holiday symbolizes bidding farewell to the winter and welcoming the spring. \r\n\r\nThe start date of Maslenitsa changes every year because it depends on the start date of Lent. Maslenitsa is not just one day but a whole week which precedes Lent. Maslenitsa is also called “crepe week” because crepes are the main dish eaten during the holidays.\r\n');
INSERT INTO `random_facts` VALUES ('14', 'Юрий Гагарин - first human to travel to outer space.');
INSERT INTO `random_facts` VALUES ('15', 'St. Petersburg is often referred to as Russia\'s \"most Western-style city.\"');
INSERT INTO `random_facts` VALUES ('16', 'One of the most ancient and most important Slavic holidays in Russia is the holiday of Ivan Kupala which is celebrated every year on the eve of July 6. \r\n\r\nThe symbols of the holiday of Ivan Kupala are fire, water, and grass, which is why a lot of traditions and beliefs are linked specifically to them. For example, the holiday’s main rite is jumping over a bonfire which has curative power on the Night of Ivan Kupala.');
INSERT INTO `random_facts` VALUES ('17', 'The Moscow International Film Festival first took place in 1935 under the name Soviet Film Festival in Moscow. However, it has been regularly held only since 1959. \r\n\r\nToday, the Film Festival takes place every year at the end of June and goes on for ten days. The main Film Festival award for the best film is the Golden St. George statue which depicts the fight between St. George the Victorious and the dragon. At the Film Festival, a Silver St. George is also awarded for the best actor or best director.');
INSERT INTO `random_facts` VALUES ('18', 'The Kremlin contains four palaces, four cathedrals, and some towers.');
INSERT INTO `random_facts` VALUES ('19', 'Before Easter, in the sixth week of Lent, on Sunday, the Christians in Russia celebrate a holiday called\r\n “Entrance of our Lord into Jerusalem.” \r\n\r\nIn Orthodox Christianity, the holiday of the Entrance of our Lord into Jerusalem is also called Palm Sunday\r\ndue to the tradition of celebrating the holiday with date palm branches. However, in Russia this holiday is\r\npopularly known as Willow Sunday. This is related to the fact that in Russia, willow branches are\r\ncustomarily used instead of palm branches.');
INSERT INTO `random_facts` VALUES ('20', 'In addition to St. Valentine’s Day, there is a holiday in Russia that is dedicated to family and marriage, called the Day of Family, Love, and Fidelity.\r\n\r\nOn the occasion of this holiday, the best families in Russia are presented with medals for love and fidelity. To receive a medal, the married couple should have been in a happy marriage for at least 25 years, raised their children as worthy members of Russian society, and served as models for the preservation of family values.');
INSERT INTO `random_facts` VALUES ('21', 'выставка достижений народного хозяйства / Exhibition of Economic Achievements\r\n\r\n\r\nEto bol\'shoy krasivyy park,na territorii kotorogo mnogo krasivykh zdaniy-pavil\'onov,gde ran\'she pokazyvali vse luchsheye,chego dostigal SSSR v promyshlennosti i t.d. Byl period \"zastoya i razgroma\",seychas vse vosstanovleno i usovershenstvovano.\r\nThis is a large beautiful park, on whose territory a lot of beautiful buildings, pavilions, where formerly showed the best of what the Soviet Union reached in the industry, etc. There was a period of \"stagnation and defeat\", now it is restored and improved.');
INSERT INTO `random_facts` VALUES ('22', 'Leo Tolstoy\r\n\r\nTolstoy is a Russian writer, the author of numerous novels and short stories. His two most famous works are the novels \"War and Peace\" and \"Anna Karenina.\"');
INSERT INTO `random_facts` VALUES ('23', 'In Russia, it\'s believed that a horseshoe hung above the front door will bring you luck. If you hang the horseshoe inside the house, it protects the people and things inside. If you hang it from the outside, it will keep thieves and other bad things out of your house.');
INSERT INTO `random_facts` VALUES ('24', 'Feast of the Transfiguration\r\n\r\nThe holiday Transfiguration of Jesus is observed every year on August 19.This holiday commemorates the transfiguration of Jesus Christ and symbolizes the beginning of fall, the changes in nature, and the need for personal change. \r\n\r\nOn this day, the priests wear white chasubles. The people bring the new fruit crop to church and have it blessed. On Apple Savior Day, people customarily prepare various apple-based dishes, like apple pies, preserves, pastries, and turnovers.');
INSERT INTO `random_facts` VALUES ('25', 'Russian Guild Exorsus was the 1st to beat (World 1st) Xavius on mythic mode, the last boss of the 1st raid in Legion.');
INSERT INTO `random_facts` VALUES ('26', 'Пельмени (Dumplings) are one of the most famous dishes in Russia. They are usually stuffed with meat, pork or beef, diced onion and garlic. It can be boiled, fried or baked in a clay pot.');
INSERT INTO `random_facts` VALUES ('27', 'The Russian alphabet contains 21 consonants, 10 vowels, and 2 letters with no sound.');
INSERT INTO `random_facts` VALUES ('28', 'Baikal Lake is the oldest lake in the world and was declared a UNESCO World Heritage Site in 1996.');
INSERT INTO `random_facts` VALUES ('29', 'There are 3 main types of compartments on Russian trains: 1st Class (SV), 2nd Class (coupe) and 3rd Class (Platzkart).');
INSERT INTO `random_facts` VALUES ('30', 'In Russia, it\'s believed that if a black cat runs across your path, you\'re going to have bad luck. This is because black cats are believed to be connected to mysterious and dark things.');
INSERT INTO `random_facts` VALUES ('31', 'Russians middle names are always the fathers first name + a suffix. So if you know someone\'s fathers name, you easily know their middle name.\r\nFor males: -ович, -евич, or -ьевич  (-ovich, -yevich, -\'yevich)\r\nFor females: -овна, -евна (-ovna, -yevna)\r\n\r\nA few examples:\r\nPerson is мужчина (Male)\r\nFather\'s name is Пётр (Peter)\r\nAbove Person\'s Middle Name is Петрович (Peterovich)\r\n\r\nPerson is женщина (female)\r\nFather\'s name is Антон (Anton)\r\nAvobe Person\'s Middle Name is Антоновна (Antonovna)');
INSERT INTO `random_facts` VALUES ('32', 'In Russia, it\'s believed that if you spill some salt, you are going to have a fight or quarrel with someone. \r\n\r\nThis is because in the past there were times when salt was as expensive as gold in Russia, so if someone spilt even a little bit, a fight seemed imminent.');
INSERT INTO `random_facts` VALUES ('33', 'In Russia, it\'s believed that when entering the house after the wedding for the first time, the groom should carry his bride over the threshold.\r\nThis is because the spirits of one\'s ancestors are believed to live near the entrance. \r\nSo, bringing the new wife (a new person) inside the house on your own hands is like introducing her to them.');
INSERT INTO `random_facts` VALUES ('34', 'In Russia, it\'s believed that if you break a mirror, 7 years of bad luck are waiting for you.\r\nThis is because in the 15th century, when glass mirrors were becoming popular, they were very expensive.');
INSERT INTO `random_facts` VALUES ('35', 'In Russia, it\'s believed that a pigeon sitting on your windowsill is a good sign, and nothing bad will happen in the near future.\r\nThis is because the pigeon is believed to be a symbol of peace and love in many countries. \r\nThere were also times when pigeons used to bring mail, so pigeons are still associated with good news.');
INSERT INTO `random_facts` VALUES ('36', 'In Russia, it\'s believed that if your right palm is itching, you will have to give away or lose some money.\r\nThis is because the right hand is considered to be the \"giving hand\" and the left one is \"taking hand.\"\r\nSo to get some money, you should scratch your left palm.');
INSERT INTO `random_facts` VALUES ('37', 'Driving in Russia with a dirty car is considered a criminal offense.');
INSERT INTO `random_facts` VALUES ('38', 'The tradition of Halloween came to Russia mostly from American and European movies.\r\nSo you will often see some Halloween themed parties at night clubs, decorations at shops, or some parades on day Halloween, but it\'s still not widely celebrated in Russia.');
INSERT INTO `random_facts` VALUES ('39', 'National Unity Day is celebrated on November 14 and was instituted in 2004 with the federal law titled \"On the Days of Military Glory and Memorable Dates in Russia.” \r\n\r\nThis holiday is connected to the liberation of Moscow from the Polish invaders in 1612 by the people’s army led by Kuzma Minin and Dmitry Pozharsky.');
INSERT INTO `random_facts` VALUES ('40', 'One of the main reasons that Russian folk songs and dances are not very popular nowadays is that there are more than 300 ethnic groups in Russia and most of them have their own ethnic culture.');
INSERT INTO `random_facts` VALUES ('41', 'Peter I, also known as Peter the Great, ruled the Tsardom of Russia and then later the Russian Empire.');
INSERT INTO `random_facts` VALUES ('42', 'Chamomile is a very popular flower in Russia. \r\nIt\'s a symbol of the Day of Family, Love and Fidelity. It symbolizes purity and innocence and very often is given to loved ones. \r\nRussians also use chamomile for fortune-telling.');
