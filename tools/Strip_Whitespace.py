from Russian_Flash_Cards import *


#
# THIS SEEMS TO WORK NOW
# BUT I AM NOT SURE ABOUT THE ROWS WORKING RIGHT??
#

class StripWhitespace:

    # ONLY toggle these if running file directly
    ONLY_RU = False
    ONLY_RU_COPY = False

    words = []

    def run_checks(self, l_table):
        conn = mysql.connector.connect(**config)
        cur = conn.cursor()
        cur.execute("SELECT * FROM " + l_table + " ORDER BY id ASC")
        for i in cur:
            self.words.append(i)
        cur.close()
        conn.close()

        for w in self.words:
            result = check_for_whitespace(w, l_table)
            if result:
                print("Removed whitespace from ID# " + str(w[0]) + " in table " + str(l_table))

    def run(self):
        if self.ONLY_RU and self.ONLY_RU_COPY:
            print("Both ONLY RU booleans are True, ONLY ONE True IS ALLOWED")
            exit()
        if self.ONLY_RU:
            self.run_checks(lang_tables['ru'])
        elif self.ONLY_RU_COPY:
            self.run_checks("ru_words_copy")
        else:
            for (k, v) in lang_tables.items():
                self.run_checks(lang_tables[k])


if __name__ == "__main__":
    a = StripWhitespace()  # 4052
    a.run()
