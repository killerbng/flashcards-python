# This Python file uses the following encoding: utf-8
from Russian_Flash_Cards import *

class AddToDBManual():

    tbl = lang_tables['ru']
    #tbl = "ru_words_copy"

    tar = trans = en = audio = type = gender = date = ""
    cat = active = trouble = 0

    def reset_values(self):
        tar = trans = en = audio = type = gender = date = ""
        cat = active = trouble = 0

    def add_data(self): # Can't remember... how do I escape ' again? :/
        try:
            conn = mysql.connector.connect(**config)
            cur = conn.cursor()
            query = "INSERT INTO  " + self.tbl
            query += " (ru,pro,en,audio,word_type,word_gender,category,date,active,trouble_word)"
            # Using formating here is bad practice, we should use the 2nd arg of .execute, but we get away with it here
            # Because we know we are adding safe items.
            query += " VALUES ('{}','{}','{}','{}','{}','{}',{},'{}','{}','{}')".format(
                self.tar,
                self.trans,
                self.en,
                self.audio,
                self.type,
                self.gender,
                self.cat,
                self.date,
                self.active,
                self.trouble
            )
            #print(query)
            cur.execute(query)
            conn.commit()

            cur.close()
            conn.close()
            return True
        except:
            return False

    def get_info(self):
        self.tar = input("Target Text: ")
        self.pro = input("Transliteration: ")
        self.en = input("English Text: ")
        self.audio = input("Audio File: ")
        self.type = input("Word Type: ")
        self.gender = input("Word Gender: ")
        try:
            self.cat = int(input("Category: "))
        except:
            self.cat = 51
        self.date = input("Updated Date: ")
        if self.date is "":
            self.date = "2009 11 02"
        try:
            self.active = int(input("Active word? "))
        except:
            self.active = 0
        try:
            self.trouble = int(input("Trouble word? "))
        except:
            self.trouble = 0
        if self.cat is 66:
            self.en = self.audio = "UNKNOWN-TEXT"
            self.type = self.gender = "False"


    def run(self):
        self.get_info()
        test = self.add_data()
        self.reset_values()
        if test:
            print("\nSUCCESS\n\n")
        else:
            print("\nERROR ADDING ENTRY TO DB")
            print("ERROR ADDING ENTRY TO DB")
            print("ERROR ADDING ENTRY TO DB\n")

if __name__ == "__main__":
    a = AddToDBManual()
    a.reset_values()
    while(True):
        print()
        a.run()
