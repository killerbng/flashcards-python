# This Python file uses the following encoding: utf-8
from Russian_Flash_Cards import *


"""
    BROKEN SINCE CLASS MOVE - NEED TO FIX
"""

'''
    ALLOW INSERTING INFO IN TO DATABASE FROM A TEXT FILE
'''

class InsertIntoDB():

    queRU = []
    RU = []
    RUT = []

    def add_item(self, x, tbl):
        conn = mysql.connector.connect(**config)
        cur = conn.cursor()

        count = 0

        for z in x:
            try:
                ru = str(z[0])
            except:
                ru = None
            try:
                en = str(z[1])
            except:
                en = None
            try:
                cat = int(z[2])
            except:
                cat = 0
            #print("------")
            #print(z)
            #print("------")

        ru = ru.replace("\\ufeff﻿", "")
        ru = ru.replace("﻿\\ufeff", "")
        ru = ru.strip()
        if count == 0:
            ru = ru[1:]
            count += 1

        en = en.strip()
        cat = int(cat)

        query = "INSERT INTO " + str(tbl) + " (ru,en,audio,word_type,word_gender,category,date) VALUES (" + str(ru) + \
                "," + str(en) + ",'False','False','False'," + str(cat) + ",'2009 11 02')"

        print("\n")
        print(x)
        print(tbl)
        print(ru)
        print(en)
        print(cat)
        print(query)

        cur.execute(query)
        conn.commit()
        cur.close()
        conn.close()

    def scan_file(self, fl, tbl):
        entries = []
        split_entries = []

        f = open(fl, 'r')
        #f = open(fl, 'r', encoding="utf-8")
        for line in f.read().split('\n'):
            entries.append(line)
        f.close()

        for e in entries:
            data = e.split("=")
            split_entries.append(data)

        for i in split_entries:
            #print(i)
            self.add_item(i, tbl)

        print("\nShould be done!\n")

    def run(self):
        global exit_quit
        while True:
            print("RU / EN / CATEGORY split should be an = character. ie 'я тебя люблю. = I love you. = 48' ")
            #file = "C:/Users/killerbng/Desktop/a.txt"
            #table = "ru_words_copy"
            file = input(str("Full path to text file: "))
            table = input(str("Which table to insert in to? "))
            if file in exit_quit or table in exit_quit:
                break
            else:
                try:
                    self.scan_file(file, table)
                except:
                    self.run()
