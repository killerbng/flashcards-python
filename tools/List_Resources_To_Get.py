from Russian_Flash_Cards import *
from Russian_Flash_Cards.db import *


class ListURLResourceList:

    file = "../Site_Reference_URLS.txt"
    count = 0

    def run(self):
        with open(self.file) as f:
            for line in f:
                if line == "####################################################################################\n":
                    break
                if line is not None and line is not "" and line is not "\n":
                    print(line)
                    self.count += 1
        print("\n"+str(self.count)+" resource links to explore / add to datebase.\n")

if __name__ == "__main__":
    a = ListURLResourceList()
    a.run()
