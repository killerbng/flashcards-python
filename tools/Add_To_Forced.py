# This Python file uses the following encoding: utf-8
from Russian_Flash_Cards import *

'''
 TODO:
 - Currently this only handles NEW entries, there are NO checks, so dupes will happen
 - For RU only, but RU is the only lang with a forced db atm, so not a big deal?
'''

class AddToForced:
    ranged_add = False
    table = "ru_forced"
    #table = "ru_forced_copy" # Testing DB

    def check_if_active(self, id):
        result = 0 # If ID is out of range, this should stay 0 and continue as if inactive.
        conn = mysql.connector.connect(**config)
        cur = conn.cursor()

        sql = "SELECT * from " + lang_tables['ru'] + " WHERE id='" + str(id) + "'"

        cur.execute(sql)
        for c in cur:
            result = int(c[9])

        cur.close()
        conn.close()
        if result is 1:
            self.add_to_db(id)
        else:
            print("\nThat ID is not active or out of range.")

    def add_to_db(self, id):
        conn = mysql.connector.connect(**config)
        cur = conn.cursor()
        query = "INSERT INTO " + str(self.table) + " (tid) VALUES ('"+str(id)+"')"
        cur.execute(query)
        conn.commit()
        cur.close()
        conn.close()
        print("ID# " + str(id) + " added to " + self.table + "\n")

    def run(self):
        if self.ranged_add: # Ranged IDs
            start = int(input("Starting ID: "))
            stop = int(input("Final ID: "))
            for i in range(start, stop+1):
                self.check_if_active(i)
        else: # Single ID
            start = input("ID: ")
            self.check_if_active(start)

'''
14545
15060
'''
if __name__ == "__main__":
    a = AddToForced()
    a.ranged_add = True # This needs to be set if adding in a range of IDs instead of a single ID
    while True:
        a.run()