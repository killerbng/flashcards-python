# -*- coding: utf-8 -*-
from Russian_Flash_Cards import *

class CheckInformalFormal():
    entries = []

    table = ""

    ignore = []

    ru_ignore = [
        84, 101, 202, 257, 291, 775, 930, 945, 1031, 1051, 1052, 1055, 7431, 9510
    ]

    @staticmethod
    def do_update(id, tbl, formal=False, informal=True):
        if formal and not informal:
            word = "formal"
        elif formal and informal:
            word = "informal formal"
        else:
            word = "informal"
        row = "word_gender"
        try:
            update_item(word, tbl, row, id)
        except:
            print("I think updating item failed, PLEASE CHECK MANUALLY.")

    def add_ignores(self):
        global ignore
        global ru_ignore
        if self.table is lang_tables['ru']:
            self.ignore = self.ru_ignore

    def check_db(self, tbl):
        conn = mysql.connector.connect(**config)
        cur = conn.cursor()

        # Spaces are because of how so many other words will have, for example вый, which will cause вы w/o spaces to apply
        where_string = "`ru` LIKE '%Ты %' or `ru` LIKE '%тебе %' or `ru` LIKE '%тебя%'"
        where_string += " or `ru` LIKE '%вы %' or `ru` LIKE '%вас %' or `ru` LIKE '%вам %' or `ru` LIKE '%вами %'"

        sql = "SELECT * FROM " + tbl + " WHERE ("+where_string+") AND word_gender='False'"
        sql += " ORDER BY id ASC"
        cur.execute(sql)
        for e in cur:
            self.entries.append(e)

    def run_entries(self):
        print("Entries to fix: " + str(len(self.entries)))
        formal = False
        informal = True
        for e in self.entries:
            id = e[0]
            if id not in self.ignore:
                ru = e[1]
                a = input(ru + " :")
                if a in do_it:
                    b = input("Is this formal?")
                    if b in do_it:
                        formal = True
                        informal = False
                    elif b is "b" or b is "both":
                        formal = True
                        informal = True
                    print("You choose " + a + " and " + b + ".\n")
                    self.do_update(e[0], self.table, formal, informal)
                else:
                    print("WE SKIPPED THAT ONE.")

    def run(self, tbl):
        self.table = tbl
        self.add_ignores()
        self.check_db(self.table)
        #print(self.table)
        #print(str(len(self.ignore)))
        #print(str(len(self.entries)))
        if len(self.entries) > 0:
            self.run_entries()

if __name__ == "__main__":
    global lang_tables
    check = True
    t = ""
    while(check):
        q = input("What table to use? (example: ru_words or de_words) ")
        if q in lang_tables.values():
            t = q
            check = False
            print() # Just to make a nice space
    a = CheckInformalFormal()
    a.run(t)
