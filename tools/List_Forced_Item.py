# This Python file uses the following encoding: utf-8
from Russian_Flash_Cards import *
import mysql.connector

ru_ids = []


def get_forced_ids():
    conn = mysql.connector.connect(**config)
    cur = conn.cursor()
    query = "SELECT * FROM ru_forced WHERE enabled='1' ORDER BY id ASC"
    cur.execute(str(query))
    for i in cur:
        ru_ids.append(i[1])
    cur.close()
    conn.close()


def print_forced():
    where_str = ""
    conn = mysql.connector.connect(**config)
    cur = conn.cursor()
    for id in ru_ids:
        where_str += " id='"+str(id)+"' OR"
    query = "SELECT * FROM ru_words WHERE"+where_str+"DER BY id ASC"
    # print(query)
    # exit()
    cur.execute(str(query))
    for i in cur:
        print(str(i[0]) + " —→ " + str(i[1]))
    cur.close()
    conn.close()

get_forced_ids()
print(ru_ids)
if len(ru_ids) > 0:
    print_forced()
    print("Currently active force: " + str(len(ru_ids)))
else:
    print("No forced items found.")
