# -*- coding: utf-8 -*-
from Audio_Row_Search import *
from Debugger import *
from Insert_Into_DB import *
from add_pro import *
from List_Resources_To_Get import *
from tts import *
from Strip_Whitespace import *
from os import mkdir
import shutil
from urllib.parse import quote

#
# Custom Settings
#
FIND_UNKNOWN_TEXT = False
TABLE_COPY_VERSION = "_copy"  # Need to implement this, so I am not re-writing in so many tools/ files
AUTOYES = True

#
# DO NOT EDIT BELOW THIS LINE
#

def check_for_bad_cats(l_table):
    count = 0
    cat_count = get_cat_count()
    conn = mysql.connector.connect(**config)
    cur = conn.cursor()
    query = "SELECT * FROM " + l_table + " ORDER BY id ASC"
    cur.execute(str(query))
    for i in cur:
        cat = i[7]
        if cat < 1 or cat > cat_count:
            print("Cat for ID " + str(i[0]) + " seems bad. Cat = " + str(cat))
            count += 1
    cur.close()
    conn.close()
    if count > 0:
        print("\n" + str(count) + " items had bad categories.")


def check_for_invalid_dates(l_table):
    count = 0
    from datetime import datetime
    conn = mysql.connector.connect(**config)
    cur = conn.cursor()
    query = "SELECT * FROM " + l_table + " ORDER BY id ASC"
    cur.execute(str(query))
    for i in cur:
        date_text = i[8]
        try:
            datetime.strptime(date_text, '%Y %m %d')
        except Exception as e:
            if SHOW_EXCEPTION_ERRORS:
                print("Error setting datetime\n" + e)
            else:
                print("Incorrect data format for ID: " + str(i[0]))
            count += 1
    cur.close()
    conn.close()
    if count > 0:
        print("\n" + str(count) + " bad dates found.")


def check_for_no_audio(l_table):
    global AUTOYES
    if l_table == lang_tables["ru"]:
        ignore = [828, 835, 838, 840, 5987, 5988, 9489]
    elif l_table == lang_tables["de"]:
        ignore = [252]
        # Add Alphabet below - (TTS seems to mess up German alphabet audio)
        de_i = 57
        while de_i < 88:
            ignore.append(de_i)
            de_i += 1
    else:
        ignore = []
    conn = mysql.connector.connect(**config)
    count = 0
    cur = conn.cursor()
    query = "SELECT * FROM " + l_table + " ORDER BY id ASC"
    cur.execute(str(query))
    for i in cur:
        audio = i[4]
        audio = str_to_bool(audio)
        if audio is False:
            if i[3] is not 'F':
                if i[0] not in ignore:
                    pt = "ID #" + str(i[0]) + " has no audio file."
                    if i[9] == '1':
                        print(pt + "  --URGENT--")
                    else:
                        print(pt)
                    count += 1
    cur.close()
    conn.close()
    if count > 0:
        print("\n" + str(count) + " items have no audio file.")
        if AUTOYES:
            q = 'y'
        else:
            q = input(str("\nShould we try and run TTS? "))
        if q in do_it:
            tts = TTS()
            #  Default is ru, so for now, we only need to switch if DE
            if l_table is lang_tables['de']:
                tts.lang = "de-de" # WHY isn't this setting this correctly :( temp disabled in Debugger.py
            elif l_table is lang_tables['cn']:# WHY isn't this setting this correctly :( temp disabled in Debugger.py
                tts.lang = "zh-cn"
            tts.run()


def check_for_empty_pro(l_table):
    # global AUTOYES
    ap = ADD_PRO()
    ap.table = l_table
    ap.fix_me = []
    ap.fixed = []
    # ap.autoyes = AUTOYES
    ap.autoyes = True
    ap.run()


def check_trouble_words(cards):
    count = 0
    for c in cards:
        if c[9] == '0' and c[10] == '1':
            print(str(c[0]) + " >>> " + c[1] + " >>> " + c[3] + " >>> is a trouble word, but inactive.")
            count += 1
    if count > 0:
        print(str(count) + " items had bad trouble_word status.")


def find_missing_ids(table):
    found = []
    max = 0
    mcnt = 0
    conn = mysql.connector.connect(**config)
    cur = conn.cursor()
    cur.execute("SELECT id from " + table + " ORDER BY id DESC LIMIT 1")
    for i in cur:
        max = i[0]
    cur.execute("SELECT * FROM " + table + " ORDER BY id ASC")
    for i in cur:
        found.append(i[0])
    cur.close()
    conn.close()
    for i in range(1,(max+1)):
        if i not in found:
            mcnt += 1
            print(str(i) + " unused in " + table)
    if mcnt > 0:
        print(str(table) + " had " + str(mcnt) + " missing IDs.")


def find_questions_with_wrong_cat(table):
    q_cats = [20, 35, 39, 48, 61, 64, 65, 66, 68]  # Categories where questions are acceptable
    if table == lang_tables["ru"]:
        ignore_ids = [
            3640, 6620, 7940, 12083, 12152, 12153, 12721, 12722, 15307, 15903, 15946
        ]
    elif table == lang_tables["fi"]:
        ignore_ids = [6]
    elif table == lang_tables["de"]:
        ignore_ids = [254]
    else:
        ignore_ids = []
    total_bad = 0
    conn = mysql.connector.connect(**config)
    cur = conn.cursor()
    cur.execute("SELECT * FROM " + table + " ORDER BY id ASC")
    for i in cur:
        if i[1].find("?") > -1:
            if i[7] not in q_cats:
                if i[0] not in ignore_ids:
                    print("#" + str(i[0]) + " - " + i[1] + " has bad question cat of " + str(i[7]))
                    total_bad += 1
    cur.close()
    conn.close()
    if total_bad > 0:
        print(str(total_bad) + " question rows found.")


def find_unused_audio(table):  # Linux version NOT TESTED
    from os import listdir
    from os.path import isfile, join

    ignore = []
    ignore_ru = [
        "IAmAnAmericanISpeakEnglishAndRussianREAL", "PreparationForTheGreatFeast", "PreparationForTheGreatFeast",
        "NoPlayfulINF", "ICanNot", "ItCantBeOLD", "Cold-BACKUPAUDIO", "FiveAnya-Old", "Anya", "City", "SaintPetersburg",
        "Yes_OLD", "Language-Anna-Backup", "WatchI-Old",
    ]
    ignore_cn = [
        "1-10-AP", "Hi"
    ]

    if ON_LINUX:
        audio_path = str(SET_LINUX_PATH) + "audio/"
    else:
        audio_path = str(WINDOWS_PATH) + "\\audio\\"

    move_to_folder = audio_path + "\\Unused-Mixed\\"

    if table == lang_tables["br"]:
        audio_path += "br/"
    elif table == lang_tables["cn"]:
        audio_path += "cn/"
        for f in ignore_cn:
            ignore.append(f)
    elif table == lang_tables["de"]:
        audio_path += "de/"
    elif table == lang_tables["esp"]:
        audio_path += "esp/"
    elif table == lang_tables["fi"]:
        audio_path += "fi/"
    elif table == lang_tables["ltv"]:
        audio_path += "ltv/"
    elif table == lang_tables["ru"]:
        audio_path += "ru\\"
        #print(audio_path)
        #quit()
        for f in ignore_ru:
            ignore.append(f)
    elif table == lang_tables["spa"]:
        audio_path += "spa/"
    elif table == lang_tables["ukr"]:
        audio_path += "ukr/"
    in_db = []
    total_bad = 0
    possible_easy_fix = 0
    in_folder = []
    for file in listdir(audio_path):
        if isfile(join(audio_path, file)):
            file = file.replace(".wav", "")
            file = file.replace(".mp3", "")
            file = file.replace(".ogg", "")
            in_folder.append(file)
    conn = mysql.connector.connect(**config)
    cur = conn.cursor()
    cur.execute("SELECT * FROM " + table + " ORDER BY id ASC")
    for i in cur:
        if i[4] is not False:
            in_db.append(i[4])
    cur.close()
    conn.close()
    for f in in_folder:
        if f not in in_db:
            if f not in ignore:
                might_exist = False
                found = ""
                for dbw in in_db:
                    a = dbw.lower()
                    b = f.lower()
                    if f not in ignore:
                        if a == b:
                            might_exist = True
                            found = dbw
                if might_exist:
                    print(f + " is not used. HOWEVER db entry " + found + " was found.")
                    possible_easy_fix += 1
                else:
                    print(f + " is not used in the database.")
                    if path.isdir(move_to_folder) is False:
                        mkdir(move_to_folder, 777)
                    try:
                        shutil.move(audio_path + f + ".wav", move_to_folder+f+".wav")
                    except Exception as e:
                        try:
                            shutil.move(audio_path + f + ".mp3", move_to_folder+f+".mp3")
                        except Exception as e:
                            try:
                                shutil.move(audio_path + f + ".ogg", move_to_folder+f+".ogg")
                            except Exception as e:
                                pass

                    total_bad += 1
    if total_bad > 0:
        print("\n" + str(total_bad) + " files are not used atm. They were moved to temp folder on desktop.")
    if possible_easy_fix > 0:
        print("\n" + str(possible_easy_fix) + " files may just have typos.")


def bad_verb_cat(table):
    count = 0
    ignore_cats = [10, 11, 13, 48, 49, 70]
    if table == 'ru_words':
        ignore_ids = [870, 901, 3761, 9612, 10029, 10154, 4252, 8648, 15342, 15343]
    elif table == 'de_words':
        ignore_ids = []
    else:
        ignore_ids = []
    conn = mysql.connector.connect(**config)
    cur = conn.cursor()
    cur.execute("SELECT * FROM " + table + " ORDER BY id ASC")
    for i in cur:
        if i[5] == 'verb':
            if i[7] != 44:
                if i[7] not in ignore_cats:
                    if i[0] not in ignore_ids:
                        count += 1
                        if i[7] == 51:
                            update_item(str(44), table, 'category', i[0])
                            print("AUTO SET " + i[1] + " to category 44.")
                        else:
                            print(str(i[0]) + " - " + i[1] + " set to category " + str(i[7]) + " not 44. CHECK IT!")
    cur.close()
    conn.close()
    if count > 0:
        print("\n" + str(count) + " verbs found to not be in verb category.")


def find_bad_chars(table):
    find_me = ["Ð"]
    count = 0
    conn = mysql.connector.connect(**config)
    cur = conn.cursor()
    cur.execute("SELECT * FROM " + table + " ORDER BY id ASC")
    for i in cur:
        for bw in find_me:
            if i[1].find(bw) != -1:
                count += 1
                print(str(i[0]) + " has an invalid character. :/ (" + colored(bw, styles['third']) + ")")
    cur.close()
    conn.close()
    if count > 0:
        print(str(count) + " entries have bad/invalid characters.")


def find_bad_qa(table):
    needs_fixing = []
    ignore_cats = [39, 48, 65, 66]
    if table == 'ru_words':
        ignore_ids = [
            7048, 7050, 7223, 7598, 7940, 8535, 8608, 8687, 8993, 9025, 9234, 9379, 9667, 9800, 9934, 10051, 10248,
            10587, 10530, 10651, 10685, 11065, 11198, 11405, 11519, 11532, 11661, 11662, 11978, 11984, 11986, 12083,
            12072, 12077, 12152, 12153, 12159, 12160, 539, 12529, 12721, 12722, 12944, 12977, 13063, 13831, 14243,
            14245, 14246, 14930, 15307, 15903, 15946, 3022, 16125, 16476, 17001, 17003
        ]
    elif table == "de_words":
        ignore_ids = [254, 708]
    elif table == "ukr_words":
        ignore_ids = []
    else:  # NEED to make list for other langs at some point!
        ignore_ids = []
    count = 0
    conn = mysql.connector.connect(**config)
    cur = conn.cursor()
    cur.execute("SELECT * FROM " + table + " ORDER BY id ASC")
    for i in cur:
        if i[1].find("?") > -1 and (i[1].find(".") > -1 or i[1].find("!") > -1) and i[1].find("...") < 0:
            if i[7] != 64 and i[7] not in ignore_cats:
                if i[0] not in ignore_ids:
                    print("#" + str(i[0]) + " - " + i[1] + " seems to be a QaA but category is: " + str(i[7]))
                    needs_fixing.append(i[0])
                    count += 1
    cur.close()
    conn.close()
    if count > 0:
        print("\n" + str(count) + " QaA found to not be in QaA category.\n")
        que = input(str("Would you like to attempt to automatically correct the problem?"))
        if que in do_it:  # This has been tested... but not 100% sure it is working correctly.
            print("Attempting to fix problems")
            for itm in needs_fixing:
                try:
                    update_item(64, table, "category", itm)
                except Exception as err:
                    print("There was an error updating ID " + str(itm) + " to cat 64.")


def find_stress_marks(table):
    count_word = 0
    count_pro = 0
    needs_updating_word = []
    needs_updating_pro = []
    specials = {
        "́":"", "`" :"", "á":"а", "é" :"e", "и́":"и", "ó" :"о", "у́":"у", "я́":"я", "́":"", "о́":"о", "Э́":"Э", "и́":"и", "﻿":""
    }
    conn = mysql.connector.connect(**config)
    cur = conn.cursor()
    cur.execute("SELECT * FROM " + table + " ORDER BY id ASC")
    for i in cur:
        for k, v in specials.items():
            if i[1].find(k) > -1:
                needs_updating_word.append([i[0], i[1]])
                #print(str(i[0]) + "  --  " + str(i[1]))
                count_word += 1
            if i[2] is not None:
                if i[2].find(k) > -1:
                    needs_updating_pro.append([i[0], i[2]])
                    #print(str(i[0]) + "  --  " + str(i[2]))
                    count_pro += 1

    cur.close()
    conn.close()

    b = list()
    for sublist in needs_updating_word:
        if sublist not in b:
            b.append(sublist)
    needs_updating_word = b
    del b

    c = list()
    for sublist in needs_updating_pro:
        if sublist not in c:
            c.append(sublist)
    needs_updating_pro = c
    del c

    if count_word > 0:
        print("\n" + str(count_word) + " stress marks found.")
        ans = input("\nWould you like to auto fix these? (Backup DB just in case!)")
        if ans in do_it:
            try:
                conn = mysql.connector.connect(**config)
                cur = conn.cursor()
                for i in needs_updating_word:
                    a = str(i[1])
                    for k, v in specials.items():
                        a = a.replace(k, v)
                    print("Replacing " + i[1] + "    WITH  " + a)
                    sql = "UPDATE " + str(table) + " SET ru='" + str(a) + "' WHERE id='" + str(i[0]) + "'"
                    cur.execute(str(sql))
                    conn.commit()
                cur.close()
                conn.close()
            except Exception as e:
                print("Could not update item. \n" + str(e))

    if count_pro > 0:
        print("\nPRO STRESS MARK UPDATE NOT FULLY IMPLEMENTED YET -- TO BE DONE\n")


def find_unknown_text(l_table):
    count = 0
    conn = mysql.connector.connect(**config)
    cur = conn.cursor()
    cur.execute("SELECT * FROM " + l_table + " ORDER BY id ASC")
    for i in cur:
        if i[4].find("UNKNOWN-") > -1 or int(i[7]) == 66: # When I get these set up better, this should be an and not or
            print("ID: " + str(i[0]) + " has unknown sentence text.")
            count += 1

    cur.close()
    conn.close()

    if count > 0:
        print("\n" + str(count) + " entries have text with an unknown English translation.\n")


def find_blank_entries(l_table):
    count = 0
    conn = mysql.connector.connect(**config)
    cur = conn.cursor()
    cur.execute("SELECT * FROM " + l_table + " ORDER BY id ASC")
    for i in cur:
        if i[1] == "" and i[3] == "":
            print(str(i[0]) + " is a blank entry.")
            count += 1
            continue
        if i[1] == None and i[3] == None:
            print(str(i[0]) + " is a blank entry.")
            count += 1
            continue
    cur.close()
    conn.close()

    if count > 0:
        print("\n" + str(count) + " blank entries found.\n")


def find_adverbs_as_noun(l_table):
    count = 0
    ignore = []
    needs_fixing = []
    if l_table == "ru_words":
        ignore += []
    conn = mysql.connector.connect(**config)
    cur = conn.cursor()
    cur.execute("SELECT * FROM " + l_table + " ORDER BY id ASC")
    for i in cur:
        if i[5] == 'adverb' and i[7] == 51:
            if i[0] not in ignore:
                needs_fixing.append(i[0])
                print("ID: " + str(i[0]) + ", " + str(i[1]) + ", says it's an adverb but is set to a noun.")
                count += 1

    cur.close()
    conn.close()

    if count > 0:
        print("\n" + str(count) + " adverbs set to noun entries found.\n")
        que = input(str("Would you like to attempt to automatically correct the problem?"))
        if que in do_it:
            print("Attempting to fix problems")
            for itm in needs_fixing:
                try:
                    update_item(44, l_table, "category", str(itm))
                except Exception as err:
                    print("There was an error updating ID " + str(itm) + " to cat 64.")
    # exit()


def make_lower(txt): # FIXME: Still does not make lowercase.
    txt = quote(txt)
    txt = txt.lower()
    txt = list(txt)
    txt[0] = str(txt[0]).lower()
    txt = "".join(txt)
    txt = txt.lower()
    #txt = txt.decode('utf-8').lower() # 'str' object has no attribute 'decode' - Yet suggested by SO post?
    return txt


def filter_comment(txt):
    # TODO: complete replace list
    replace = [" (as noun)", " (as pronoun)", " (as verb)", " (as adverb)", " (as adjective)", " (as body part)"]
    for bad in replace:
        txt = txt.replace(bad, "")
    return txt


def find_false_type_gender(l_table, lang_short):
    # TODO: FIGURE OUT what other categories need to be added
    only_show_singles = True  # Use True when super high numbers - False when count is lowered (less over whelming)

    ignore = []
    found = []

    count = 0
    ncount = 0

    cats = [2,3,4,7,8,13,15,21,30,39,44,51] # These categories should be good to use with any language. - DOUBLE CHECK

    cats_sql = ""
    cats_count = 0
    for c in cats:
        if cats_count is 0:
            cats_sql += "category='" + str(c) + "'"
            cats_count = 1
        else:
            cats_sql += " OR category='" + str(c) + "'"

    ru_ignore_ids = [
        451, 455, 501, 506, 582, 10508, 12709, 7477, 2098, 1319, 1458, 1493, 1510, 12920, 12934, 1524,  1548, 1565,
        1566, 1574, 1657, 1608, 11547, 1673, 1760, 13084, 1883, 1998, 2054, 2089, 2120, 2218, 2351, 2352, 2480, 2319,
        13281, 2603, 5535, 2608, 2609, 2612, 2621, 8642, 2658, 2676, 2679, 2693, 13558, 8703, 2725, 2730, 2789, 2805,
        11547, 13809, 680, 12920, 13844, 1228, 1234, 1235, 1239, 1240, 1241, 1242, 1551, 2023, 1944, 2377, 2751, 2957,
        137, 13870, 725, 732, 744, 746, 14017, 14032, 761, 762, 763, 764, 765, 766, 767, 768, 769, 775, 1580, 2765,
        1584, 1986, 1992, 1993, 1994, 2011, 2101, 2102, 2393, 2401, 2402, 2403, 2404, 2408, 2425, 2426, 2450, 2458,
        2460, 7909, 14218, 2561, 2640, 2642, 2660, 2666, 2669, 2670, 2671, 2672, 8330, 9802, 2923, 278, 285, 692, 991,
        2980, 2986, 16004, 16031, 3022, 3091, 3093, 3100, 3101, 3102, 3110, 3113, 3115, 3126, 3279, 3280, 3297, 3343,
        3306, 3526, 3661, 3656, 3715
    ]
    # Interjection / Particle / Conjunction / Preposition / Accusative
    # --- Ignore these in SQL string instead of using a list --- BUT ONLY AFTER FIX ALL CURRENTLY FOUND
    ru_ignore_part_inter_conj_prep_ids = [
        45, 175, 381, 383, 389, 398, 416, 422, 449, 450, 464, 470, 481, 500, 502, 503, 504, 512, 1009, 4653, 10022, 1474,
        1206, 1270, 1271, 1272, 1273, 1509, 1645, 1740, 1721, 1899, 2241, 2336, 3530, 13485, 2448, 2866,
    ]
    # Pronouns ... no gender??
    ru_ignore_pronoun_ids = [
        526, 527, 568, 2265, 2432, 2657, 2715, 2716, 2717
    ]
    # Adjective -- go back and fix GENDER for these later
    ru_ignore_adj_possible_no_gender = [
        544, 570, 631, 963, 1424, 1586, 4294
    ]
    # Foods ignore
    ru_ignore_foods_and_drinks = [
        2232, 2771, 3275, 3362, 3305, 3520, 3719, 3720, 3721, 3722, 3724, 3718
    ]

    de_ignore_ids = [ # TODO: I NEED to make a way so DE adjectives are not queried!
        1, 49, 51, 52
    ]

    numbers_found = []

    if l_table is lang_tables['ru']:
        for i in ru_ignore_ids:
            ignore.append(i)
        for i in ru_ignore_part_inter_conj_prep_ids:
            ignore.append(i)
        for i in ru_ignore_pronoun_ids:
            ignore.append(i)
        for i in ru_ignore_adj_possible_no_gender:
            ignore.append(i)
        for i in ru_ignore_foods_and_drinks:
            ignore.append(i)
    elif l_table is lang_tables['de']:
        for i in de_ignore_ids:
            ignore.append(i)

    conn = mysql.connector.connect(**config)
    cur = conn.cursor()
    sql = "SELECT * FROM `"+l_table+"`"
    sql += " WHERE ("
    sql += cats_sql
    sql += ")"
    sql += " AND (word_type='False' OR word_gender='False')"
    sql += " AND CHARACTER_LENGTH(ru)>'1'"  # So we ignore alphabet and single letter words that "glitch" on wiktionary
    sql += " ORDER BY id DESC"  # Desc so 1st is on bottom... so not scrolling the console
    # sql += " LIMIT 100"
    ##
    #print(sql)
    #exit()
    ##
    cur.execute(sql)
    for e in cur:
        if str(e[5]).lower() != "adverb" and str(e[5]).lower() != "slang" and str(e[5]).lower() != "phrase":
            if e[0] not in ignore:
                try:
                    a = e[1]
                    a = int(a)
                    numbers_found.append([str(e[0]), str(e[1])])
                    continue
                except ValueError:
                    if "|" in e[1]:
                        if only_show_singles is False:
                            found.append(e)
                            count += 1
                    else:
                        found.append(e)
                        count += 1
    cur.close()
    conn.close()

    if count > 0:
        lang_shortcut = lang_short
        if lang_shortcut == "ru":
            lang_shortcut = "Russian"
        elif lang_shortcut == "de":
            lang_shortcut = "German"
        elif lang_shortcut == "ukr":
            lang_shortcut = "Ukrainian"
        elif lang_shortcut == "spa":
            lang_shortcut = "Spanish"
        elif lang_shortcut == "on":
            lang_shortcut = "OldNorse"
        elif lang_shortcut == "br":
            lang_shortcut = "Brazilian"
        elif lang_shortcut == "esp":
            lang_shortcut = "Esperanto"
        elif lang_shortcut == "ltv":
            lang_shortcut = "Latvian"
        elif lang_shortcut == "fi":
            lang_shortcut = "Finnish"
        elif lang_shortcut == "se":
            lang_shortcut = "Swedish"
        elif lang_shortcut == "cn":
            lang_shortcut = "Chinese"
        else:
            lang_shortcut = ""

        ecount = len(found)
        eecount = 0
        get_count = ecount - 20
        # for entry in reversed(found):
        for entry in found:
            if eecount > get_count:
                output = str(entry[0]) + " - " + entry[1]  # entry[1] will show with comments here
                if "|" not in entry[1]:
                    ent = filter_comment(entry[1])
                    ent = make_lower(ent)
                    url = "https://en.wiktionary.org/wiki/"+ent.lower()+"#"+lang_shortcut
                    output += "  ----  " + url
                else:
                    output += "  ---- word_type and/or word_gender is False"
                print(output)

            eecount += 1

        print("\n"+str(count)+" entries need to be fixed\n")

    if len(numbers_found) > 0:
        for n in numbers_found:
            if ncount < 20:
                print(str(n[0]) + "  ~~  " + str(n[1]))
        print("\nFound " + str(len(numbers_found)) + " numbers. Add ids to ignores if correct!\n")


def find_unmarked_formal_informal(l_table):
    '''
        I would love to make this so I could just be asked if I want it changed, and it changes it
        IE: "IS this a legit entry to fix:"
        if yes: fix_entry(entry)
    '''
    ignore = []
    ru_ignore = [
        84, 101, 202, 257, 291, 775, 930, 945, 1031, 1051, 1052, 1055, 7431
    ]
    count = 0

    if l_table is lang_tables['ru']:
        for i in ru_ignore:
            ignore.append(i)

    conn = mysql.connector.connect(**config)
    cur = conn.cursor()

    # Spaces are because of how so manty other words will have, for example вый, which will cause вы w/o spaces to apply
    where_string = "`ru` LIKE '%Ты %' or `ru` LIKE '%тебе %' or `ru` LIKE '%тебя%'"
    where_string += " or `ru` LIKE '%вы %' or `ru` LIKE '%вас %' or `ru` LIKE '%вам %' or `ru` LIKE '%вами %'"

    sql = "SELECT * FROM `"+l_table+"` WHERE ("+where_string+") AND word_gender='False'"
    sql += " ORDER BY id ASC"  # DESC so 1st is on bottom... so not scrolling the console
    cur.execute(sql)
    for e in cur:
        if e[0] not in ignore:
            if count < 20:
                print(str(e[0]) + " → " + e[1] + " → has no formal or informal tag set.")
            count += 1

    cur.close()
    conn.close()
    if count > 0:
        print("Found " + str(count) + " entries with unmarked formal / informal text")


def find_phrase_as_noun(l_table):
    ignore = []
    ru_ignore = [

    ]
    count = 0

    if l_table is lang_tables['ru']:
        for i in ru_ignore:
            ignore.append(i)

    conn = mysql.connector.connect(**config)
    cur = conn.cursor()

    sql = "SELECT * FROM `"+l_table+"` WHERE word_type='phrase' AND category='51'"
    sql += " ORDER BY id ASC"  # Desc so 1st is on bottom... so not scrolling the console
    cur.execute(sql)
    for e in cur:
        if e[0] not in ignore:
            if count < 20:
                print(str(e[0]) + " → " + e[1] + " → is set as both a phrase and a noun.")
            count += 1
    cur.close()
    conn.close()
    if count > 0:
        print("Found " + str(count) + " entries with incorrect phrase and category state.")


def find_whitespace_items(l_table, ru_only):
    sws = StripWhitespace()
    sws.ONLY_RU = ru_only
    # sws.ONLY_RU_COPY = ru_only  # Switch to this if u want to use the test table
    sws.run()


def find_empty_ranked_rows():
    count = 0
    conn = mysql.connector.connect(**config)
    cur = conn.cursor()

    sql = "SELECT * FROM ru_ranking WHERE ru_id IS NULL OR ru_id = ''"
    # sql += " LIMIT 100"  # Let's not spam the output console too much
    sql += " ORDER BY rank ASC"
    cur.execute(sql)
    for e in cur:
        if count < 20:
            print("Need to add ru_id for RANK: " + str(e[0]))
        count += 1

    cur.close()
    conn.close()
    if count > 0:
        print("Found " + str(count) + " ranked with no ru_id info.")


"""
    MAKE A SCRIPT TO ADD DATES AS DAY NUMBER MONTH ...  ie 6th April
"""
def add_second_date_format(tbl):
    months = [
        ["января", 'January'],
        ["Февраль", 'February'],
        ["март", 'Match'],
        ["апрель", 'April'],
        ["мая", 'May'],
        ["июня", 'June'],
        ["Июль", 'July'],
        ["августа", 'August'],
        ["сентября", 'September'],
        ["октябрь", 'October'],
        ["ноябрь", 'Novemember'],
        ["декабрь" 'December'],
    ]
    days_letters = [ # Days are always neuter!
        ["первое", '1st'],
        ["второе", '2nd'],
        ["третье", '3rd'],
        ["четвертое", '4th'],
        ["пятое", '5th'],
        ["шестое", '6th'],
        ["седьмое", '7th'],
        ["восьмо", '8th'],
        ["девятое", '9th'],
        ["десятое", '10th'],
        ["одиннадцатое", '11th'],
        ["двенадцатое", '12th'],
        ["тринадцатое", '13th'],
        ["четырнадцатое", '14th'],
        ["пятнадцатое", '15th'],
        ["шестнадцатое", '16th'],
        ["семнадцатое", '17th'],
        ["восемнадцатое", '18th'],
        ["девятнадцатое", '19th'],
        ["двадцатое", '20th'],
        ["двадцать первое", '21st'],
        ["двадцать второе", '22nd'],
        ["двадцать третье", '23rd'],
        ["двадцать четвертое", '24th'],
        ["двадцать пятое", '25th'],
        ["двадцать шестое", '26th'],
        ["двадцать седьмое", '27th'],
        ["двадцать восьмое", '28th'],
        ["двадцать девятое", '29th'],
        ["тридцатое", '30th'],
        ["тридцать первое" '31st']
    ]
    days_numbers_only = []
    for i in range(1,32):
        days_numbers_only.append(i)
    # print(days)

def find_tts_audio(tbl): # Find TTS Audio that needs to be replaced with "real" audio.
    # Ignores are audio I will never really be able to find audio for, unless I try and use my own voice.
    # Usually full sentences or questions. Single words or short phrases should be "findable"
    ignore = []

    ru_ignore = [
        1292,
    ]

    cn_ignore = [

    ]

    de_ignore = [

    ]


    if tbl is lang_tables['ru']:
        ignore = ru_ignore
    elif tbl is lang_tables['cn']:
        ignore = cn_ignore
    elif tbl is lang_tables['de']:
        ignore = de_ignore

    count = 0
    conn = mysql.connector.connect(**config)
    cur = conn.cursor()
    sql = "SELECT * FROM " + tbl + " WHERE audio LIKE '%TTS%'"
    cur.execute(sql)
    for e in cur:
        if e[0] not in ignore:
            count += 1
            if count < 11:
                if e[1] is not None and e[1] is not "":
                    print(str(e[0]) + " >>> " + str(e[1]) + " --- needs TTS replaced.")
                elif e[2] is not None and e[2] is not "":
                    print(str(e[0]) + " >>> " + str(e[2]) + " --- needs TTS replaced.")

    print("\n"+ str(count) + " entries need TTS audio files replaced.\n")
    cur.close()
    conn.close()
