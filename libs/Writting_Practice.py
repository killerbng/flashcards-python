# This Python file uses the following encoding: utf-8
from Russian_Flash_Cards import *

class WritingPractice():
    sentences_questions_phrases = []
    words = []

    def get_five(self, arr):
        global words
        global sentences_questions_phrases
        out = []
        for i in range(0, 5):
            r = randint(0, len(arr)-1)
            out.append(arr[r])
        return out

    def build_cats(self, arr):
        a = ""
        for c in arr:
            a += "category='" + str(c) + "' OR "
        a = a.rstrip("OR ")
        return a

    def get_all_words(self, table):
        global words
        cats = [44, 51]  # This needs to be improved later
        cats_sql = self.build_cats(cats)
        conn = mysql.connector.connect(**config)
        cur = conn.cursor()
        cur.execute("SELECT * from " + table + " WHERE (" + cats_sql + ") AND active='1' ORDER BY id DESC")
        for i in cur:
            self.words.append(i)
        cur.close()
        conn.close()

    def get_all_sqp(self, table):
        global sentences_questions_phrases
        cats = [35, 46, 61]
        cats_sql = self.build_cats(cats)
        conn = mysql.connector.connect(**config)
        cur = conn.cursor()
        cur.execute("SELECT * from " + table + " WHERE (" + cats_sql + ") AND active='1' ORDER BY id DESC")
        for i in cur:
            self.sentences_questions_phrases.append(i)
        cur.close()
        conn.close()

    def run(self):
        l = input(str("What lang? cn | de | ru | ukr : "))
        l = l.lower()

        self.get_all_sqp(lang_tables[l])
        self.get_all_words(lang_tables[l])
        five_words = self.get_five(self.words)
        five_sqp = self.get_five(self.sentences_questions_phrases)

        print("\nWRITE THESE IN "+l.upper()+ " SENTENCES!")
        for w in five_words:
            print(str(w[1]))
        print("\nNOW WRITE THESE ENGLISH SENTENCES / QUESTIONS / PHRASES IN "+l.upper()+"!")
        for s in five_sqp:
            print(str(s[3]))

"""
a = WritingPractice()
a.run()
"""