# This Python file uses the following encoding: utf-8
from Russian_Flash_Cards import *

class SentenceStructures():

    ConstructionsDeck = []
    """
    def __init__(self):
        self.ConstructionsDeck = self.build_deck()
    """
    def build_deck(self):
        arr = []
        conn = mysql.connector.connect(**config)
        cur = conn.cursor()
        query = "SELECT * FROM constructions ORDER BY id ASC"
        cur.execute(str(query))
        for i in cur:
            arr.append(i)
        cur.close()
        conn.close()
        return arr

    def run(self):  # The example line is not currently used, but kept anyways
        self.ConstructionsDeck = self.build_deck()
        possible = ""
        examples = []
        for i in range(0,3):
            r = randint(0,len(self.ConstructionsDeck)-1)
            if str_to_bool(self.ConstructionsDeck[r][2]) is not False:
                possible += " formal"
                examples.append("formal: " + str(self.ConstructionsDeck[r][2]))
            if str_to_bool(self.ConstructionsDeck[r][3]) is not False:
                possible += " informal"
                examples.append("informal: " + str(self.ConstructionsDeck[r][3]))
            if str_to_bool(self.ConstructionsDeck[r][4]) is not False:
                possible += " male"
                examples.append("male: " + str(self.ConstructionsDeck[r][4]))
            if str_to_bool(self.ConstructionsDeck[r][5]) is not False:
                possible += " female"
                examples.append("female: " + str(self.ConstructionsDeck[r][5]))
            if str_to_bool(self.ConstructionsDeck[r][6]) is not False:
                possible += " plural"
                examples.append("plural: " + str(self.ConstructionsDeck[r][6]))
            if str_to_bool(self.ConstructionsDeck[r][7]) is not False:
                possible += " I / General"
                examples.append("I/General: " + str(self.ConstructionsDeck[r][7]))
            print(str(self.ConstructionsDeck[r][1]) + "\t\t\t\t\t Looking for: ( " + possible + " )\n", end="")
        print("\n", end="")
