from Russian_Flash_Cards import *

class ListTroubles():

    ONLY_RU = True
    ListTroubleDeck = []
    ListTroubleTotal = 0

    def run(self):
        if self.ONLY_RU:
            build_trouble_deck(lang_tables['ru'], self.ListTroubleDeck)
            self.ListTroubleDeck.sort()
            for i in self.ListTroubleDeck:
                print(str(i[1]) + " - " + str(i[3]))
                self.ListTroubleTotal += 1
        else:
            # First reset Deck and Total
            self.ListTroubleDeck = []
            self.ListTroubleTotal = 0
            pass  # TO DO

        print("\n" + str(self.ListTroubleTotal) + " entries.\n")
