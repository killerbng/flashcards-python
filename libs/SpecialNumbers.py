# This Python file uses the following encoding: utf-8
from Russian_Flash_Cards import *

class SpecialNumbers():

    SN = []
    pause = 6

    def play_deck(self):
        r = randint(0, len(self.SN)-1)
        # Merge alt number when needed
        nbr = str(self.SN[r][1])
        if self.SN[r][2] != None:
            nbr += " / " + self.SN[r][2]
        # Give Number(s)
        print("\n" + colored(nbr, styles['first']))
        # Pause
        sleep(self.pause)
        # Give Results
        print(colored(self.SN[r][3],styles['third']))
        # Wait for user input to continue
        inp = input(str("\nPress ENTER to continue..."))
        # Delete Card
        del self.SN[r]
        # Move on to next card or end
        if len(self.SN) > 0:
            self.play_deck()

    def query_numbers(self):
        conn = mysql.connector.connect(**config)
        cur = conn.cursor()
        cur.execute("SELECT * FROM ru_special_numbers ORDER BY id ASC")
        for nbr in cur:
            self.SN.append(nbr)
        cur.close()
        conn.close()

    def run(self):
        # Build Deck
        self.query_numbers()
        #
        self.play_deck()
        #
        quit()
