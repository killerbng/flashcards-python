# This Python file uses the following encoding: utf-8
from Russian_Flash_Cards import *

'''
    Maybe I should add a count for how many I get right and wrong?
'''

class Ten_Ten:

    __PLAYAUDIO__ = True

    # Want more than 10 and 10? Maybe 10, 10, 10, 10?
    __INC_ON__ = False
    __INC_CN__ = True
    __INC_BR__ = False
    __INC_SPA__ = False

    TenDE = []
    TenRU = []
    TenON = []
    TenCN = []
    TenBR = []
    TenSPA = []
    Deck = []

    pause = 10

    @staticmethod
    def run_audio(fp, ext):
        global ON_LINUX
        if ON_LINUX:
            call(["mpg123", "--quiet", fp + ext])
        else:
            music = pyglet.media.load(fp + ext)
            music.play()

    def play_audio(self, dir, sound):
        if ON_LINUX:
            snd = LINUX_PATH + "/audio/" + dir + "/" + sound
        else:
            snd = WINDOWS_PATH + "\\audio\\" + dir + "\\" + sound
        # Try each audio extension
        try:
            self.run_audio(snd, ".mp3")
        except Exception as e:
            if SHOW_EXCEPTION_ERRORS:
                print(".mp3 not found\n" + str(e))
            try:
                self.run_audio(snd, ".wav")
            except Exception as e:
                if SHOW_EXCEPTION_ERRORS:
                    print(".wav not found\n" + str(e))
                try:
                    self.run_audio(snd, ".ogg")
                except Exception as e:
                    if SHOW_EXCEPTION_ERRORS:
                        print(".ogg not found\n" + str(e))
                    print("~~ AUDIO FILE MISSING " + str(snd) + "  ~~")

    def build_deck(self, tbl):
        temp = []
        conn = mysql.connector.connect(**config)
        cur = conn.cursor()
        cur.execute("SELECT * FROM " + tbl + " WHERE active='1' AND category<>'1' ORDER BY RAND()")
        if tbl is not "cn_words":
            for w in cur:
                temp.append(w)
        else:
            # We need to rebuild the list here so we get pinyin
            for w in cur:
                a = [0,1,2,3,4,5,6,7,8,9,10,11,12]
                a[0] = w[0]
                a[1] = w[2]
                a[2] = w[3]
                for i in range(3,13):
                    a[i] = w[i]
                temp.append(a)
        cur.close()
        conn.close()
        if tbl == lang_tables['de']:
            l = len(temp)
            for i in range(0, 10):
                a = my_randint(l)
                self.TenDE.append(temp[a])
        elif tbl == lang_tables['ru']:
            l = len(temp)
            for i in range(0, 10):
                a = my_randint(l)
                self.TenRU.append(temp[a])
        elif tbl == lang_tables['on']:
            l = len(temp)
            for i in range(0, 10):
                a = my_randint(l)
                self.TenON.append(temp[a])
        elif tbl == lang_tables['cn']:
            l = len(temp)
            for i in range(0, 10):
                a = my_randint(l)
                self.TenCN.append(temp[a])
        elif tbl == lang_tables['br']:
            l = len(temp)
            for i in range(0, 10):
                a = my_randint(l)
                self.TenBR.append(temp[a])
        elif tbl == lang_tables['spa']:
            l = len(temp)
            for i in range(0, 10):
                a = my_randint(l)
                self.TenSPA.append(temp[a])

    def combine_decks(self):
        for i in self.TenDE:
            a = list(i)
            a.append("de")
            self.Deck.append(a)
        for i in self.TenRU:
            a = list(i)
            a.append("ru")
            self.Deck.append(a)
        if self.__INC_ON__:
            for i in self.TenON:
                a = list(i)
                a.append("on")
                self.Deck.append(a)
        if self.__INC_CN__:
            for i in self.TenCN:
                a = list(i)
                a.append("cn")
                self.Deck.append(a)
        if self.__INC_BR__:
            for i in self.TenBR:
                a = list(i)
                a.append("br")
                self.Deck.append(a)
        if self.__INC_SPA__:
            for i in self.TenSPA:
                a = list(i)
                a.append("spa")
                self.Deck.append(a)

    def ask_answer(self, Card, ds, r):
        id = str(Card[0])
        tbl = str(lang_tables[str(Card[13])])
        answer = input(str("\nDid you get this correct?"))
        remove = True
        if answer == '0':
            try:
                add_to_correct_incorrect_count(id, tbl, 1, str(ds))
            except Exception as e:
                print(e)
        elif answer == '1':
            try:
                add_to_correct_incorrect_count(id, tbl, 0, str(ds))
            except Exception as e:
                print(e)
        elif answer == '2':
            remove = False
        elif answer == '3':
            update_item(Card[11]+1, tbl, 'times_correct', id)
            if Card[10] is not '0' or Card[10] is not 0:
                update_item('1', tbl, 'trouble_word', id)
            print("You have got this word correct", str(Card[11]+1), "time(s) and incorrect",
                  str(Card[12]), "time(s).")
            print()
        elif answer == 'r':
            # self.play_audio(l, audio)
            self.play_audio(str(Card[13]), str(Card[4]))
            remove = False
            self.ask_answer(Card, ds, r)
        else:
            remove = False
            self.ask_answer(Card, ds, r)
        if remove:
            del self.Deck[r]

    def get_card(self):
        ds = '' + str(year) + " " + str(month) + " " + str(day) + ''
        r = my_randint(len(self.Deck))
        id = str(self.Deck[r][0])
        tbl = str(lang_tables[str(self.Deck[r][13])])
        # This uses a custom utf text for the lang output
        # This is really just a minor styling feature
        custom_lang = self.Deck[r][13].upper()
        if custom_lang == "BR":
            custom_lang = "🇧🇷"
        elif custom_lang == "CN":
            custom_lang = "🇨🇳"
        elif custom_lang == "DE":
            custom_lang = "🇩🇪"
        elif custom_lang == "RU":
            custom_lang = "🇷🇺"
        else:
            custom_lang += "ERR" # This is here so when I re-enable others, I will know to add this to this if statement
        # Print DE or RU
        print(colored("" + custom_lang, styles['second']) + colored(" ↺ ", styles[">>>"]) +  colored(self.Deck[r][1], styles['first']))
        # Pause
        sleep(self.pause)
        # Reveal Answer
        print(colored("→ ", styles[">>>"]) + colored(self.Deck[r][3], styles['third']))
        # Play Sound
        audio_chk = str_to_bool(self.Deck[r][4])
        if audio_chk is not False:
            if self.__PLAYAUDIO__:
                self.play_audio(str(self.Deck[r][13]), self.Deck[r][4])

        self.ask_answer(self.Deck[r], ds, r)

        # LOAD next card - or finish script
        if len(self.Deck) > 0:
            print(colored("\nThere are " + str(len(self.Deck)) + " cards left.\n", styles['left']))
            self.get_card()
        else:
            # TODO: Add here .. + 10 to ru daily count database
            print(colored("\nYou have finished the 10-10 deck!", styles['left']))

    def run(self):
        # Build Deck
        self.build_deck(lang_tables['de'])
        self.build_deck(lang_tables['ru'])
        # Add Some Brazilian?
        if self.__INC_BR__:
            self.build_deck(lang_tables['br'])
        # Add Some Chinese?
        if self.__INC_CN__:
            self.build_deck(lang_tables['cn'])
        # Add Some Old Norse?
        if self.__INC_ON__:
            self.build_deck(lang_tables['on'])
        # Add Some Spanish?
        if self.__INC_SPA__:
            self.build_deck(lang_tables['spa'])
        # Combined All
        self.combine_decks()
        # Start Review
        print()
        self.get_card()
        # Quit
        quit()

if __name__ == "__main__":
    a = Ten_Ten()
    a.run()
