# This Python file uses the following encoding: utf-8
from Russian_Flash_Cards import *

class SpecificCategory():

    Deck = []

    def run_audio(self, fp, ext):
        music = pyglet.media.load(fp + ext)
        music.play()

    def play_audio(self, sound):
        fdir = "ru"
        snd = WINDOWS_PATH + "\\audio\\" + fdir + "\\" + sound
        # Try each audio extension
        try:
            self.run_audio(snd, ".mp3")
        except Exception as e:
            if SHOW_EXCEPTION_ERRORS:
                print(".mp3 not found\n" + str(e))
            try:
                self.run_audio(snd, ".wav")
            except Exception as e:
                if SHOW_EXCEPTION_ERRORS:
                    print(".wav not found\n" + str(e))
                try:
                    self.run_audio(snd, ".ogg")
                except Exception as e:
                    if SHOW_EXCEPTION_ERRORS:
                        print(".ogg not found\n" + str(e))
                    print("~~ AUDIO FILE MISSING " + str(snd) + "  ~~")

    def review(self, deck, last=-1):
        if len(deck) == 0:
            pass
        else:
            print(str(len(deck)) + " cards left in this deck.\n")
            if len(deck) == 1:
                r = 0
            else:
                r = my_randint(len(deck))
                if len(deck) > 1:
                    while r == last:
                        r = my_randint(len(deck))
        #
        Card = deck[r]

        #
        ws_chk = check_for_whitespace(deck[r], lang_tables['ru'])
        if ws_chk is True:
            word = deck[r][1].strip()
        else:
            word = deck[r][1]
        if len(word) > long:
            word = split_long_sentences(word)
        cat = deck[r][7]
        if cat is 35 or cat is 46 or cat is 47 or cat is 48:
            pause_text = 0.3
            if len(deck[r][1]) <= long:
                pause_audio = 10.0
            else:
                pause_audio = 15.0
        else:
            pause_text = 0.3
            pause_audio = 4.4


        pro = deck[r][2]
        if pro is None or pro == "":
            if curr_db == lang_tables['ru']:
                pro = transliterate(word, cyrillic_translit_RU)
                update_item(pro, curr_db, "pro", deck[r][0])
        else:
            pro = pro.strip()
        if pro is not None:
            if len(pro) > long:
                pro = split_long_sentences(pro)
        # Audio / Gender Init
        audio_chk = str_to_bool(deck[r][4])
        gender_chk = str_to_bool(deck[r][6])
        # ENG translation Init
        en = deck[r][3]
        en = en.strip()
        if len(en) > long:
            en = split_long_sentences(en)
        # OUTPUT WORD
        if Reverse:
            if en != "":
                print(colored(str(en), 'blue', attrs=['bold']))
            else:
                print("ENGLISH IS NOT SUPPLIED FOR THIS WORD.")
        else:
            print(colored(str(word), 'blue', attrs=['bold']))
        # Play Audio
        if audio_chk is not False:
            sleep(pause_audio)
            self.play_audio(str(deck[r][4]))
            sleep(pause_text)
        else:
            sleep(pause_no_audio)

        #
        while True:
            corr_incorr = input(str("Did you get this correct?"))
            ds = '' + str(year) + " " + str(month) + " " + str(day) + ''
            if corr_incorr is 'y' or corr_incorr is '1':
                add_to_correct_incorrect_count(str(deck[r][0]), lang_tables['ru'], 0, str(ds))
                break
            elif corr_incorr is 'n' or corr_incorr is '0' or corr_incorr is '4':
                add_to_correct_incorrect_count(str(deck[r][0]), lang_tables['ru'], 1, str(ds))
                break
            elif corr_incorr is 'c' or corr_incorr is '2':
                break
            else:
                continue

        #
        if corr_incorr is not '2':
            del deck[r]
        if(len(deck) > 0):
            self.review(deck, r)
        else:
            print("\nDECK COMPLETE!!!\n")



    def build_deck(self, cat):
        conn = mysql.connector.connect(**config)
        cur = conn.cursor()
        query = "SELECT * FROm ru_words WHERE category='"+cat+"' AND active='1'"
        cur.execute(str(query))
        for i in cur:
            self.Deck.append(i)
        cur.close()
        conn.close()

    def run(self):
        global OnRU
        OnRU = True
        options = "[1] Colors [2] Household [3] Food and Drink [4] Days of the week [5] Bad / Vulgar  [6] Names"
        options += "\n[7] Relationship Topics [8] Friends and Family [9] Clothes"
        answer = input(str(options))
        print()
        if answer == '1':
            self.build_deck('7')
            self.review(self.Deck)
        elif answer == '2':
            self.build_deck('21')
            self.review(self.Deck)
        elif answer == '3':
            self.build_deck('13')
            self.review(self.Deck)
        elif answer == '4':
            self.build_deck('9')
            self.review(self.Deck)
        elif answer == '5':
            self.build_deck('49')
            self.build_deck('65')
            self.review(self.Deck)
        elif answer == '6':
            self.build_deck('31')
            self.review(self.Deck)
        elif answer == '7':
            self.build_deck('47')
            self.build_deck('48')
            self.review(self.Deck)
        elif answer == '8':
            self.build_deck('17')
            self.review(self.Deck)
        elif answer == '9':
            self.build_deck('4')
            self.review(self.Deck)
        OnRU = False
