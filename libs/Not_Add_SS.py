# This Python file uses the following encoding: utf-8
from Russian_Flash_Cards import *
import glob
import ntpath
from subprocess import DEVNULL, STDOUT, check_call


class NotAddedScreenshots:
    folder = SS_TODO_PATH
    images = []
    image_ext = ["jpg", "jpeg", "png", "gif", "webp"] # There shouldn't be any gif's, but I added just incase.
    excludes = [
        folder + "Screenshot_2017-04-21-21-23-11.png",
        folder + "Screenshot_2017-04-21-21-23-13.png",
        folder + "Screenshot_2017-04-21-23-40-43.png",
        folder + "Screenshot_2017-04-22-00-10-26.png"
    ]


    def query_images(self):
        # print(WINDOWS_PATH)
        # print(self.folder)
        for ext in self.image_ext:
            a = glob.glob(self.folder + "*." + ext)
            for aa in a:
                # print(aa)
                if aa not in self.excludes:
                    self.images.append(aa)
        print(len(self.images))
        # exit()

    def get_random_image(self):
        r = my_randint(len(self.images))
        # print(str(r))
        # exit()
        file = self.images[r]
        if ON_LINUX:
            # subprocess.call(["xdg-open", file])
            call(["xdg-open", file])
        else:
            os.startfile(file)

        inp = input(str("Press any key when ready."))

        if ON_LINUX:
            pass
        else:
            window_title = ntpath.basename(file) + " - " + IMAGE_PROGRAM_TITLE
            check_call('taskkill /fi "WINDOWTITLE eq ' + window_title + '"', stdout=DEVNULL, stderr=STDOUT)
        del self.images[r]
        if len(self.images) > 0:
            self.get_random_image()
        else:
            print("All done!")
            exit()

    def run(self):
        self.query_images()
        self.get_random_image()

if __name__ == "__main__":
    a = NotAddedScreenshots()
    a.run()
