from libs import *
from Russian_Flash_Cards import *

class Challenge:

    ChallengeDeck = []
    Pause = 25

    def query_deck(self):
        self.ChallengeDeck = []
        table = "3x_challenge"
        conn = mysql.connector.connect(**config)
        cur = conn.cursor()
        cur.execute("SELECT * FROM " + table + " WHERE enabled='1' ORDER BY id ASC")
        for w in cur:
            self.ChallengeDeck.append(w)
        cur.close()
        conn.close()
        pass

    def get_card(self, deck):
        # SELECT DECK ITEM
        r = randint(0, len(deck)-1)
        ru = deck[r][1]
        de = deck[r][2]
        en = deck[r][3]

        # CHOOSE WHICH LANG TO SHOW
        r2 = randint(1,3)

        # SET QUESTION / ANSWER ORDER
        if r2 == 1:
            first = ru
            second = en
            third = de
        elif r2 == 2:
            first = de
            second = en
            third = ru
        elif r2 == 3:
            first = en
            second = de
            third = ru

        # SHOW FIRST
        print(colored(first, "green"))
        sleep(self.Pause)

        # GIVE ANSWERS
        print(colored(second, "yellow"))
        print(colored(third, "blue"))

        # MOVE ON
        del deck[r]
        if len(deck) > 0:
            print("\n" + str(len(deck)) + " cards to go.\n")
            input(str("Press any key to move on to the next card..."))
            print("\n")
            self.get_card(deck)
        else:
            print("All done!")

    def run(self):
        self.query_deck()
        #print(str(len(self.ChallengeDeck)))
        print("\n" + str(len(self.ChallengeDeck)) + " cards to go.\n")
        self.get_card(self.ChallengeDeck)
        quit()

"""
a = Challenge()
a.run()
"""