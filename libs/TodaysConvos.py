# This Python file uses the following encoding: utf-8
from Russian_Flash_Cards import *
import random
pyglet.options['search_local_libs'] = True

'''
    FFMPEG is needed here, because Russian Accelerator uses an odd codec that Pyglet can not play.
    Also, since I had to add FFMPEG, I added a few videos to the list.
'''

class TodaysConvos():
    today = datetime.date.today()
    today = today.strftime("%d")

    # Pimsleur Variables
    random_pims_course_level = True # When I start level 2, I will need to turn this to False until I am done.
    current_pims_level_max = 1 # 1-3 - Used when above is True

    # Russian Accelerator Variables
    rand_rua_course_unit = True
    rand_rua_course_lesson = True
    min_rua_unit = 1
    max_rua_unit = 10 # Max is 19
    max_rua_lesson = 1
    min_rua_otg = 1
    max_rua_otg = 3 # Max is 5

    @staticmethod
    def check_exist(dir, file): # NOT IMPLEMENTED
        count = 0
        for path, subdirs, files in os.walk(dir):
            for filename in files:
                if file in filename:
                    print(path + filename)
                    count = count + 1
        if count > 0:
            return True
        else:
            return False


    @staticmethod
    def find_misc_tracks(dir):
        l = []
        for path, subdirs, files in os.walk(dir):
            for filename in files:
                bad = ["txt", "png", "jpg", "gif"]
                temp = dir + os.path.basename(path) + filename
                fn, ext = os.path.splitext(temp)
                ext = ext.replace(".","")
                if ext not in bad:
                    l.append(temp)
        return l

    def set_date(self):
        self.today = datetime.date.today()
        self.today = self.today.strftime("%d")

    def convert_file(self, f, out="TodaysConvo.mp3"): # out arg isn't needed, but I added incase I change things
        global ON_LINUX
        print("\nAbout to convert: " + f +"\n")
        output = TEMP_FILE_LOC + "\\" + out
        cwd = os.getcwd()

        ffmpeg = ""
        if FFMPEG_LOC:
            ffmpeg = FFMPEG_LOC
        else:
            ffmpeg = LIB_DIR + "\\ffmpeg.exe"

        if ON_LINUX or ON_MAC: # Not tested for mac
            cmd = 'ffmpeg -y -i "' + f + '" -vn -ar 44100 -ac 2 -ab 192k -f mp3 "' + output + '"'
            check_call(shlex.split(cmd)) # This splits the options for us!
        else: # Windows
             if "libs" in cwd:
                cmd = 'ffmpeg -y -i "' + f + '" -vn -ar 44100 -ac 2 -ab 192k -f mp3 "' + output + '"'
                os.system(cmd)
             else:
                cmd = '"' + ffmpeg + '" -y -i "' + f + '" -vn -ar 44100 -ac 2 -ab 192k -f mp3 "' + output + '"'
                call(cmd) # For some reason we need to use call here, so hopefully all windows are okay with it.

        self.run_audio(output)

    @staticmethod
    def run_audio(f):
        global ON_LINUX
        print("Now playing: " + f)
        if ON_LINUX:
            call(["mpg123", "--quiet", f])
        else:
            music = pyglet.media.load(f)
            music.play()


    def play_pims(self):
        if PIMS_CONVO_PATH is not False:
            self.set_date()
            d = self.today.lstrip('0') # Removes the 0 from single digit ints
            level = 0
            if self.random_pims_course_level:
                while level < 1: # Using a while loop, because even with +1 it still can come up 0 somehow
                    level = my_randint(self.current_pims_level_max + 1)
            else:
                level = 1
            path = PIMS_CONVO_PATH + "Level " + str(level) + "/"
            #full = path + "Day " + str(self.today) + ".mp3"
            full = path + "Day " + str(d) + ".mp3"
            try:
                self.run_audio(full)
            except:
                print("\n" + full + " most likely doesn't exist.")
        else:
            print("\nSkipping Pimsleur audio.")


    def play_ruaccel_lesson(self):
        if RUACCCEL_CONVO_PATH is not False:
            self.set_date()
            chosen_unit = 0
            chosen_lesson = 0
            path = RUACCCEL_CONVO_PATH + "0 Complete Series - TO DO/"

            no_lesson = [ # [ UNIT, LESSON ]
                [1, 1], # 1,1 is not skipped...why?
                [6, 1], [6, 2], [6, 4], [6, 5],
                [7, 5],
            ]

            has_two_lessons = [  # [ UNIT, LESSON ]
                [7, 1], [7, 2],
                [8, 5]
            ]

            if self.rand_rua_course_unit:
                chosen_unit = random.randint(self.min_rua_unit, self.max_rua_unit)
            else:
                chosen_unit = self.max_rua_unit

            if self.rand_rua_course_lesson:
                if chosen_unit is self.max_rua_unit:
                    # Only use max lesson number
                    chosen_lesson = random.randint(1, self.max_rua_lesson)
                else:
                    # Can be 1-5
                    chosen_lesson = random.randint(1, 5)
            else:
                chosen_lesson = self.max_rua_lesson

            test = [ chosen_unit, chosen_lesson ]

            if test in no_lesson: # Restart, because that unit/lesson combo has no audio file.
                self.play_ruaccel_lesson()

            file = ""
            if test in has_two_lessons:
                r2 = random.randint(1,2)
                if r2 is 1:
                    file = "Listening Practice"
                else:
                    file = "Listening Practice 2"
            else:
                file = "Listening Practice"

            # Now we put them all together
            path = path + "Unit " + str(chosen_unit) + "/"
            path = path + "Lesson " + str(chosen_lesson) + "/"
            full = path + file + ".mp3"

            print("\nChoose " + full + "\n")
            self.convert_file(full)
        else:
            print("\nSkipping Russian Accelerater Lesson audio.")

    def play_ruaccel_listen_answer(self, return_file=False):
        if RUACCCEL_CONVO_PATH is not False:
            self.set_date() # Not needed here, just reseting... "reasons"
            path = RUACCCEL_CONVO_PATH + "Listen and Answer/"
            files = [
                "RAM-QNA-AUDIO-V1P2.mp3", "RAM-QNA-AUDIO-V1P3.mp3", "RAM-QNA-AUDIO-V1P2.mp3",
                "RAM-QNA-VIDEO-V1P1-1-ccc.mp4", "RAM-QNA-VIDEO-V1P2-1-ccc.mp4"
            ]
            r = my_randint(len(files))
            full = path + files[r]
            if return_file:
                return full
            else:
                self.convert_file(full)
        else:
            print("\nSkipping Russian Accelerater Listen and Answer audio.")

    def play_ruaccel_secret(self):
        # Secret Phrases Russian Women want to hear.
        # This isn't exactly a convo, but I decided to put it here.
        if RUACCCEL_CONVO_PATH is not False:
            self.set_date() # Not needed here, just reseting... "reasons"
            path = RUACCCEL_CONVO_PATH + "Secret Phrases/audio/"
            max = 101
            r = my_randint(max)
            full = path + str(r) + ".mp3"
            self.convert_file(full)
        else:
            print("\nSkipping Russian Accelerater Secret Phrases audio.")


    def play_ruaccel_grammar(self):
        self.set_date()
        # TO DO

    def play_ruaccel_verbal(self):
        self.set_date()
        # TO DO

    def play_ruaccel_otg(self): # Russian Accelerator  - On The Go
        self.set_date()
        path = RUACCCEL_CONVO_PATH + "\\On The Go Audio\\"
        r = my_randint(self.max_rua_otg)
        path = path + "CD " + str(r) + "\\"
        misc_files = self.find_misc_tracks(path)
        r = my_randint(len(misc_files))
        if r < 0:
            r = 0
        full = misc_files[r]
        self.convert_file(full)


    def play_misc_listening_practice(self):
        global INIT_DIR
        p = "\\audio\\ru_misc_listening\\"
        p2 = "\\audio\\ru_listen\\"
        if "libs" in INIT_DIR:
            path = INIT_DIR.replace("\\libs", "")
            path = path + p
            path2 = path + p2
        else:
            path = INIT_DIR + p
            path2 = INIT_DIR + p2

        #print(path)
        #exit()
        list_a = self.find_misc_tracks(path)
        list_b = self.find_misc_tracks(path2)
        misc_files = list_a + list_b
        r = my_randint(len(misc_files))
        full = misc_files[r]
        self.convert_file(full)

    def busuu_listening_practice(self):
        global INIT_DIR
        p = "\\audio\\ru_busuu\\"
        if "libs" in INIT_DIR:
            path = INIT_DIR.replace("\\libs", "")
            path = path + p
        else:
            path = INIT_DIR + p

        misc_files = self.find_misc_tracks(path)
        r = my_randint(len(misc_files))
        full = misc_files[r]
        self.convert_file(full)


    def run(self):
        run = True
        while(run):
            inp = input(str("\n[1] Pimsleur [2] RU Accel [3] RU Accel Q/A [4] RU Accell Secret [5] Busuu [6] Misc Practice [7] RU Accel OTG [exit]: "))
            inp_temp = strip_to_nums(inp)
            if inp_temp is not '':
                inp = inp_temp
            if inp is "1":
                self.play_pims()
            elif inp is "2":
                self.play_ruaccel_lesson()
            elif inp is "3":
                self.play_ruaccel_listen_answer()
            elif inp is "4":
                self.play_ruaccel_secret()
            elif inp is "5":
                self.busuu_listening_practice()
            elif inp is "6":
                self.play_misc_listening_practice()
            elif inp is "7":
                self.play_ruaccel_otg()
            elif "exit" in inp.lower():
                print()
                run = False


if __name__ == "__main__":
    a = TodaysConvos()
    a.run()
    exit()
