# This Python file uses the following encoding: utf-8
from Russian_Flash_Cards import *
if HAVE_GST and INTERNET_SOUNDS:
	import pygst
	import gst
if HAVE_VLC and INTERNET_SOUNDS:
	import vlc

class Flashcards:

    queARA = []
    queBR = []
    queCN = []
    queDE = []
    queESP = []
    queFI = []
    queLTV = []
    queON = []
    queRU = []
    queSE = []
    queSPA = []
    queUKR = []

    ARA = []
    BR = []
    CN = []
    DE = []
    ESP = []
    FI = []
    LTV = []
    ON = []
    RU = []
    SE = []
    SPA = []
    UKR = []

    ARAT = []
    BRT = []
    CNT = []
    DET = []
    ESPT = []
    FIT = []
    LTVT = []
    ONT = []
    RUT = []
    SET = []
    SPAT = []
    UKRT = []

    customRU = [] # This really should be renamed
    RUROM = [] # Romance Related

    review_q = "Would you like to review "
    trbl_q = "Trouble words only?"
    reverse_q = "Reverse test?"

    only_older = False # This is actually double implemented if you look at RU "customRU"

    @staticmethod
    def run_audio(fp, ext):
        global ON_LINUX
        if ON_LINUX:
            call(["mpg123", "--quiet", fp + ext])
        else:
            # music = pyglet.resource.media(fp + ext)
            music = pyglet.media.load(fp + ext)
            music.play()

    @staticmethod
    def run_audio_online(url):
        if not ON_LINUX:
            if HAVE_VLC:
                try:
                    p = vlc.MediaPlayer(url)
                    p.play()
                    #  print("vlc used")
                except Exception as e:
                    print(e)
                    #  pass
            elif HAVE_GST:
                def on_tag(bus, msg):
                    taglist = msg.parse_tag()
                    print
                    'on_tag:'
                    for key in taglist.keys():
                        print
                        '\t%s = %s' % (key, taglist[key])

                # our stream to play
                music_stream_uri = url
                player = gst.element_factory_make("playbin", "player")
                player.set_property('uri', music_stream_uri)
                player.set_state(gst.STATE_PLAYING)
                bus = player.get_bus()
                bus.enable_sync_message_emission()
                bus.add_signal_watch()
                bus.connect('message::tag', on_tag)
            else:
                pass
        else:
            pass #  Can I do this easy in linux?

    def play_audio(self, sound):
        global OnBR
        global OnDE
        global OnCN
        global OnESP
        global OnFI
        global OnLTV
        global OnON
        global OnRU
        global OnSE
        global OnSPA
        global OnUKR
        fdir = ""
        if OnBR:
            fdir = "br"
        elif OnCN:
            fdir = "cn"
        elif OnDE:
            fdir = "de"
        elif OnESP:
            fdir = "esp"
        elif OnFI:
            fdir = "fi"
        elif OnLTV:
            fdir = "ltv"
        elif OnON:
            fdir = "on"
        elif OnRU:
            fdir = "ru"
        elif OnSE:
            fdir = "se"
        elif OnSPA:
            fdir = "spa"
        elif OnUKR:
            fdir = "ukr"
        # Linux or Windows?
        if ON_LINUX:
            snd = LINUX_PATH + "/audio/" + fdir + "/" + sound
        else:
            snd = WINDOWS_PATH + "\\audio\\" + fdir + "\\" + sound

        # LOCAL --- Try each audio extension
        if not INTERNET_SOUNDS and play_sounds:
            try:  #  All audio files should be .mp3 now, but we try all if needed!
                self.run_audio(snd, ".mp3")
            except Exception as e:
                if SHOW_EXCEPTION_ERRORS:
                    print(".mp3 not found\n" + str(e))
                try:
                    self.run_audio(snd, ".wav")
                except Exception as e:
                    if SHOW_EXCEPTION_ERRORS:
                        print(".wav not found\n" + str(e))
                    try:
                        self.run_audio(snd, ".ogg")
                    except Exception as e:
                        if SHOW_EXCEPTION_ERRORS:
                            print(".ogg not found\n" + str(e))
                        print("~~ AUDIO FILE MISSING " + str(snd) + "  ~~")
        else:
            #  print(BASE_AUDIO_URL + "ru/" + sound + INTERNET_EXT)
            if OnRU:
                self.run_audio_online(BASE_AUDIO_URL + "ru/" + sound + INTERNET_EXT)

    def review(self, ReviewDeck, last=-1):
        global OnBR
        global OnCN
        global OnDE
        global OnESP
        global OnFI
        global OnLTV
        global OnON
        global OnRU
        global OnSE
        global OnSPA
        global OnUKR
        global customRU
        global RUROM #
        if len(ReviewDeck) == 0:
            pass
        else:
            print(str(len(ReviewDeck)) + " cards left in this deck.\n")
            if len(ReviewDeck) == 1:
                r = 0
            else:
                r = my_randint(len(ReviewDeck))
                if len(ReviewDeck) > 1:
                    while r == last:
                        r = my_randint(len(ReviewDeck))
            curr_db = ""
            if OnBR:
                curr_db = lang_tables['br']
            elif OnCN:
                curr_db = lang_tables['cn']
            elif OnDE:
                curr_db = lang_tables['de']
            elif OnESP:
                curr_db = lang_tables['esp']
            elif OnFI:
                curr_db = lang_tables['fi']
            elif OnLTV:
                curr_db = lang_tables['ltv']
            elif OnON:
                curr_db = lang_tables['on']
            elif OnRU:
                curr_db = lang_tables['ru']
            elif OnSE:
                curr_db = lang_tables['se']
            elif OnSPA:
                curr_db = lang_tables['spa']
            elif OnUKR:
                curr_db = lang_tables['ukr']
            # Category and timing Init
            cat = ReviewDeck[r][7]
            if cat is 35 or cat is 46 or cat is 47 or cat is 48:
                pause_text = 0.3
                if len(ReviewDeck[r][1]) <= long:
                    pause_audio = 10.0
                else:
                    pause_audio = 15.0
            else:
                pause_text = 0.3
                pause_audio = 4.4
            pause_no_audio = pause_text + pause_audio
            # Word Init
            ws_chk = ""
            if OnBR:
                ws_chk = check_for_whitespace(ReviewDeck[r], lang_tables['br'])
            elif OnCN:
                ws_chk = check_for_whitespace(ReviewDeck[r], lang_tables['cn'])
            elif OnDE:
                ws_chk = check_for_whitespace(ReviewDeck[r], lang_tables['de'])
            elif OnESP:
                ws_chk = check_for_whitespace(ReviewDeck[r], lang_tables['esp'])
            elif OnFI:
                ws_chk = check_for_whitespace(ReviewDeck[r], lang_tables['fi'])
            elif OnLTV:
                ws_chk = check_for_whitespace(ReviewDeck[r], lang_tables['ltv'])
            elif OnON:
                ws_chk = check_for_whitespace(ReviewDeck[r], lang_tables['on'])
            elif OnRU:
                ws_chk = check_for_whitespace(ReviewDeck[r], lang_tables['ru'])
            elif OnSE:
                ws_chk = check_for_whitespace(ReviewDeck[r], lang_tables['se'])
            elif OnSPA:
                ws_chk = check_for_whitespace(ReviewDeck[r], lang_tables['spa'])
            elif OnUKR:
                ws_chk = check_for_whitespace(ReviewDeck[r], lang_tables['ukr'])
            if ws_chk is True:
                word = ReviewDeck[r][1].strip()
            else:
                word = ReviewDeck[r][1]
            if len(word) > long:
                word = split_long_sentences(word)
            # Pronounce Init
            pro = ReviewDeck[r][2]
            if pro is None or pro == "":
                if curr_db == lang_tables['ru']:
                    pro = transliterate(word, cyrillic_translit_RU)
                    update_item(pro, curr_db, "pro", ReviewDeck[r][0])
                '''
                elif curr_db == lang_tables['de']:
                    pro = transliterate(word, german_translite)
                    update_item(pro, curr_db, "pro", ReviewDeck[r][0])
                elif curr_db == lang_tables['ukr']:
                    pro = transliterate(word, cyrillic_translit_UKR)
                    update_item(pro, curr_db, "pro", ReviewDeck[r][0])
                elif curr_db == lang_tables['ltv']:
                    pro = transliterate(word, latvian_translit)
                    update_item(pro, curr_db, "pro", ReviewDeck[r][0])
                '''
            else:
                pro = pro.strip()
            if pro is not None:
                if len(pro) > long:
                    pro = split_long_sentences(pro)
            # Audio / Gender Init
            audio_chk = str_to_bool(ReviewDeck[r][4])
            gender_chk = str_to_bool(ReviewDeck[r][6])
            # ENG translation Init
            en = ReviewDeck[r][3]
            en = en.strip()
            if len(en) > long:
                en = split_long_sentences(en)
            # OUTPUT WORD
            if Reverse:
                if en != "":
                    print(colored(str(en), 'blue', attrs=['bold']))
                else:
                    print("ENGLISH IS NOT SUPPLIED FOR THIS WORD.")
            else:
                print(colored(str(word), 'blue', attrs=['bold']))
            # Play Audio
            if audio_chk is not False:
                sleep(pause_audio)
                if play_sounds:
                    self.play_audio(str(ReviewDeck[r][4]))
                sleep(pause_text)
            else:
                sleep(pause_no_audio)
            # Output "pro"
            print(colored(">>> ", 'yellow') + colored(str(pro), 'magenta'))
            # Type Init
            type_chk = str_to_bool(ReviewDeck[r][5])
            type = ""
            if type_chk is not False:
                type += colored(">>> ", "yellow") + colored(str(ReviewDeck[r][5]), 'green')
            if gender_chk is not False:
                if type_chk is not False:
                    type += colored(" (", 'blue') + colored(str(ReviewDeck[r][6]), 'red') + colored(")", 'blue')
                else:
                    type += colored(">>>", 'yellow')
                    type += colored(" (", 'blue') + colored(str(ReviewDeck[r][6]), 'red') + colored(")", 'blue')
            # CARD OUTPUT
            if Reverse:
                print(colored(">>> ", 'yellow') + colored(str(word), 'blue'))
            else:
                if en != "":
                    print(colored(">>> ", 'yellow') + colored(str(en), 'red'), end="\n")
            if len(type) > 0:
                print(str(type) + "\n")
            # While loop to use above and set results
            while True:
                corr_incorr = input(str("Did you get this correct?"))
                #corr_incorr = corr_incorr.lower()
                cinc_temp = strip_to_nums(corr_incorr)
                if cinc_temp is not '':
                    corr_incorr = cinc_temp
                curr_date = get_date()
                ds = '' + str(curr_date[0]) + " " + str(curr_date[1]) + " " + str(curr_date[2]) + ''
                # If correct
                if corr_incorr is 'y' or corr_incorr is '1':
                    try:
                        add_to_correct_incorrect_count(str(ReviewDeck[r][0]), str(curr_db), 0, str(ds))
                        update_daily_count(0)
                        if ReviewDeck[r][12] != 1 or ReviewDeck[r][11] > 4:
                            ratio = get_ratio(int(ReviewDeck[r][11]+1), ReviewDeck[r][12])
                            check_trouble_ratio(ReviewDeck[r], ratio, curr_db, r, ReviewDeck)
                        del ReviewDeck[r]
                        break
                    except TimeoutError as e:
                        print("DB connection lost, quiting")
                        quit()
                    except mysql.connector.errors.InterfaceError:
                        print("DB connection lost, quiting")
                        quit()
                # If INcorrect
                elif corr_incorr is 'n' or corr_incorr is '0' or corr_incorr is '4':
                    try:
                        add_to_correct_incorrect_count(str(ReviewDeck[r][0]), str(curr_db), 1, str(ds))
                        update_daily_count(1)
                        if ReviewDeck[r][12]+1 > 1:
                            ratio = get_ratio(ReviewDeck[r][11], int(ReviewDeck[r][12]+1))
                            check_trouble_ratio(ReviewDeck[r], ratio, curr_db, r, ReviewDeck)
                        if corr_incorr is '4':
                            pass  # Make it change to trouble here (if I end up using this trigger in the future)
                        break
                    except TimeoutError as e:
                        print("DB connection lost, quiting")
                        quit()
                    except mysql.connector.errors.InterfaceError:
                        print("DB connection lost, quiting")
                        quit()
                # Skip
                elif corr_incorr is 'c' or corr_incorr is '2':
                    # print("\nSKIPPING\n")
                    break
                # Correct but make Trouble Word
                elif corr_incorr is '3':
                    try:
                        update_item(ReviewDeck[r][11]+1, curr_db, 'times_correct', ReviewDeck[r][0])
                        update_daily_count(0)
                        if ReviewDeck[r][10] is not '0' or ReviewDeck[r][10] is not 0:
                            update_item('1', curr_db, 'trouble_word', ReviewDeck[r][0])
                        print("You have got this word correct", str(ReviewDeck[r][11]+1), "time(s) and incorrect",
                              str(ReviewDeck[r][12]), "time(s).")
                        print()
                        del ReviewDeck[r]
                        break
                    except TimeoutError as e:
                        print("DB connection lost, quiting" + "\n" + e)
                        quit()
                    except mysql.connector.errors.InterfaceError:
                        print("DB connection lost, quiting")
                        quit()
                # Replay Sound
                elif corr_incorr is 'r' or corr_incorr is 're':
                    if audio_chk is not False:
                        self.play_audio(str(ReviewDeck[r][4]))
                    else:
                        print("No audio file for this word/sentence, sorry.")
                    continue
                elif corr_incorr == 'play':
                    f = input(str("What file to play? (must be part of current lang.): "))
                    self.play_audio(f)
                    continue
                elif corr_incorr == 'help':
                    temp = "\n[0]Incorrect [1]Correct [2]Skip [3]Correct and add to Trouble [4]Wrong and add to Trouble\n"
                    temp += "[r or re]Replay sound [play] Play Specific sound file [back] Back to Selections [exit] Exit App\n"
                    print(temp)
                    del temp
                    continue
                elif corr_incorr == 'back':
                    break
                elif corr_incorr.lower() == 'e' or corr_incorr.lower() == 'exit':
                    exit()
                # Bad input
                else:
                    continue
            # Next Word
            if corr_incorr == '0' and len(ReviewDeck) == 1:
                pass
            elif corr_incorr == 'back':
                pass
            else:
                self.review(ReviewDeck, r)

    def fill_arrays(self):
        global customRU  #
        # Query all activated words
        try:
            if len(self.queBR) < 1:
                query_active_words(lang_tables['br'], self.queBR)
            if len(self.queCN) < 1:
                query_active_words(lang_tables['cn'], self.queCN)
            if len(self.queDE) < 1:
                query_active_words(lang_tables['de'], self.queDE)
            if len(self.queESP) < 1:
                query_active_words(lang_tables['esp'], self.queESP)
            if len(self.queFI) < 1:
                query_active_words(lang_tables['fi'], self.queFI)
            if len(self.queLTV) < 1:
                query_active_words(lang_tables['ltv'], self.queLTV)
            if len(self.queON) < 1:
                query_active_words(lang_tables['on'], self.queON)
            if len(self.queRU) < 1:
                query_active_words(lang_tables['ru'], self.queRU)
            if len(self.queSE) < 1:
                query_active_words(lang_tables['se'], self.queSE)
            if len(self.queSPA) < 1:
                query_active_words(lang_tables['spa'], self.queSPA)
            if len(self.queUKR) < 1:
                query_active_words(lang_tables['ukr'], self.queUKR)
        except Exception as e:
            if SHOW_EXCEPTION_ERRORS:
                print("Error while running query_active_words()\n" + str(e))
            else:
                print("Database is most likely down.\nTo see the full error, change SHOW_EXCEPTION_ERRORS to True\n")
            quit()
        # Build regular decks
        try:
            if len(self.BR) < 1:
                build_review_deck(self.queBR, self.BR)
            if len(self.CN) < 1:
                build_review_deck(self.queCN, self.CN)
            if len(self.DE) < 1:
                build_review_deck(self.queDE, self.DE)
            if len(self.ESP) < 1:
                build_review_deck(self.queESP, self.ESP)
            if len(self.FI) < 1:
                build_review_deck(self.queFI, self.FI)
            if len(self.LTV) < 1:
                build_review_deck(self.queLTV, self.LTV)
            if len(self.ON) < 1:
                build_review_deck(self.queON, self.ON)
            if len(self.RU) < 1:
                build_review_deck(self.queRU, self.RU, True)
            if len(self.SE) < 1:
                build_review_deck(self.queSE, self.SE)
            if len(self.SPA) < 1:
                build_review_deck(self.queSPA, self.SPA)
            if len(self.UKR) < 1:
                build_review_deck(self.queUKR, self.UKR)
        except Exception as e:
            if SHOW_EXCEPTION_ERRORS:
                print("Error while running build_review_deck()\n" + e.args)
            else:
                print("There was an error building review decks.\nChange SHOW_EXCEPTION_ERRORS to True for more info.\n")
            quit()
        # Build trouble decks
        try:
            if len(self.BRT) < 1:
                build_trouble_deck(lang_tables['br'], self.BRT)
            if len(self.CNT) < 1:
                build_trouble_deck(lang_tables['cn'], self.CNT)
            if len(self.DET) < 1:
                build_trouble_deck(lang_tables['de'], self.DET)
            if len(self.ESPT) < 1:
                build_trouble_deck(lang_tables['esp'], self.ESPT)
            if len(self.FIT) < 1:
                build_trouble_deck(lang_tables['fi'], self.FIT)
            if len(self.LTVT) < 1:
                build_trouble_deck(lang_tables['ltv'], self.LTVT)
            if len(self.ONT) < 1:
                build_trouble_deck(lang_tables['on'], self.ONT)
            if len(self.RUT) < 1:
                build_trouble_deck(lang_tables['ru'], self.RUT)
            if len(self.SET) < 1:
                build_trouble_deck(lang_tables['se'], self.SET)
            if len(self.SPAT) < 1:
                build_trouble_deck(lang_tables['spa'], self.SPAT)
            if len(self.UKRT) < 1:
                build_trouble_deck(lang_tables['ukr'], self.UKRT)
        except Exception as e:
            if SHOW_EXCEPTION_ERRORS:
                print(e)
            else:
                print("There was an error building trouble review decks.\n" +
                      "To see the full error, change SHOW_EXCEPTION_ERRORS to True\n")
        # Build Custom Deck(s)
        try:
            sql = "SELECT * FROM " + lang_tables['ru']
            sql += " WHERE active='1' AND category<>1 AND category<>31 AND category<>55 AND date LIKE '%2017%' "
            sql += "ORDER BY RAND()"
            build_custom_deck(sql, self.customRU, True)
        except Exception as e:
            if SHOW_EXCEPTION_ERRORS:
                print(e)
            else:
                print("There was an error building 2017 special deck.\n" +
                      "To see the full error, change SHOW_EXCEPTION_ERRORS to True\n")
        # Build Romantic Deck
        try:
            sql = "SELECT * FROM " + lang_tables['ru']
            sql += " WHERE active='1' AND category='48' "
            sql += "ORDER BY RAND()"
            build_custom_deck(sql, self.RUROM, True)
        except Exception as e:
            if SHOW_EXCEPTION_ERRORS:
                print(e)
            else:
                print("There was an error building 2017 special deck.\n" +
                      "To see the full error, change SHOW_EXCEPTION_ERRORS to True\n")

    def lang_select(self, lang_txt, Deck, TroubleDeck, QueriedDeck, Custom=[], Romance=[]):
        global Reverse
        global OnRU
        if len(Deck) > 0 or IGNORE_ZERO is True:
            if lang_txt is "Chinese":
                #Because I can only handle pinyin, not traditional or simplified Chinese :)
                q = input("\n" + self.review_q + str(len(Deck)) + " " + lang_txt + "(pinyin) cards?")
            else:
                q = input("\n" + self.review_q + str(len(Deck)) + " " + lang_txt + " cards?")
            q = strip_to_nums(q)
            if q in do_it:
                if lang_txt is "Chinese":
                    # For now, CN is so basic, I will just review ALL.
                    # This will need to be changed later, where the q2 is bumped up ABOVE this if
                    a = FlashcardsCN()
                    a.review(TroubleDeck)
                else:
                    q2_txt = "[Enter] Standard Review [1] Trouble Words ( " + str(len(TroubleDeck)) + " ) "
                    q2_txt += "[2] Review ALL ( " + str(len(QueriedDeck)) + " ) "
                    if OnRU:
                        q2_txt += "[3] 2017 no dates/names ( " + str(len(Custom)) + " ) "
                        q2_txt += "[4] Romantic Category ( " + str(len(Romance)) + " ) "
                    q2 = input(str(q2_txt))
                    q2 = strip_to_nums(q2)
                    q3 = input(str("[Enter] Show " + lang_txt + " [1] Show English "))
                    q3 = strip_to_nums(q3)
                    if q3 in do_it:
                        Reverse = True
                    if q2 == '1':
                        self.review(TroubleDeck)
                    elif q2 == '2':
                        self.review(QueriedDeck)
                    elif q2 == '3':
                        self.review(Custom)
                    elif q2 == '4':
                        self.review(Romance)
                    else:
                        self.review(Deck)
                    if q3 in do_it:
                        Reverse = False

    def run(self):
        global customRU  #
        global OnBR
        global OnCN
        global OnDE
        global OnESP
        global OnFI
        global OnLTV
        global OnON
        global OnRU
        global OnSE
        global OnSPA
        global OnUKR
        self.fill_arrays()
        OnRU = True
        self.lang_select("Russian", self.RU, self.RUT, self.queRU, self.customRU, self.RUROM)
        OnRU = False
        OnDE = True
        self.lang_select("German", self.DE, self.DET, self.queDE)
        OnDE = False
        OnCN = True
        # At some point, maybe I should change this so it has 2 options, traditional-simplified OR pinyin
        self.lang_select("Chinese", self.CN, self.CNT, self.queCN)
        OnCN = False
        OnON = True
        self.lang_select("Old Norse", self.ON, self.ONT, self.queON)
        OnON = False
        OnBR = True
        self.lang_select("Brazilian", self.BR, self.BRT, self.queBR)
        OnBR = False
        OnESP = True
        self.lang_select("Esperanto", self.ESP, self.ESPT, self.queESP)
        OnESP = False
        OnFI = True
        self.lang_select("Finnish", self.FI, self.FIT, self.queFI)
        OnFI = False
        OnLTV = True
        self.lang_select("Latvian", self.LTV, self.LTVT, self.queLTV)
        OnLTV = False
        OnSE = True
        self.lang_select("Swedish", self.SE, self.SET, self.queSE)
        OnSE = False
        OnSPA = True
        self.lang_select("Spanish", self.SPA, self.SPAT, self.queSPA)
        OnSPA = False
        OnUKR = True
        self.lang_select("Ukrainian", self.UKR, self.UKRT, self.queUKR)
        OnUKR = False
        # Small sleep to make sure (short) audio clips finish
        sleep(3)
        # FINISHED!
        print("\nYou're all caught up!\n\n")


########################################################################################################################
class FlashcardsCN(Flashcards):
    # Our override
    def review(self, ReviewDeck, last=-1):
        global OnCN
        global customRU  #
        if len(ReviewDeck) == 0:
            pass
        else:
            print(str(len(ReviewDeck)) + " cards left in this deck.\n")
            if len(ReviewDeck) == 1:
                r = 0
            else:
                r = my_randint(len(ReviewDeck))
                if len(ReviewDeck) > 1:
                    while r == last:
                        r = my_randint(len(ReviewDeck))
            curr_db = lang_tables['cn']
            # Category and timing Init
            cat = ReviewDeck[r][7]
            if cat is 35 or cat is 46 or cat is 47 or cat is 48:
                pause_text = 0.3
                if len(ReviewDeck[r][1]) <= long:
                    pause_audio = 10.0
                else:
                    pause_audio = 15.0
            else:
                pause_text = 0.3
                pause_audio = 4.4
            pause_no_audio = pause_text + pause_audio
            # Word Init
            ws_chk = check_for_whitespace(ReviewDeck[r], lang_tables['cn'])
            # We are using PRO field here, because I can only handle pinyin... otherwise, no need for this special class
            if ws_chk is True:
                word = ReviewDeck[r][2].strip()
            else:
                word = ReviewDeck[r][2]
            if len(word) > long:
                word = split_long_sentences(word)
            # Audio / Gender Init
            audio_chk = str_to_bool(ReviewDeck[r][4])
            gender_chk = str_to_bool(ReviewDeck[r][6])
            # ENG translation Init
            en = ReviewDeck[r][3]
            en = en.strip()
            if len(en) > long:
                en = split_long_sentences(en)
            # OUTPUT WORD
            if Reverse:
                if en != "":
                    print(colored(str(en), 'blue', attrs=['bold']))
                else:
                    print("ENGLISH IS NOT SUPPLIED FOR THIS WORD.")
            else:
                print(colored(str(word), 'blue', attrs=['bold']))
            # Play Audio
            if audio_chk is not False:
                sleep(pause_audio)
                if play_sounds:
                    self.play_audio(str(ReviewDeck[r][4]))
                sleep(pause_text)
            else:
                sleep(pause_no_audio)
            # Type Init
            type_chk = str_to_bool(ReviewDeck[r][5])
            type = ""
            if type_chk is not False:
                type += colored(">>> ", "yellow") + colored(str(ReviewDeck[r][5]), 'green')
            if gender_chk is not False:
                if type_chk is not False:
                    type += colored(" (", 'blue') + colored(str(ReviewDeck[r][6]), 'red') + colored(")", 'blue')
                else:
                    type += colored(">>>", 'yellow')
                    type += colored(" (", 'blue') + colored(str(ReviewDeck[r][6]), 'red') + colored(")", 'blue')
            # CARD OUTPUT
            if Reverse:
                print(colored(">>> ", 'yellow') + colored(str(word), 'blue'))
            else:
                if en != "":
                    print(colored(">>> ", 'yellow') + colored(str(en), 'red'), end="\n")
            if len(type) > 0:
                print(str(type) + "\n")
            # While loop to use above and set results
            while True:
                corr_incorr = input(str("Did you get this correct?"))
                #corr_incorr = corr_incorr.lower()
                ds = '' + str(year) + " " + str(month) + " " + str(day) + ''
                # If correct
                if corr_incorr is 'y' or corr_incorr is '1':
                    try:
                        add_to_correct_incorrect_count(str(ReviewDeck[r][0]), str(curr_db), 0, str(ds))
                        update_daily_count(0)
                        if ReviewDeck[r][12] != 1 or ReviewDeck[r][11] > 4:
                            ratio = get_ratio(int(ReviewDeck[r][11]+1), ReviewDeck[r][12])
                            check_trouble_ratio(ReviewDeck[r], ratio, curr_db, r, ReviewDeck)
                        del ReviewDeck[r]
                        break
                    except TimeoutError as e:
                        print("DB connection lost, quiting")
                        quit()
                    except mysql.connector.errors.InterfaceError:
                        print("DB connection lost, quiting")
                        quit()
                # If INcorrect
                elif corr_incorr is 'n' or corr_incorr is '0' or corr_incorr is '4':
                    try:
                        add_to_correct_incorrect_count(str(ReviewDeck[r][0]), str(curr_db), 1, str(ds))
                        update_daily_count(1)
                        if ReviewDeck[r][12]+1 > 1:
                            ratio = get_ratio(ReviewDeck[r][11], int(ReviewDeck[r][12]+1))
                            check_trouble_ratio(ReviewDeck[r], ratio, curr_db, r, ReviewDeck)
                        if corr_incorr is '4':
                            pass  # Make it change to trouble here (if I end up using this trigger in the future)
                        break
                    except TimeoutError as e:
                        print("DB connection lost, quiting")
                        quit()
                    except mysql.connector.errors.InterfaceError:
                        print("DB connection lost, quiting")
                        quit()
                # Skip
                elif corr_incorr is 'c' or corr_incorr is '2':
                    # print("\nSKIPPING\n")
                    break
                # Correct but make Trouble Word
                elif corr_incorr is '3':
                    try:
                        update_item(ReviewDeck[r][11]+1, curr_db, 'times_correct', ReviewDeck[r][0])
                        update_daily_count(0)
                        if ReviewDeck[r][10] is not '0' or ReviewDeck[r][10] is not 0:
                            update_item('1', curr_db, 'trouble_word', ReviewDeck[r][0])
                        print("You have got this word correct", str(ReviewDeck[r][11]+1), "time(s) and incorrect",
                              str(ReviewDeck[r][12]), "time(s).")
                        print()
                        del ReviewDeck[r]
                        break
                    except TimeoutError as e:
                        print("DB connection lost, quiting" + "\n" + e)
                        quit()
                    except mysql.connector.errors.InterfaceError:
                        print("DB connection lost, quiting")
                        quit()
                # Replay Sound
                elif corr_incorr is 'r' or corr_incorr is 're':
                    if audio_chk is not False:
                        self.play_audio(str(ReviewDeck[r][4]))
                    else:
                        print("No audio file for this word/sentence, sorry.")
                    continue
                elif corr_incorr == 'play':
                    f = input(str("What file to play? (must be part of current lang.): "))
                    self.play_audio(f)
                    continue
                elif corr_incorr == 'help':
                    temp = "\n[0]Incorrect [1]Correct [2]Skip [3]Correct and add to Trouble [4]Wrong and add to Trouble\n"
                    temp += "[r or re]Replay sound [play] Play Specific sound file [back] Back to Selections [exit] Exit app\n"
                    print(temp)
                    del temp
                    continue
                elif corr_incorr == 'back':
                    break
                elif corr_incorr == 'exit':
                    exit()
                # Bad input
                else:
                    continue
            # Next Word
            if corr_incorr == '0' and len(ReviewDeck) == 1:
                pass
            elif corr_incorr == 'back':
                pass
            else:
                self.review(ReviewDeck, r)
