# This Python file uses the following encoding: utf-8
from Russian_Flash_Cards import *

class ListeningPractice():

    OnWords = False
    OnSentences = False
    OnAll = False
    AllItems = []
    Words = []
    Sentences = []
    """
    def __init__(self):
        self.query_decks()
        self.filter_big_list()
    """
    def run_audio(self, fp, ext):
        if ON_LINUX:
            call(["mpg123", "--quiet", fp + ext])
        else:
            music = pyglet.media.load(fp + ext)
            music.play()

    def play_audio(self, sound):
        fdir = "ru"
        # Linux or Windows?
        if ON_LINUX:
            snd = LINUX_PATH + "/audio/" + fdir + "/" + sound
        else:
            snd = WINDOWS_PATH + "\\audio\\" + fdir + "\\" + sound
        # Try each audio extension
        try:
            self.run_audio(snd, ".mp3")
        except Exception as e:
            if SHOW_EXCEPTION_ERRORS:
                print(".mp3 not found\n" + str(e))
            try:
                self.run_audio(snd, ".wav")
            except Exception as e:
                if SHOW_EXCEPTION_ERRORS:
                    print(".wav not found\n" + str(e))
                try:
                    self.run_audio(snd, ".ogg")
                except Exception as e:
                    if SHOW_EXCEPTION_ERRORS:
                        print(".ogg not found\n" + str(e))
                    print("~~ AUDIO FILE MISSING " + str(snd) + "  ~~")

    def play(self, Deck):
        if len(Deck) == 0:
            pass
        else:
            r = randint(0, (len(Deck)-1))
            audio_chk = str_to_bool(Deck[r][4])
            if audio_chk is not False:
                ru_word = Deck[r][1]
                ru_word = ru_word.strip()
                en_word = Deck[r][3]
                en_word = en_word.strip()
                if self.OnWords:
                    pause = 10
                elif self.OnSentences:
                    pause = 18
                elif self.OnAll:
                    if len(ru_word) < 25:
                        pause = 10
                    else:
                        pause = 18
                self.play_audio(str(Deck[r][4]))
                sleep(pause)
                print()
                print("LAST WORD WAS....")
                print(colored(ru_word + "\n" + en_word, 'blue'))
                print(colored(str(int(len(Deck)-1)) + " to go.", 'red'))
                del Deck[r]
                self.play(Deck)


    def set_type(self):
        t = input(str("Practice listening to [w]ords or [s]entences?"))
        return t

    def filter_big_list(self):
        for i in self.AllItems:
            if i[7] not in sentence_cats:
                if i[7] is not 1:
                    self.Words.append(i)
            elif i[7] in sentence_cats or i[7] is not 1:
                self.Sentences.append(i)

    def query_decks(self):
        query_active_words(lang_tables['ru'], self.AllItems)

    def run(self):
        self.query_decks()
        self.filter_big_list()
        t = self.set_type()
        if t == 'w':
            self.OnWords = True
            self.play(self.Words)
            self.OnWords = False
        elif t == 's':
            self.OnSentences = True
            self.play(self.Sentences)
            self.OnSentences = False
        elif t == 'b':
            self.OnAll = True
            self.play(self.AllItems)
            self.OnAll = False
        else:
            self.set_type()
