from Russian_Flash_Cards import *

"""
    NOT SURE IF I WILL MAKE THIS RUN FOR MORE LANGS OR NOT
    MAY ADD GERMAN LATER ON???
    JUST ADD TABLE FIELD FOR LANG AND VAR IN FUNCTIONS IF SO!
"""

Writing_Checked = False
Structures_Checked = False

date_text = str(year) + " " + str(month) + " " + str(day)
curr_d = str(datetime.datetime.strptime(date_text, '%Y %m %d'))
curr_d = curr_d.replace("-", " ")
curr_d = curr_d.replace(" 00:00:00", "")

need_to_practice = []

def check_task(tsk):
    try:
        global need_to_practice
        conn = mysql.connector.connect(**config)
        cur = conn.cursor()
        query = "SELECT date FROM special_dates WHERE task='" + tsk + "' LIMIT 1"
        cur.execute(str(query))
        for i in cur:
            td = i[0]
        cur.close()
        conn.close()
        if td != curr_d:
            need_to_practice.append(tsk)
    except TimeoutError:
        print("Can not check writing task, DB is down, quitting.")
        quit()
    except mysql.connector.errors.InterfaceError:
        print("Can not check writing task, DB is down, quitting.")
        quit()


def update_task(tsk):
    try:
        conn = mysql.connector.connect(**config)
        cur = conn.cursor()
        query = "UPDATE special_dates SET date='" + curr_d + "' WHERE task='" + str(tsk) + "'"
        cur.execute(str(query))
        conn.commit()
        cur.close()
        conn.close()
    except mysql.connector.errors.InterfaceError:
        print("Can not check writing task, DB is down, quitting.")
        quit()
    except:
        print("Can not check writing task, DB is down.")
        quit()


def practice_these():
    global need_to_practice
    global Writing_Checked
    global Structures_Checked
    if not Writing_Checked or not Structures_Checked:
        for t in need_to_practice:
            t1 = t.replace("_", " ")
            ans = input(str("You are behind on " + t1 + ". Enter 1 if you already studied / practiced for today!"))
            if ans == '1' or ans == 1:
                update_task(t)
        print("\n")
        Writing_Checked = True
        Structures_Checked = True


def do_task_checks():
    if not Writing_Checked or not Structures_Checked:
        check_task("writing_practice")
        check_task("construction_practice")

