# This Python file uses the following encoding: utf-8
from Russian_Flash_Cards import *
import glob
import ntpath
from subprocess import DEVNULL, STDOUT, check_call


class LongText():

    long_images = []
    image_ext = ["jpg", "jpeg", "png"]

    long_images_path = ""
    if ON_LINUX:
        long_images_path = LINUX_PATH + "\\images\\long_text\\"
    else:
        long_images_path = WINDOWS_PATH + "\\images\\long_text\\"

    def query_images(self):
        # print(WINDOWS_PATH)
        # print(self.long_images_path)
        for ext in self.image_ext:
            a = glob.glob(self.long_images_path + "*." + ext)
            for aa in a:
                self.long_images.append(aa)
        # print(str(len(self.long_images)))
        # print(self.long_images)
        # exit()

    def get_random_image(self):
        r = my_randint(len(self.long_images))
        # print(str(r))
        # exit()
        file = self.long_images[r]
        if ON_LINUX:
            # subprocess.call(["xdg-open", file])
            call(["xdg-open", file])
        else:
            os.startfile(file)

        inp = input(str("Press any key when ready."))

        if ON_LINUX:
            pass
        else:
            window_title = ntpath.basename(file) + " - " + IMAGE_PROGRAM_TITLE
            check_call('taskkill /fi "WINDOWTITLE eq ' + window_title + '"', stdout=DEVNULL, stderr=STDOUT)
        del self.long_images[r]
        if len(self.long_images) > 0:
            self.get_random_image()
        else:
            print("All done!")
            exit()

    def run(self):
        self.query_images()
        self.get_random_image()


if __name__ == "__main__":
    a = LongText()
    a.run()
